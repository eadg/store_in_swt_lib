package com.mooo.rokicki.magazyn.db;

import java.util.HashSet;
import java.util.Set;

public class Dostawca implements java.io.Serializable, Hibernable {

	private static final long serialVersionUID = 1L;

	public Dostawca() {}
	
	private Integer id;
	private String name;
	private Set<Faktura> fakturs = new HashSet<Faktura>(0);

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Set<Faktura> getFakturs() {
		return fakturs;
	}
	public void setFakturs(Set<Faktura> fakturs) {
		this.fakturs = fakturs;
	}



	
}
