package com.mooo.rokicki.magazyn.db;

import java.util.Date;

public class Historia implements java.io.Serializable, Hibernable {

	public Historia() {
	}

	private static final long serialVersionUID = 1L;
	private Integer id;
	private Produkt produkt;
	private Integer amount;
	private Personel personel;
	private Date time;
	private String operation;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer idOperacji) {
		this.id = idOperacji;
	}
	public Produkt getProdukt() {
		return produkt;
	}
	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer zmiana) {
		this.amount = zmiana;
	}
	public Personel getPersonel() {
		return personel;
	}
	public void setPersonel(Personel personel) {
		this.personel = personel;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date kiedy) {
		this.time = kiedy;
	}
	
	
	public String getName() {
		return produkt.getName(); 
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}	
		
}
