package com.mooo.rokicki.magazyn.db;

import java.util.HashSet;
import java.util.Set;

public class Faktura  implements java.io.Serializable, Hibernable {
	
	private static final long serialVersionUID = 1L;
	
	public Faktura() {
	}
	
	private Integer id;
	private String name;
	private Dostawca dostawca;
	private Boolean dead;
	private Set<Produkt> produkts = new HashSet<Produkt>(0);
	private Set<HistoriaFaktura> historias = new HashSet<HistoriaFaktura>(0);

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Set<Produkt> getProdukts() {
		return produkts;
	}
	public void setProdukts(Set<Produkt> produkts) {
		this.produkts = produkts;
	}
	public Dostawca getDostawca() {
		return dostawca;
	}
	public void setDostawca(Dostawca dostawca) {
		this.dostawca = dostawca;
	}
	public Boolean getDead() {
		return dead;
	}
	public void setDead(Boolean dead) {
		this.dead = dead;
	}
	public String toString() {
		return dostawca.getName()+"<>"+name;
	}
	public Set<HistoriaFaktura> getHistorias() {
		return historias;
	}
	public void setHistorias(Set<HistoriaFaktura> historias) {
		this.historias = historias;
	}
}
