package com.mooo.rokicki.magazyn.db;

import java.util.HashSet;
import java.util.Set;

public class Produkt implements java.io.Serializable, Hibernable {

	private static final long serialVersionUID = 1L;

	public Produkt() {
	}

	private Integer id;
	private String name;
//	private Integer amount;
	private Double value;
	private Kategoria kategoria;
	private Faktura faktura;
	private Jednostka jednostka;
	private Set<Historia> historias = new HashSet<Historia>(0);

	public Integer getId() {
		return id;
	}
	public void setId(Integer idProdukt) {
		this.id = idProdukt;
	}
	public String getName() {
		return name;
	}
	public void setName(String nazwaProdukt) {
		this.name = nazwaProdukt;
	}
//	public Integer getAmount() {
//		return amount;
//	}
//	public void setAmount(Integer stanProdukt) {
//		this.amount = stanProdukt;
//	}
	public Kategoria getKategoria() {
		return kategoria;
	}
	public void setKategoria(Kategoria kategoria) {
		this.kategoria = kategoria;
	}
	public Faktura getFaktura() {
		return faktura;
	}
	public void setFaktura(Faktura faktura) {
		this.faktura = faktura;
	}	
	public Set<Historia> getHistorias() {
		return historias;
	}
	public void setHistorias(Set<Historia> historias) {
		this.historias = historias;
	}
	public Jednostka getJednostka() {
		return jednostka;
	}
	public void setJednostka(Jednostka jednostka) {
		this.jednostka = jednostka;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
}
