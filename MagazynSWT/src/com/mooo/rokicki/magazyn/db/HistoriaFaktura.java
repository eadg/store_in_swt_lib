package com.mooo.rokicki.magazyn.db;

import java.util.Date;

public class HistoriaFaktura implements java.io.Serializable, Hibernable {

	public HistoriaFaktura() {
	}

	private static final long serialVersionUID = 1L;
	private Integer id;
	private Faktura faktura;
	private Date time;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer idOperacji) {
		this.id = idOperacji;
	}
	public Faktura getFaktura() {
		return faktura;
	}
	public void setFaktura(Faktura faktura) {
		this.faktura = faktura;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getName() {
		return faktura.getName();
	}	
}
