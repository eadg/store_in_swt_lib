package com.mooo.rokicki.magazyn.db;

import java.util.HashSet;
import java.util.Set;

public class Personel  implements java.io.Serializable, Hibernable  {
	
	private static final long serialVersionUID = 1L;
	
	public Personel() {	
	}
	
	private Integer id;
	private String name;
	private Boolean system;
	private Set<Historia> historias = new HashSet<Historia>(0);

	public Integer getId() {
		return id;
	}
	public void setId(Integer idPersonel) {
		this.id = idPersonel;
	}
	public String getName() {
		return name;
	}
	public void setName(String imie) {
		this.name = imie;
	}

	public Set<Historia> getHistorias() {
		return historias;
	}
	public void setHistorias(Set<Historia> historias) {
		this.historias = historias;
	}
	public Boolean getSystem() {
		return system;
	}
	public void setSystem(Boolean system) {
		this.system = system;
	}
			
}
