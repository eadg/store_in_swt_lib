package com.mooo.rokicki.magazyn.db;

import java.util.HashSet;
import java.util.Set;

public class Jednostka  implements java.io.Serializable, Hibernable  {
	
	private static final long serialVersionUID = 1L;
	
	public Jednostka() {	
	}
	
	private Integer id;
	private String name;
	private Set<Produkt> produkts = new HashSet<Produkt>(0);

	public Integer getId() {
		return id;
	}
	public void setId(Integer idJednostka) {
		this.id = idJednostka;
	}
	public String getName() {
		return name;
	}
	public void setName(String numerJednostka) {
		this.name = numerJednostka;
	}
	public Set<Produkt> getProdukts() {
		return produkts;
	}
	public void setProdukts(Set<Produkt> produkts) {
		this.produkts = produkts;
	}

}