package com.mooo.rokicki.magazyn.util;

public enum MessageInfo {

	USUN_PRZYPISANIA_PERSONEL("UWAGA: Usunięte zostaną przypisania produktów dla wybranej osoby oraz usunięta zostanie wybrana osoba!"),
	PERSONEL_DODANY("Personel dodany."),
	PERSONEL_BYL_DODANY("Personel był już wcześniej dodany."),
	TOWAR_DODANY("Towar dodany."),
	DOSTAWCA_DODANY("Dostawca dodany."),
	DOSTAWCA_BYL_DODANY("Dostawca był już wcześniej dodany."),
	JEDNOSTKA_DODANA("Jednostka dodana."),
	JEDNOSTKA_BYLA_DODANA("Jednostka była już wcześniej dodana."),
	KATEGORIA_DODANA("Kategoria dodana."),
	KATEGORIA_BYLA_DODANA("Kategoria była już wcześniej dodana."),
	FAKTURA_DODANA("Faktura dodana."),
	TOWAR_PRZYPISANY("Towar przypisany."),
	AKTUALIZACJA("Dane zaktualizowane."),
	FAKTURA_ROZLICZ("Faktura rozliczona."),
	ZMIANA_FAKTURA("Faktura zmieniona."),
	ZMIANA_DOSTAWCA("Dostawca zmieniony."),
	ZMIANA_PERSONEL("Osoba zmieniona."),
	ZMIANA_UNIT("Jednostka zmieniona."),
	NO_ITEM_BILL("Brak zarejestrowanych produktów z wybranej faktury.");
	
	private String value;
	
	private MessageInfo(String s) {
		value=s;
	}

	public String getValue() {
		return value;
	}


}
