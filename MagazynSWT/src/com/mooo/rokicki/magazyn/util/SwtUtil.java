package com.mooo.rokicki.magazyn.util;

import org.eclipse.swt.widgets.Text;

import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.bindings.keys.ParseException;
import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.fieldassist.SimpleContentProposalProvider;
import org.eclipse.jface.fieldassist.TextContentAdapter;

import com.mooo.rokicki.magazyn.util.SystemVariable;

public class SwtUtil {
	
	private String KEY_PRESS = SystemVariable.AUTOCOMPLETATION_KEY.getValue();

	public void addAutoComplete(Text swtText, String[] data) {
		try {
			SimpleContentProposalProvider scp = new SimpleContentProposalProvider(data);
			scp.setProposals(data);
			KeyStroke ks = KeyStroke.getInstance(KEY_PRESS);
			ContentProposalAdapter adapter = new ContentProposalAdapter(swtText, new TextContentAdapter(), scp, ks, null);
			adapter.setProposalAcceptanceStyle(ContentProposalAdapter.PROPOSAL_REPLACE);
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

}
