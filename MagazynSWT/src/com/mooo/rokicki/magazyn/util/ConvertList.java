package com.mooo.rokicki.magazyn.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.mooo.rokicki.magazyn.db.Historia;
import com.mooo.rokicki.magazyn.db.Hibernable;

public class ConvertList {
	
	public static String[] listObjectToArrayString(List<Object> list_in) {
		String[] array_out = new String[list_in.size()];
		array_out=list_in.toArray(array_out);
		return array_out;
	}
	
	public static String[] listArrayStringToArrayString(List<String[]> list_in) {
		List<String> list_out = new ArrayList<String>();
		String out = null;
		for (Iterator<String[]> iterator = list_in.iterator(); iterator.hasNext();) {
			String[] sarray = (String[]) iterator.next();
			out = sarray[0] + " " + sarray[1]; 
			list_out.add(out);
		}
		String[] sarray_out = new String[list_out.size()];
		return list_out.toArray(sarray_out);
	}
	
	public static List<String[]> listArrayObjectToListArrayString(List<Object[]> list_in) {
		List<String[]> list_out = new ArrayList<String[]>();
		for (Iterator<Object[]> iterator = list_in.iterator(); iterator.hasNext();) {
			Object[] oarray = (Object[]) iterator.next();
			String[] sarray = new String[oarray.length]; 
			for(int index=0 ; index <oarray.length ; index++) {
				sarray[index] = oarray[index].toString();
			}
			list_out.add(sarray);
		}
		return list_out;
	}
	
	public static List<String[]> historiaToListString(List<Historia> list_in) {
		
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		List<String[]> list_out = new ArrayList<String[]>();
			for (Iterator<Historia> iterator = list_in.iterator(); iterator.hasNext();) {
				Historia hist = (Historia) iterator.next();
				list_out.add(new String[] { 
						hist.getProdukt().getName(),
						new Integer(Math.abs(hist.getAmount())).toString(),
						hist.getPersonel().getName(),
						df.format(hist.getTime())
						});
			}
			return list_out;
	}	

//	public static List<String[]> produktToListString(List<Produkt> list_in) {
//
//		List<String[]> list_out = new ArrayList<String[]>();
//			for (Iterator<Produkt> iterator = list_in.iterator(); iterator.hasNext();) {
//				Produkt produkt = (Produkt) iterator.next();
//				list_out.add(new String[] { 
//						produkt.getName(),
//						produkt.getAmount().toString(),					
//						produkt.getFaktura().getDostawca().getName(),
//						produkt.getFaktura().getName(),
//						produkt.getJednostka().getName(),
//						produkt.getValue().toString()
//						});
//			}
//			return list_out;
//	}
	
	public static String[] listToArray(List<? extends Hibernable> list_in) {
		
		String[] array_out = new String[list_in.size()];
		int index=0;
		for (Iterator<? extends Hibernable> iterator = list_in.iterator(); iterator
				.hasNext();) {
			Hibernable hiberobj = (Hibernable) iterator.next();
			array_out[index]=hiberobj.getName();
			index++;
		}
		return array_out;
	}

}