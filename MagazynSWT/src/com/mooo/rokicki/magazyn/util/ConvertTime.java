package com.mooo.rokicki.magazyn.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ConvertTime {
	
	public Date arrayIntToDate(Integer[] input) {
		Calendar cl = Calendar.getInstance();
		cl.set(input[0], input[1], input[2], 0, 0, 0);
		return cl.getTime();
	}
	
	public Date arrayIntegerToDateBegin(Integer[] input) {
		Calendar cl = Calendar.getInstance();
		cl.set(input[0], input[1], input[2], 0, 0, 0);
		return cl.getTime();
	}
	
	public Date arrayIntegerToDateEnd(Integer[] input) {
		Calendar cl = Calendar.getInstance();
		cl.set(input[0], input[1], input[2]+1, 0, 0, 0);
		return cl.getTime();
	}
	
	public static String prettyFullDate(Date d) {
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = DATE_FORMAT.format(d);
		return date;
	}
	
	public String prettySimpleDate(Date d) {
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
		String date = DATE_FORMAT.format(d);
		return date;
	}
	

	
}