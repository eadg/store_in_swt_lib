package com.mooo.rokicki.magazyn.util;

public enum MessageErrors {
	
	ONE_VALUE("Proszę wprowadzić wartość."),
	ALL_VALUE("Proszę uzupełnić wartości."),
	OUTPASS_AMOUNT("Przekroczenie stanu magazynowego."),
	PRODUKT_OUT("Produkt wydany");

	private String value;
	
	private MessageErrors(String s) {
		value=s;
	}

	public String getValue() {
		return value;
	}


}
