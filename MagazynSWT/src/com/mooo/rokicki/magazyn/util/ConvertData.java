package com.mooo.rokicki.magazyn.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class ConvertData {

	public static List<Object> absList(List<Object> list_in) {
		List<Object> list_out = new ArrayList<Object>();
		for(Object obj : list_in) {
			list_out.add(Math.abs((Integer)obj));
		}
		return list_out;
	}
	
	public static String setPrecisionToTwo(Double d) {
		BigDecimal bd = new BigDecimal(d);
		bd = bd.setScale(2, RoundingMode.HALF_EVEN);
		return bd.toString();
	}

}
