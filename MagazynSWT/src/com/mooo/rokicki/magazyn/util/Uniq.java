package com.mooo.rokicki.magazyn.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Uniq {
	
	public static String[] arrayToUniqArray(String[] input) {
		
		Set<String> temp = new HashSet<String>(Arrays.asList(input));
		String[] out = temp.toArray(new String[temp.size()]);
		return out;		
	}

}
