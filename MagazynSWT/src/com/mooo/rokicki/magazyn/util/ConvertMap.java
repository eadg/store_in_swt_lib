package com.mooo.rokicki.magazyn.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class ConvertMap {

	public static String[] getArrayString(Map<String, String> m) {
		Iterator<Entry<String, String>> it = m.entrySet().iterator();
		String[] sarray = new String[m.size()];
		int index = 0;
		while (it.hasNext()) {
			Map.Entry<String, String> pairs = (Map.Entry<String, String>) it
					.next();
			sarray[index] = pairs.getKey() + ": " + pairs.getValue();
//			it.remove(); // avoids ConcurrentModificationException! uuuu:)
			index++;
		}
		return sarray;
	}
	
	public static String arrayToString(String[] sarray) {
		StringBuilder sb = new StringBuilder();
		for(String s : sarray) {
			sb.append(s);
			sb.append("\n");
		}
		return sb.toString();
	}
}
