package com.mooo.rokicki.magazyn.util;
import com.mooo.rokicki.magazyn.net.Path;

public enum SystemVariable {

	PATH(new Path().getPath()),
	PATHPDF(new Path().getPath()+"pdf/"),
	AUTOCOMPLETATION_KEY("Ctrl+Space");

	private String value;
	
	private SystemVariable(String s) {
		value=s;
	}

	public String getValue() {
		return value;
	}
}
