package com.mooo.rokicki.magazyn.util;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.HibernateUtil;

public class TruncateTable {

	public void drop(String s) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.createSQLQuery("truncate table "+ s).executeUpdate();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
			System.out.println("finally");
		}
	
	}

}
