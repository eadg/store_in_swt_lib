package com.mooo.rokicki.magazyn.util;

public enum EnumInsert {

	NAME(0), AMOUNT(1), BILL(2), UNIT(3), CATEGORY(4), VALUE(5);	
//	NAME(0), AMOUNT(1), BILL(2), UNIT(3), CATEGORY(4);	
//	NAME(0), AMOUNT(1), CONTRACTOR(2), BILL(3), UNIT(4), CATEGORY(5);
//	NAME(0), AMOUNT(1), CATEGORY(2), CONTRACTOR(3), BILL(4), UNIT(5);
	
	private int value;
	
	public int getValue() {
		return value;
	}

	private EnumInsert(int i) {
		value=i;
		
	}
}
