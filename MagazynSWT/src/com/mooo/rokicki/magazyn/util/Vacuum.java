package com.mooo.rokicki.magazyn.util;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.HibernateUtil;

public class Vacuum {

	public void vacuumProdukt() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.createQuery("delete from Produkt p where p.id > 0").executeUpdate();
			session.createQuery("delete from Dostawca d where d.id > 2").executeUpdate();
			session.createQuery("delete from Kategoria k where k.id > 2").executeUpdate();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
			System.out.println("finally");
//			org.hsqldb.DatabaseManager.closeDatabases(0);
			
		}
	}

}
