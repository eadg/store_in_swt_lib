package com.mooo.rokicki.magazyn.util;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Version {
	
	private LinkedHashMap<String, String> verMap;
	
	public LinkedHashMap<String, String> getVerMap() {
		return verMap;
	}

	public Version() {
		verMap = new LinkedHashMap<String, String>();
		verMap.put("0.96b", "Poprawa błędów przy dodawaniu Jednostki/Personelu/Dostawcy. Dodanie nowego raportu: lista osób zarejestrowanych");
		verMap.put("0.96a", "Dodanie systemu pomocy. Dodanie możliwości cofnięcia na magazyn błędnie przydzielonego towaru");
		verMap.put("0.96", "Dodanie menu dostępnego z prawego przycisku myszki w celu przyśpieszenia rozchodu");
		verMap.put("0.95d", "Dodanie MAC adresów pozostałych komputerów z WZŻ");
		verMap.put("0.95c", "Usunięcie property Produkt.amount");
		verMap.put("0.95b", "Dodanie do formatki DodajTowar wszystkich dotychczasowych nazw produktów w celu przyśpieszenia wpisywania");
		verMap.put("0.95a", "Poprawa formatki Rozchód (dodanie ilości wolnego produktu), dodanie potwierdzeń operacji");
		verMap.put("0.95", "Dodanie menu: Przypisz towar innej osobie");
		verMap.put("0.94b", "Dodanie menu: Usuń pozycję z faktury");
		verMap.put("0.94a", "Poprawa raportu 'Co zostało przydzielone danej osobie' - dodanie okresu wyszukiwania");
		verMap.put("0.94", "Poprawa dostępu do katalogów w Linuxie - działają Raporty PDF");
		verMap.put("0.93", "Poprawa raportów 'Stan magazynu za okres' oraz 'Wyszukiwanie faktur dostawcy za okres' - dodanie okresu wyszukiwania");
		verMap.put("0.92", "Dodanie raportów 'Wyświetl historię danego produktu'");
		verMap.put("0.91", "Dodanie raportu 'Co zostało przydzielone danej osobie'");
		verMap.put("0.90", "Dodanie raportu 'Historia faktur dostawcy'");
		verMap.put("0.8", "Dodanie raportu 'Stan magazynu'");
		verMap.put("0.7", "Dodanie: db.Historia pola operacja oraz wartości początkowej, dodanie użytkownika system");
		verMap.put("0.6", "Dodanie: wartości pieniężnej, dodanie system wersjonowania via JNLP, dodanie 'Edycja Jednostki/Personelu/Dostawcy/Faktury'");
	}
	
	public String getVersion() {
		 Iterator<Entry<String, String>> it = verMap.entrySet().iterator();
		 String sout="";
		 while (it.hasNext()) {
		        Map.Entry<String, String> pairs = (Map.Entry<String, String>)it.next();
		        sout=" - wersja " + pairs.getKey();
		        break;
		 }       
		 return sout;
	}
}
