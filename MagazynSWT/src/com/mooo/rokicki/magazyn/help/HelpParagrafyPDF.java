package com.mooo.rokicki.magazyn.help;

import java.util.LinkedHashMap;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.mooo.rokicki.magazyn.pdf.FontPDF;

public class HelpParagrafyPDF extends FontPDF {
	
	private LinkedHashMap<String, String> map;
	
	public HelpParagrafyPDF() {
		map= new LinkedHashMap<String, String>();
		map.put("intro", "Wstęp");
		map.put("p1", "Od czego zacząć? - dodawanie towaru do magazynu");
		map.put("p2", "Rozchód towaru");
	}
	
	public LinkedHashMap<String, String> getMap() {
		return map;
	}

	Paragraph intro() {
		Paragraph pa = new Paragraph();
		Phrase ph = new Phrase("", body_font);

		ph.add("Program magazynowy można używać do ewidencjonowania towarów z różnych kategorii np. chemii, odzieży. ");
		ph.add("Ilość kategorii jest w zasadzie nieograniczona. Jednak ze względu na charakter lepiej nie mieszać np środków spożywczych ze środkami niespożywczymi. ");
		ph.add("Do ewidencjonowania środków spożywczych będzie możliwe szybkie stworzenie podobnego programu.");
//		ph.add(Chunk.NEWLINE);
		pa.add(ph);

		return pa;
	}

	Paragraph p1() {
		Paragraph pa = new Paragraph();
		Phrase ph = new Phrase("", body_font);

		ph.add("Ażeby dodać towar do magazynu należy wcześniej dodać kategorię towarową, dostawcę oraz fakturę. ");
		ph.add(Chunk.NEWLINE);
		ph.add("Aby dodać kategorię należy z menu 'Dodaj' wybrać opcję 'Dodaj kategorię' wpisać kategorię i kliknąć OK. ");
		ph.add(Chunk.NEWLINE);
		ph.add("Aby dodać dostawcę należy z menu 'Dodaj' wybrać opcję 'Dodaj dostawcę' wpisać dostawcę i kliknąć OK. ");
		ph.add(Chunk.NEWLINE);
		ph.add("Aby dodać fakturę należy z menu 'Dodaj' wybrać opcję 'Dodaj fakturę' wpisać fakturę, wybrać dostawcę od którego jest dana faktura i kliknąć OK. ");
		ph.add(Chunk.NEWLINE);
		ph.add("Jeżeli mamy już zarejestrowaną fakturę to można dodać do niej asortyment. W tym celu wybieramy z menu 'Dodaj' opcję 'Dodaj towar' i uzupełniamy formularz. ");
		ph.add("Jeśli brakuje jakieś jednostki (np butelka), to trzeba ją dodać. ");
		ph.add(Chunk.NEWLINE);
		ph.add("Aby dodać jednostkę z menu 'Dodaj' należy wybrać opcję 'Dodaj jednostkę' wpisać i kliknąć OK. ");
		ph.add("Wracając do dodawania towaru po wypełnieniu formularz klikamy OK i towar powinien się pojawić w prawej tabelce 'Wolne środki na magazynie'. ");
		ph.add(Chunk.NEWLINE);
		ph.add("Podcza wypełnienia formularza w polu 'Nazwa produktu' można skorzystać z nazw towarów, które zostały wcześniej dodane w celu zaoszczedzenia ręczego wpisywania nazw. ");
		ph.add("W tym celu należy na klawiaturzę nacisnąć jednocześnie lewy przycisk CTRL (control) oraz dwukrotnie spację. Pojawi się wtedy lista dodanych wcześniej towarów, które można kliknąć myszką. ");
		pa.add(ph);

		return pa;
	}

	Paragraph p2() {
		Paragraph pa = new Paragraph();
		Phrase ph = new Phrase("", body_font);

		ph.add("Aby wydać towar z magazynu należy wcześniej dodać osobę która przejmie towar. W tym celu wybieramy z menu 'Dodaj' opcję 'Dodaj osobę' i uzupełniamy formularz. ");
		ph.add("Oczywiście daną osobę wystarczy dodać tylko raz. Jeżeli mamy już osobę dla której wydamy towar, wybieramy z menu 'Rozchód' opcję 'Rozchód towaru'. ");
		ph.add("Wypełnaiamy formularz i zatwierdzamy przyciskiem OK. Ilość do wydania nie może być większa niż ilość na magazynie. Efekt rozchodu jest widoczny w lewej tabelce 'Przydzielone środki'. ");
		ph.add(Chunk.NEWLINE);
		ph.add("Operację rozchodu można wykonać również poprzez kliknięcie prawym przyciskiem myszki na prawą tabelkę 'Wolne środki'. Formularz zostanie wypełniony automatycznie danymi, które zostały 'kliknięte'. ");
		ph.add("Wystarczy wpisać ilość wydanego towaru i wybrać osobę. ");
		pa.add(ph);

		return pa;
	}

}
