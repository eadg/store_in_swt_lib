package com.mooo.rokicki.magazyn.help;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
//import com.itextpdf.text.List;
//import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;
import com.mooo.rokicki.magazyn.pdf.FontPDF;
import com.mooo.rokicki.magazyn.help.HelpParagrafyPDF;

public class HelpPDF extends FontPDF {

	public String make(String uri) {
		Document document = new Document();
//		List orderedList = new List(List.ORDERED);
		HelpParagrafyPDF pgrafy = new HelpParagrafyPDF();
		LinkedHashMap<String, String> metody = pgrafy.getMap();
		String title = "Pomoc do magazynu";
		try {
			PdfWriter.getInstance(document, new FileOutputStream(uri));
			document.open();
			document.addTitle(title);

			Iterator<Entry<String, String>> iter = metody.entrySet().iterator();
			String skey = null;
			String sval = null;
			Paragraph head =null;
			Paragraph body =null;
			Integer item = new Integer(1);

			while (iter.hasNext()) {
				Entry<String, String> ent = iter.next();
				skey = ent.getKey();
				sval = ent.getValue();
				Class<?> c = Class.forName("com.mooo.rokicki.magazyn.help.HelpParagrafyPDF");
				Method metoda = c.getDeclaredMethod(skey);
				head = new Paragraph();
				head.add(new Phrase(item.toString() + ". " + sval, header_font));
				body = ((Paragraph) metoda.invoke(pgrafy, new Object[] {}));
//				orderedList.add(sval);
//				orderedList.add(new ListItem(sval, header_font));
//				document.add(orderedList);
				document.add(head);
				document.add(body);
				document.add(Chunk.NEWLINE);
				item++;
			}
			document.close();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return uri;
	}
}
