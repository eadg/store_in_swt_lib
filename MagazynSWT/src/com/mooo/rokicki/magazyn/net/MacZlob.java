package com.mooo.rokicki.magazyn.net;

import java.util.HashMap;

public class MacZlob {

	private HashMap<String, String> hm;

	public MacZlob() {
		hm = new HashMap<String, String>();
		hm.put("d8-5d-4c-fe-4e-a9", "Palikot");
		hm.put("44-37-E6-58-67-C8", "Fabryczna");
		hm.put("00-21-B7-AA-E4-47", "Żłobek Nr 1");
		hm.put("B8-88-E3-74-23-B6", "Żłobek Nr 1");
		hm.put("B8-88-E3-74-21-B4", "Żłobek Nr 2");
		hm.put("00-1A-6B-5E-3D-FC", "Żłobek Nr 2");	
		hm.put("B8-88-E3-74-21-32", "Żłobek Nr 4");	
		hm.put("00-1A-6B-5E-4C-71", "Żłobek Nr 4");	
		hm.put("B8-88-E3-74-1F-B3", "Żłobek Nr 5");	
		hm.put("00-1A-6B-5E-4C-77", "Żłobek Nr 5");	
		hm.put("00-1A-6B-5E-4C-D8", "Żłobek Nr 6");	
		hm.put("B8-88-E3-74-20-0E", "Żłobek Nr 6");	
		hm.put("00-1A-6B-5E-4C-C5", "Żłobek Nr 7");	
		hm.put("B8-88-E3-74-1E-F0", "Żłobek Nr 7");	
		hm.put("B8-88-E3-74-1F-62", "Żłobek Nr 9");	
		hm.put("00-1A-6B-5E-3D-71", "Żłobek Nr 10");
		hm.put("B8-88-E3-74-1F-BF", "Żłobek Nr 10");
		hm.put("00-1A-6B-5E-4C-98", "Żłobek Nr 11");
		hm.put("00-1A-6B-5E-39-82", "Żłobek Nr 12");
		hm.put("F0-DE-F1-A7-5A-98", "Żłobek Nr 13");
		hm.put("00-16-41-6F-BD-CB", "Żłobek Nr 13");
		hm.put("00-01-6C-4D-37-AF", "Żłobek Nr 14");
		hm.put("00-01-6C-4D-8E-22", "Żłobek Nr 14");
		hm.put("B8-88-E3-74-20-0D", "Żłobek Nr 14");
		hm.put("00-01-6C-4D-37-A7", "Żłobek Nr 14");
		hm.put("90-FB-A6-02-D8-9C", "Żłobek Nr 15");
		hm.put("90-FB-A6-02-FC-2D", "Żłobek Nr 15");
		hm.put("F8-0F-41-8E-F3-39", "Żłobek Nr 2");
		hm.put("F8-0F-41-8F-5E-F9", "Żłobek Nr 3");
		hm.put("F8-0F-41-8F-5E-F1", "Żłobek Nr 5");
		hm.put("F8-0F-41-8E-F3-D0", "Żłobek Nr 8");
		hm.put("F8-0F-41-8E-F4-03", "Żłobek Nr 10");
		hm.put("F8-0F-41-8F-5E-F4", "Żłobek Nr 14");
		hm.put("F8-0F-41-8E-F3-D2", "Żłobek Nr 14");
		hm.put("F8-0F-41-8F-5F-25", "Żłobek Nr 14");
		hm.put("F8-0F-41-8B-C6-5F", "Żłobek Nr 13");
		hm.put("F8-0F-41-8B-C6-56", "Żłobek Nr 15");
		//a
	}
	
	
	public String getNrZlobka(String s) {
		if(hm.containsKey(s))
			return hm.get(s);
		else
			return "Żłobek Nr **";
	}

}
