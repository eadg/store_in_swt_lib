package com.mooo.rokicki.magazyn.net;

public class NazwaZlobka {
	
	private String nazwa;
	
	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public NazwaZlobka() {
		LocalMac m = new LocalMac();
		MacZlob mc = new MacZlob();
		setNazwa(mc.getNrZlobka(m.getMac()));
	}

}
