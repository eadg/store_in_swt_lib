package com.mooo.rokicki.magazyn.net;

public class CheckOS {

	private String os;

	public CheckOS() {
		if (System.getProperty("os.name").matches(".+indows.+")) {
			setOs("windows");
		} else if (System.getProperty("os.name").matches("Linux")) {
			setOs("linux");
		}
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

}
