package com.mooo.rokicki.magazyn.net;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LocalMac {

	private String mac;

	public LocalMac() {
		String s = new CheckOS().getOs();
		if (s.equals("windows")) {
			ustalMac();
		} 
		else if(s.equals("linux")) {
			ustalMacLinux();
		}
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	private String ustalMac() {
		InetAddress ip;
		try {

			ip = InetAddress.getLocalHost();
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			byte[] mac = network.getHardwareAddress();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i],
						(i < mac.length - 1) ? "-" : ""));
			}
			setMac(sb.toString());
			return sb.toString();

		} catch (UnknownHostException e) {

			e.printStackTrace();
			return "";

		} catch (SocketException e) {

			e.printStackTrace();
			return "";
		}

	}

	private String ustalMacLinux() {

		try {
			String line;
			String mac="";
			Matcher mat;
			Process p = Runtime.getRuntime().exec("ip addr show dev wlan0");			
			BufferedReader inn = new BufferedReader(new InputStreamReader(p.getInputStream()));
			Pattern mac_pattern = Pattern
					.compile(".*ether.*(\\w\\w:\\w\\w:\\w\\w:\\w\\w:\\w\\w:\\w\\w).*brd.*");
			while ((line=inn.readLine()) != null) {
				mat=mac_pattern.matcher(line);
				if(mat.matches()) {
					mac=mat.group(1);
				}
			}
			inn.close();
			setMac(mac);
			return mac;
		} catch (IOException ioe) {
			System.out.println(ioe);
			return mac;
		}
	}
}