package com.mooo.rokicki.magazyn.net;

public class Path {
	
	private String path;
	
	public Path() {
		String s = new CheckOS().getOs();
		if (s.equals("windows")) {
			setPath("c:/Program Files/magazyn/");
		} 
		else if(s.equals("linux")) {
			setPath("../../../magazyn/");
		}		
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	

}
