package com.mooo.rokicki.magazyn.swt;

import java.util.List;

import org.eclipse.swt.SWT;

import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.mooo.rokicki.magazyn.hql.SelectPersonel;
import com.mooo.rokicki.magazyn.hql.ListProdukt;
import com.mooo.rokicki.magazyn.hql.SelectKategoria;
import com.mooo.rokicki.magazyn.hql.SelectProdukt;
import com.mooo.rokicki.magazyn.util.ConvertList;
import com.mooo.rokicki.magazyn.util.MessageErrors;
import com.mooo.rokicki.magazyn.util.MessageInfo;
import com.mooo.rokicki.magazyn.util.Uniq;

import com.mooo.rokicki.magazyn.db.Produkt;

public class FormRozchodTowar {

	private Shell nowySzel;
	private Display display;
	private Shell parent;

	private int modalResult = SWT.CANCEL;
	private Text stanText;
	private Label kategoriaLabel;	
	private Label stanLabel;
	private Label fakturaLabel;
	private Label personelLabel;
	private Label produktLabel;
	private Combo kategoriaCombo;
	private Combo fakturaCombo;
	private Combo produktCombo;
	private Combo personelCombo;
	private Label magazynLabelLeft;
	private Label magazynLabelRight;

	private Button buttonOK;
	private Button buttonCancel;

	private ListProdukt lp;
	private SelectProdukt sp;
	private List<Produkt> listaProduktow;

	private Integer[] updateAmount;
	private Boolean update;
	private String person;

	private final FormToolkit formToolkit = new FormToolkit(
			Display.getDefault());


	public FormRozchodTowar(Shell parentShell) {
		display = Display.getCurrent();
		parent = parentShell;
		nowySzel = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		init();
	}

	public void init() {
		setUpdate(false);
		lp = new ListProdukt();
		sp = new SelectProdukt();
		nowySzel.setSize(292, 276);
		nowySzel.setText("Rozchód produktu z faktury");
		nowySzel.setLayout(new GridLayout(2, false));

		kategoriaLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_kategoriaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_kategoriaLabel.widthHint = 103;
		kategoriaLabel.setLayoutData(gd_kategoriaLabel);
		formToolkit.adapt(kategoriaLabel, true, true);
		kategoriaLabel.setText("Kategoria");
		
		kategoriaCombo = new Combo(nowySzel, SWT.READ_ONLY);
		kategoriaCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,	false, 1, 1));
		kategoriaCombo.setItems(ConvertList.listToArray(new SelectKategoria().listStringKategoria()));
		kategoriaCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				fakturaCombo.setItems(Uniq.arrayToUniqArray(lp.arrayFakturaByKategoria(kategoriaCombo.getText())));
//						Uniq.arrayToUniqArray(Convert.listMultiArrayToArray(Convert.produktToListString(sp.listProduktByKategoria(kategoriaCombo.getText())),EnumInsert.BILL.getValue())));
						produktCombo.setItems(new String[0]);
						magazynLabelRight.setText(new String());	
			}
		});

		formToolkit.adapt(kategoriaCombo);
		formToolkit.paintBordersFor(kategoriaCombo);		
		

		fakturaLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_fakturaLabel = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_fakturaLabel.widthHint = 103;
		fakturaLabel.setLayoutData(gd_fakturaLabel);
		formToolkit.adapt(fakturaLabel, true, true);
		fakturaLabel.setText("Faktura");

		fakturaCombo = new Combo(nowySzel, SWT.READ_ONLY);
		fakturaCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		fakturaCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				produktCombo.setItems(sp.arraySelectProduktByFaktura(fakturaCombo.getText().split("<>")[1]));
//				produktCombo.setItems(Convert.listMultiArrayToArray(
//						Convert.produktToListString(sp.listProduktByFaktura(fakturaCombo.getText())),EnumInsert.NAME.getValue()));
				magazynLabelRight.setText(new String());
			}
		});
		formToolkit.adapt(fakturaCombo);
		formToolkit.paintBordersFor(fakturaCombo);

		produktLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_jednostkaLabel = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_jednostkaLabel.widthHint = 100;
		produktLabel.setLayoutData(gd_jednostkaLabel);
		formToolkit.adapt(produktLabel, true, true);
		produktLabel.setText("Produkt");

		produktCombo = new Combo(nowySzel, SWT.READ_ONLY);
		produktCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		produktCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				setLp(lp.listProduktByFakturaProdukt(produktCombo.getText(), fakturaCombo.getText().split("<>")[1]));
				magazynLabelRight.setText(sp.getAmountForProdukt(listaProduktow.get(0).getId()).get(0).toString()+"  ["+listaProduktow.get(0).getJednostka().getName()+"]" );
//				jednostkaLabelRight.setText(Convert.listMultiArrayToArray(Convert.produktToListString(listaProduktow),EnumInsert.UNIT.getValue())[0]);
			}
		});
		formToolkit.adapt(produktCombo);
		formToolkit.paintBordersFor(produktCombo);
		
		magazynLabelLeft = new Label(nowySzel, SWT.NONE);
		formToolkit.adapt(magazynLabelLeft, true, true);
		magazynLabelLeft.setText("Ilość na magazynie");
		
		magazynLabelRight = new Label(nowySzel, SWT.BORDER | SWT.READ_ONLY);
		magazynLabelRight.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1));
		formToolkit.adapt(magazynLabelRight, true, true);
		
		stanLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_stanLabel = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_stanLabel.widthHint = 103;
		stanLabel.setLayoutData(gd_stanLabel);
		formToolkit.adapt(stanLabel, true, true);
		stanLabel.setText("Ilość do wydania");

		stanText = new Text(nowySzel, SWT.BORDER);
		GridData gd_stanText = new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1);
		gd_stanText.widthHint = 133;
		stanText.setLayoutData(gd_stanText);
		formToolkit.adapt(stanText, true, true);

		personelLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_personelLabel = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_personelLabel.widthHint = 102;
		personelLabel.setLayoutData(gd_personelLabel);
		formToolkit.adapt(personelLabel, true, true);
		personelLabel.setText("Osoba");

		personelCombo = new Combo(nowySzel, SWT.READ_ONLY);
		personelCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		personelCombo.setItems(ConvertList.listToArray(new SelectPersonel()
				.listPersonel()));
		formToolkit.adapt(personelCombo);
		formToolkit.paintBordersFor(personelCombo);


		new Label(nowySzel, SWT.NONE);
		new Label(nowySzel, SWT.NONE);
		// new Label(nowySzel, SWT.NONE);
		// new Label(nowySzel, SWT.NONE);

		buttonOK = new Button(nowySzel, SWT.PUSH);
		GridData gd_buttonOK = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_buttonOK.widthHint = 102;
		buttonOK.setLayoutData(gd_buttonOK);
		buttonOK.setText("OK");
		buttonOK.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				controls2Data();
				modalResult = SWT.OK;
				nowySzel.close();
			}
		});

		buttonCancel = new Button(nowySzel, SWT.CENTER);
		GridData gd_buttonCancel = new GridData(SWT.CENTER, SWT.CENTER, false,
				false, 1, 1);
		gd_buttonCancel.widthHint = 102;
		buttonCancel.setLayoutData(gd_buttonCancel);
		buttonCancel.setText("Anuluj");
		buttonCancel.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				//controls2Data();
				modalResult = SWT.CANCEL;
				nowySzel.close();
			}
		});
	}

	public void startEventLoop() {
		while (!nowySzel.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	public int doModal() {
		nowySzel.open();
		// data2Controls();
		startEventLoop();
		return modalResult;
	}

	public void data2Controls() {
		// produktText.setText(textValue);
	}

	public void controls2Data() {
		
		if(listaProduktow!=null && !personelCombo.getText().isEmpty() && !stanText.getText().isEmpty()) {
			
			String oDB = sp.getAmountForProdukt(listaProduktow.get(0).getId()).get(0).toString();
			Integer ileDB = Integer.decode(oDB);
//			Integer ileDB = sp.getAmountForProdukt(listaProduktow.get(0).getId()).get(0);
			Integer ileForm = Integer.decode(stanText.getText().trim());

			if(ileDB < ileForm){
				MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.OUTPASS_AMOUNT.getValue());
			}
			else {
				setUpdate(true);
				Integer updateProdukt = ileDB - ileForm;
				Integer updateHistoria= ileForm*(-1);
				setUpdateAmount(new Integer[] {updateProdukt,  updateHistoria});
				setPerson(personelCombo.getText());
				MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.TOWAR_PRZYPISANY.getValue());
			}			
		}
		else {
			MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.ALL_VALUE.getValue());			
		}
	
	}

	public List<Produkt> getListaProduktow() {
		return listaProduktow;
	}

	public void setLp(List<Produkt> lp) {
		this.listaProduktow = lp;
	}

	public Boolean getUpdate() {
		return update;
	}

	public void setUpdate(Boolean update) {
		this.update = update;
	}

	public String getPerson() {
		return person;
	}

	public void setPerson(String person) {
		this.person = person;
	}

	public Integer[] getUpdateAmount() {
		return updateAmount;
	}

	public void setUpdateAmount(Integer[] updateAmount) {
		this.updateAmount = updateAmount;
	}	

	
}
