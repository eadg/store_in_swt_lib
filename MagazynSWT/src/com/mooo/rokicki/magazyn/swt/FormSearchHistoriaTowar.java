package com.mooo.rokicki.magazyn.swt;

import java.util.List;

import org.eclipse.swt.SWT;

import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.mooo.rokicki.magazyn.hql.ListProdukt;
import com.mooo.rokicki.magazyn.hql.SelectKategoria;
import com.mooo.rokicki.magazyn.hql.SelectProdukt;
import com.mooo.rokicki.magazyn.util.ConvertList;
import com.mooo.rokicki.magazyn.util.MessageErrors;
import com.mooo.rokicki.magazyn.util.Uniq;

import com.mooo.rokicki.magazyn.db.Produkt;

public class FormSearchHistoriaTowar {

	private Shell nowySzel;
	private Display display;
	private Shell parent;

	private int modalResult = SWT.CANCEL;
	private Label kategoriaLabel;	
	private Label fakturaLabel;
	private Label produktLabel;
	private Combo kategoriaCombo;
	private Combo fakturaCombo;
	private Combo produktCombo;
	private Label jednostkaLabelLeft;
	private Label jednostkaLabelRight;

	private Button buttonOK;
	private Button buttonCancel;

	private ListProdukt lp;
	private SelectProdukt sp;
	private List<Produkt> listaProduktow;

	private final FormToolkit formToolkit = new FormToolkit(
			Display.getDefault());

	public FormSearchHistoriaTowar(Shell parentShell) {
		display = Display.getCurrent();
		parent = parentShell;
		nowySzel = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		init();
	}

	public void init() {
		lp = new ListProdukt();
		sp = new SelectProdukt();
		nowySzel.setSize(292, 226);
		nowySzel.setText("Wyszukiwanie produktu  do przedstawienia");
		nowySzel.setLayout(new GridLayout(2, false));

		kategoriaLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_kategoriaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_kategoriaLabel.widthHint = 103;
		kategoriaLabel.setLayoutData(gd_kategoriaLabel);
		formToolkit.adapt(kategoriaLabel, true, true);
		kategoriaLabel.setText("Kategoria");
		
		kategoriaCombo = new Combo(nowySzel, SWT.READ_ONLY);
		kategoriaCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,	false, 1, 1));
		kategoriaCombo.setItems(ConvertList.listToArray(new SelectKategoria().listStringKategoria()));
		kategoriaCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				fakturaCombo.setItems(Uniq.arrayToUniqArray(lp.arrayFakturaByKategoria(kategoriaCombo.getText())));
//						Uniq.arrayToUniqArray(Convert.listMultiArrayToArray(Convert.produktToListString(sp.listProduktByKategoria(kategoriaCombo.getText())),EnumInsert.BILL.getValue())));
						produktCombo.setItems(new String[0]);
						jednostkaLabelRight.setText(new String());				
			}
		});

		formToolkit.adapt(kategoriaCombo);
		formToolkit.paintBordersFor(kategoriaCombo);		
		

		fakturaLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_fakturaLabel = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_fakturaLabel.widthHint = 103;
		fakturaLabel.setLayoutData(gd_fakturaLabel);
		formToolkit.adapt(fakturaLabel, true, true);
		fakturaLabel.setText("Faktura");

		fakturaCombo = new Combo(nowySzel, SWT.READ_ONLY);
		fakturaCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		fakturaCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				produktCombo.setItems(sp.arraySelectProduktByFaktura(fakturaCombo.getText().split("<>")[1]));
				jednostkaLabelRight.setText(new String());
			}
		});
		formToolkit.adapt(fakturaCombo);
		formToolkit.paintBordersFor(fakturaCombo);

		produktLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_jednostkaLabel = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_jednostkaLabel.widthHint = 100;
		produktLabel.setLayoutData(gd_jednostkaLabel);
		formToolkit.adapt(produktLabel, true, true);
		produktLabel.setText("Produkt");

		produktCombo = new Combo(nowySzel, SWT.READ_ONLY);
		produktCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		produktCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				setLp(lp.listProduktByFakturaProdukt(produktCombo.getText(), fakturaCombo.getText().split("<>")[1]));
				jednostkaLabelRight.setText(listaProduktow.get(0).getJednostka().getName());
			}
		});
		formToolkit.adapt(produktCombo);
		formToolkit.paintBordersFor(produktCombo);

		jednostkaLabelLeft = new Label(nowySzel, SWT.NONE);
		GridData gd_lblJednostka = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_lblJednostka.widthHint = 103;
		jednostkaLabelLeft.setLayoutData(gd_lblJednostka);
		formToolkit.adapt(jednostkaLabelLeft, true, true);
		jednostkaLabelLeft.setText("Jednostka");

		jednostkaLabelRight = new Label(nowySzel, SWT.BORDER | SWT.READ_ONLY);
		jednostkaLabelRight.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1));
		formToolkit.adapt(jednostkaLabelRight, true, true);

		new Label(nowySzel, SWT.NONE);
		new Label(nowySzel, SWT.NONE);

		buttonOK = new Button(nowySzel, SWT.PUSH);
		GridData gd_buttonOK = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_buttonOK.widthHint = 102;
		buttonOK.setLayoutData(gd_buttonOK);
		buttonOK.setText("OK");
		buttonOK.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				controls2Data();
				modalResult = SWT.OK;
				nowySzel.close();
			}
		});

		buttonCancel = new Button(nowySzel, SWT.CENTER);
		GridData gd_buttonCancel = new GridData(SWT.CENTER, SWT.CENTER, false,
				false, 1, 1);
		gd_buttonCancel.widthHint = 102;
		buttonCancel.setLayoutData(gd_buttonCancel);
		buttonCancel.setText("Anuluj");
		buttonCancel.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				//controls2Data();
				modalResult = SWT.CANCEL;
				nowySzel.close();
			}
		});
	}

	public void startEventLoop() {
		while (!nowySzel.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	public int doModal() {
		nowySzel.open();
		// data2Controls();
		startEventLoop();
		return modalResult;
	}

	public void data2Controls() {
		// produktText.setText(textValue);
	}

	public void controls2Data() {
		
		if(listaProduktow!=null ) {



		}
		else {
			MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.ALL_VALUE.getValue());			
		}
	
	}

	public List<Produkt> getListaProduktow() {
		return listaProduktow;
	}

	public void setLp(List<Produkt> lp) {
		this.listaProduktow = lp;
	}

	
}
