package com.mooo.rokicki.magazyn.swt;

import org.eclipse.swt.SWT;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.graphics.Point;
import com.mooo.rokicki.magazyn.hql.SelectPersonel;
import com.mooo.rokicki.magazyn.util.ConvertList;

public class FormDeletePersonel {
	
	private Shell nowySzel;
	private Display display;
	private Shell parent;

	private int modalResult = SWT.CANCEL;
	private Label personelLabel;	
	private Combo personelCombo;	
	
	private Button buttonOK;
	private Button buttonCancel;
	
	private String daneFormularza;
	
	public String getDaneFormularza() {
		return daneFormularza;
	}

	public void setDaneFormularza(String daneFormularza) {
		this.daneFormularza = daneFormularza;
	}

	private final FormToolkit formToolkit = new FormToolkit(
			Display.getDefault());

	public FormDeletePersonel(Shell parentShell) {
		display = Display.getCurrent();
		parent = parentShell;
		nowySzel = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		nowySzel.setMinimumSize(new Point(132, 50));
		init();
	}

	public void init() {
		nowySzel.setSize(379, 118);
		nowySzel.setText("Usuwanie osoby");
		nowySzel.setLayout(new GridLayout(2, false));
		
		personelLabel = new Label(nowySzel, SWT.NONE);
		personelLabel.setAlignment(SWT.CENTER);
		GridData gd_personelLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_personelLabel.widthHint = 140;
		personelLabel.setLayoutData(gd_personelLabel);
		formToolkit.adapt(personelLabel, true, true);
		personelLabel.setText("Wybierz osobę do usunięcia");
		
		personelCombo = new Combo(nowySzel, SWT.READ_ONLY);
		GridData gd_personelCombo = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_personelCombo.widthHint = 184;
		personelCombo.setLayoutData(gd_personelCombo);
		String[] tabPersonel=ConvertList.listToArray(new SelectPersonel().listPersonel());		
		personelCombo.setItems(tabPersonel);
		
		
		formToolkit.adapt(personelCombo);
		formToolkit.paintBordersFor(personelCombo);
				
				new Label(nowySzel, SWT.NONE);
				new Label(nowySzel, SWT.NONE);
				
						buttonOK = new Button(nowySzel, SWT.PUSH);
						GridData gd_buttonOK = new GridData(SWT.CENTER, SWT.CENTER, false, false,	1, 1);
						gd_buttonOK.widthHint = 102;
						buttonOK.setLayoutData(gd_buttonOK);
						buttonOK.setText("OK");
						buttonOK.addSelectionListener(new SelectionAdapter() {
							public void widgetSelected(SelectionEvent arg0) {
								controls2Data();
								modalResult = SWT.OK;
								nowySzel.close();
							}
						});
				
						buttonCancel = new Button(nowySzel, SWT.CENTER);
						GridData gd_buttonCancel = new GridData(SWT.CENTER, SWT.CENTER, false,
								false, 1, 1);
						gd_buttonCancel.widthHint = 100;
						buttonCancel.setLayoutData(gd_buttonCancel);
						buttonCancel.setText("Anuluj");
						buttonCancel.addSelectionListener(new SelectionAdapter() {
							public void widgetSelected(SelectionEvent arg0) {
								controls2Data();
								modalResult = SWT.CANCEL;
								nowySzel.close();
							}
						});

	}

	public void startEventLoop() {
		while (!nowySzel.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	public int doModal() {
		nowySzel.open();
		// data2Controls();
		startEventLoop();
		return modalResult;
	}

	public void controls2Data() {
		setDaneFormularza(personelCombo.getText());
	}



}
