package com.mooo.rokicki.magazyn.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class MessageBoxesWarrning {
	
	Shell parent;
	
	public MessageBoxesWarrning(Shell parrent, String s) {
		this.parent=parrent;
		MessageBox mb = new MessageBox(parent, SWT.ICON_WARNING);
		mb.setText("Operacja niebezpieczna");
		mb.setMessage(s);
		mb.open();
	}
	
}
