package com.mooo.rokicki.magazyn.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class MessageBoxesError {
	
	Shell parent;
	
	public MessageBoxesError(Shell parrent, String s) {
		this.parent=parrent;
		MessageBox mb = new MessageBox(parent, SWT.ICON_ERROR);
		mb.setText("Błąd");
		mb.setMessage(s);
		mb.open();
	}
	
}
