package com.mooo.rokicki.magazyn.swt;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Combo;

import com.mooo.rokicki.magazyn.hql.SelectDostawca;
import com.mooo.rokicki.magazyn.util.ConvertList;

public class FormSearchDostawcaPeriod {
    private Shell shell;
    private Display display;

    private int modalResult = SWT.CANCEL;
    private Button buttonOK;
    private Button buttonCancel;
    private Label labelka;
    private Combo dostawcaCombo;
    private DateTime datemeBegin;
    private DateTime datemeEnd;
    private Integer[] timeBegin;
    private Integer[] timeEnd;
    private String dostawca;

    public FormSearchDostawcaPeriod(Shell parentShell, String title) {
        display = Display.getCurrent();
        shell = new Shell(parentShell, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        shell.setText(title);  
        init();
    }

    public void init() {
        shell.setSize(441, 340);
        
        Label labelEnd = new Label(shell, SWT.SHADOW_NONE | SWT.CENTER);
        labelEnd.setText("koniec okresu");
        labelEnd.setBounds(241, 75, 184, 15);
        
        Label labelBegin = new Label(shell, SWT.SHADOW_NONE | SWT.CENTER);
        labelBegin.setBounds(10, 74, 184, 15);
        labelBegin.setText("początek okresu");

        labelka = new Label(shell, SWT.BORDER | SWT.CENTER);
        labelka.setAlignment(SWT.CENTER);
        labelka.setBounds(10, 8, 415, 25);

        buttonOK = new Button(shell, SWT.PUSH);
        buttonOK.setBounds(139, 277, 70, 25);
        buttonOK.setText("OK");
        buttonOK.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
                controls2Data();
                modalResult = SWT.OK;
                shell.close();
            }
        });

        buttonCancel = new Button(shell, SWT.PUSH);
        buttonCancel.setBounds(232, 277, 70, 25);
        buttonCancel.setText("Anuluj");
        
        datemeBegin = new DateTime(shell, SWT.BORDER | SWT.CALENDAR);
        datemeBegin.setBounds(10, 86, 184, 169);
        
        dostawcaCombo = new Combo(shell, SWT.READ_ONLY);
        dostawcaCombo.setToolTipText("Dostawca");
        dostawcaCombo.setBounds(10, 45, 415, 23);
		String[] tabDostawca=ConvertList.listToArray(new SelectDostawca().listDostawca());
		dostawcaCombo.setItems(tabDostawca);        
		
		datemeEnd = new DateTime(shell, SWT.BORDER | SWT.CALENDAR);
		datemeEnd.setBounds(241, 86, 184, 169);
            
        buttonCancel.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
                controls2Data();
                modalResult = SWT.CANCEL;
                shell.close();
            }
        });

    }

    public void startEventLoop() {
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
    }

    public int doModal() {
        shell.open();
        startEventLoop();
        return modalResult;
    }

 
    public void controls2Data() {
    	setDostawca(dostawcaCombo.getText());
        setTimeBegin(new Integer[]{datemeBegin.getYear(), datemeBegin.getMonth(), datemeBegin.getDay()});
        setTimeEnd(new Integer[]{datemeEnd.getYear(), datemeEnd.getMonth(), datemeEnd.getDay()});
    }
    
    public void setLabel(String text) {
    	labelka.setText(text);
    }

	public Integer[] getTimeBegin() {
		return timeBegin;
	}

	public void setTimeBegin(Integer[] time) {
		this.timeBegin = time;
	}


	public Integer[] getTimEnd() {
		return timeEnd;
	}

	public void setTimeEnd(Integer[] time_end) {
		this.timeEnd = time_end;
	}
	

	public String getDostawca() {
		return dostawca;
	}

	public void setDostawca(String dostawca) {
		this.dostawca = dostawca;
	}	
}
