package com.mooo.rokicki.magazyn.swt;

import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

import com.mooo.rokicki.magazyn.db.Produkt;
import com.mooo.rokicki.magazyn.help.HelpPDF;
import com.mooo.rokicki.magazyn.hql.InsertPersonel;
import com.mooo.rokicki.magazyn.hql.InsertProdukt;
import com.mooo.rokicki.magazyn.hql.InsertKategoria;
import com.mooo.rokicki.magazyn.hql.InsertDostawca;
import com.mooo.rokicki.magazyn.hql.InsertFaktura;
import com.mooo.rokicki.magazyn.hql.InsertJednostka;
import com.mooo.rokicki.magazyn.hql.InsertHistoria;
import com.mooo.rokicki.magazyn.hql.SelectFaktura;
import com.mooo.rokicki.magazyn.hql.SelectHistoria;
import com.mooo.rokicki.magazyn.hql.SelectKategoria;
import com.mooo.rokicki.magazyn.hql.ListProdukt;
import com.mooo.rokicki.magazyn.hql.SelectPersonel;
import com.mooo.rokicki.magazyn.hql.SelectProdukt;
import com.mooo.rokicki.magazyn.hql.StanMagazyn;
import com.mooo.rokicki.magazyn.hql.UpdateFaktura;
import com.mooo.rokicki.magazyn.hql.UpdateHistoria;
import com.mooo.rokicki.magazyn.hql.UpdatePersonel;
import com.mooo.rokicki.magazyn.hql.UpdateProdukt;
import com.mooo.rokicki.magazyn.hql.UpdateDostawca;
import com.mooo.rokicki.magazyn.hql.UpdateJednostka;

import com.mooo.rokicki.magazyn.util.ConvertList;
import com.mooo.rokicki.magazyn.util.CheckInputData;
import com.mooo.rokicki.magazyn.util.EnumInsert;
import com.mooo.rokicki.magazyn.util.Folders;
import com.mooo.rokicki.magazyn.util.MessageErrors;
import com.mooo.rokicki.magazyn.util.MessageInfo;
import com.mooo.rokicki.magazyn.util.ConvertTime;
import com.mooo.rokicki.magazyn.util.SystemVariable;

import com.mooo.rokicki.magazyn.net.NazwaZlobka;
import com.mooo.rokicki.magazyn.pdf.PersonelPDF;
import com.mooo.rokicki.magazyn.pdf.ReportsPDF;
import com.mooo.rokicki.magazyn.pdf.OpenPDF;

public class MainMenu {

	private Shell parent;
	
	private Menu menuBar;
	private Menu menuDodaj;
	private Menu menuRozchod;
//	private Menu menuUstaw;
	private Menu menuRaport;
	private Menu menuPlik;
	private Menu menuEdytuj;
	private Menu menuPomoc;

	private MenuItem menuItemPlik;
	private MenuItem menuInicjalizacja;
	private MenuItem menuZakoncz;
	private MenuItem menuItemDodaj;
	private MenuItem menuItemDodajFaktura;
	private MenuItem menuItemDodajTowar;
	private MenuItem menuItemDodajKategoria;
	private MenuItem menuItemDodajDostawca;
	private MenuItem menuItemDodajPersonel;
	private MenuItem menuItemDodajUnit;
	private MenuItem menuItemRozchod;
	private MenuItem menuItemRozchodProdukt;
//	private MenuItem menuItemUstaw;
//	private MenuItem menuItemUstawFakturaDead;
	private MenuItem menuItemEdytuj;
	private MenuItem menuItemPowrotTowar;	
	private MenuItem menuItemPrzepiszTowar;	
	private MenuItem menuItemEdytujNazwaFaktura;	
	private MenuItem menuItemEdytujPozycjaFaktura;	
	private MenuItem menuItemEdytujDostawca;	
	private MenuItem menuItemEdytujPersonel;
	private MenuItem menuItemUsunPersonel;
	private MenuItem menuItemEdytujUnit;
	private MenuItem menuItemRaport;
	private MenuItem menuItemRaportStanMagazynu;
	private MenuItem menuItemRaportHistoriaFakturZaOkres;
	private MenuItem menuItemRaportPozycjeFaktury;
	private MenuItem menuItemRaportCoMaOsoba;
	private MenuItem menuItemRaportHistoriaProdukt;
	private MenuItem menuItemRaportListPerson;
	private MenuItem menuItemPomoc;
	private MenuItem menuItemPomocZmiany;
	private MenuItem menuItemPomocPDF;
	private TabsFreeStuff tabsfs;
	private TabsPeopleStuff tabsps;
	private Map<String, TableFreeStuff> map_free;
	private Map<String, TablePeopleStuff> map_people;
	
	void pack() {
		Point p = parent.getSize(); 
		parent.pack();
		parent.setSize(p);
	}


	public MainMenu(Shell shell) {

		parent = shell;
		
		tabsps = new TabsPeopleStuff(shell);
		tabsps.addTabItem(ConvertList.listToArray(new SelectKategoria().listStringKategoria()));
		map_people = tabsps.getMapa_tabelek();			
		
		tabsfs = new TabsFreeStuff(shell);
		tabsfs.addTabItem(ConvertList.listToArray(new SelectKategoria().listStringKategoria()));
		map_free = tabsfs.getMapa_tabelek();
		
		menuBar = new Menu(parent, SWT.BAR);
		parent.setMenuBar(menuBar);

		menuItemPlik = new MenuItem(menuBar, SWT.CASCADE);
		menuItemPlik.setText("Plik");

		menuPlik = new Menu(menuItemPlik);
		menuItemPlik.setMenu(menuPlik);

		menuInicjalizacja = new MenuItem(menuPlik, SWT.NONE);
		menuInicjalizacja.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {

					InsertPersonel ipe = new InsertPersonel();
					InsertJednostka ije = new InsertJednostka();
					Folders f = new Folders();
					ipe.saveUserSystem();
					ije.saveUnitDefault();
					f.makePdfFolder();
					MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.AKTUALIZACJA.getValue());
					
				}

		});

		menuInicjalizacja.setText("Inicjalizacja danych");		

		menuZakoncz = new MenuItem(menuPlik, SWT.NONE);
		menuZakoncz.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				parent.close();
			}
		});
		menuZakoncz.setText("Zakończ");

		menuItemDodaj = new MenuItem(menuBar, SWT.CASCADE);
		menuItemDodaj.setText("Dodaj");

		menuDodaj = new Menu(menuItemDodaj);
		menuItemDodaj.setMenu(menuDodaj);

		menuItemDodajTowar = new MenuItem(menuDodaj, SWT.NONE);
		menuItemDodajTowar.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormAddTowar form = new FormAddTowar(parent);
				if (form.doModal() == SWT.OK) {
					InsertProdukt ipr = new InsertProdukt();
					InsertHistoria iph = new InsertHistoria();
					if(!CheckInputData.isEmptyArrayString(form.getDaneFormularza())) {
						Integer id_produkt = ipr.saveProduktString(form.getDaneFormularza());
						iph.saveHistoryAdd(id_produkt, Integer.decode(form.getDaneFormularza()[1]), "system");
						TableFreeStuff tg_towar = map_free.get(form.getDaneFormularza()[EnumInsert.CATEGORY.getValue()]);					
						tg_towar.clearItems();
						tg_towar.addItems(form.getDaneFormularza()[EnumInsert.CATEGORY.getValue()]);
						MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.TOWAR_DODANY.getValue());
						pack();
					}
					else {
						MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.ALL_VALUE.getValue());
					}
				}
			}
		});
		menuItemDodajTowar.setText("Towar");

		menuItemDodajFaktura = new MenuItem(menuDodaj, SWT.NONE);
		menuItemDodajFaktura.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormAddFaktura form = new FormAddFaktura(parent);
				if (form.doModal() == SWT.OK) {
					InsertFaktura ifa = new InsertFaktura();
					if(!CheckInputData.isEmptyArrayString(form.getDaneFormularza())) {
						ifa.saveFaktura(form.getDaneFormularza());
						MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.FAKTURA_DODANA.getValue());
						pack();
					}
					else {
						MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.ONE_VALUE.getValue());
					}
				}
			}
		});
		menuItemDodajFaktura.setText("Fakturę");

		menuItemDodajKategoria = new MenuItem(menuDodaj, SWT.NONE);
		menuItemDodajKategoria.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormtAddStringHelp form = new FormtAddStringHelp(parent, "Dodaj kategorię");
				form.setLabel("np.: środki czystości");				
				if (form.doModal() == SWT.OK) {
					InsertKategoria ik = new InsertKategoria();
					String str = form.getTextValue();
					if(!str.isEmpty()) {
						if(ik.saveKategoria(str)!=0) {
							tabsfs.addTabItem(form.getTextValue());
							tabsps.addTabItem(form.getTextValue());
							MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.KATEGORIA_DODANA.getValue());
							pack();
						}
						else {
							MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.KATEGORIA_BYLA_DODANA.getValue());
						}
					}
					else {
						MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.ONE_VALUE.getValue());
					}						
				}
			}
		});

		menuItemDodajKategoria.setText("Kategorię towarową");

		menuItemDodajDostawca = new MenuItem(menuDodaj, SWT.NONE);
		menuItemDodajDostawca.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormtAddStringHelp form = new FormtAddStringHelp(parent,	"Dodaj dostawcę");
				form.setLabel("np.: Blumar SA");
				if (form.doModal() == SWT.OK) {
					InsertDostawca id = new InsertDostawca();
					String str = form.getTextValue();
					if(!str.isEmpty()) {
						if(id.saveDostawca(str)!=0) {
						MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.DOSTAWCA_DODANY.getValue());
						pack();
						}
						else {
							MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.DOSTAWCA_BYL_DODANY.getValue());
						}
					}
					else {
						MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.ONE_VALUE.getValue());
					}					
					
				}
			}
		});

		menuItemDodajDostawca.setText("Dostawcę");
		
		menuItemDodajPersonel = new MenuItem(menuDodaj, SWT.NONE);
		menuItemDodajPersonel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormtAddStringHelp form = new FormtAddStringHelp(parent,	"Dodaj osobę");
				form.setLabel("np.: Jan Kowalski");
				if (form.doModal() == SWT.OK) {
					InsertPersonel ip = new InsertPersonel();
					String str = form.getTextValue();
					if(!str.isEmpty()) {
						if(ip.savePersonel(str)!=0) {
						MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.PERSONEL_DODANY.getValue());
						pack();
						}
						else {
							MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.PERSONEL_BYL_DODANY.getValue());
						}
					}
					else {
						MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.ONE_VALUE.getValue());
					}					
					
				}
			}
		});

		menuItemDodajPersonel.setText("Osobę");
		
		menuItemDodajUnit = new MenuItem(menuDodaj, SWT.NONE);
		menuItemDodajUnit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormtAddStringHelp form = new FormtAddStringHelp(parent, "Dodaj jednostkę");
				form.setLabel("np.: 1.5 litra");
				if (form.doModal() == SWT.OK) {
					InsertJednostka ij = new InsertJednostka();
					String str = form.getTextValue();
					if(!str.isEmpty()) {
						if(ij.saveUnit(str)!=0) {
						MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.JEDNOSTKA_DODANA.getValue());
						pack();
						}
						else {
							MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.JEDNOSTKA_BYLA_DODANA.getValue());
						}						
					}
					else {
						MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.ONE_VALUE.getValue());
					}					
					
				}
			}
		});

		menuItemDodajUnit.setText("Jednostkę towaru");			

		menuItemRozchod = new MenuItem(menuBar, SWT.CASCADE);
		menuItemRozchod.setText("Rozchód");

		menuRozchod = new Menu(menuItemRozchod);
		menuItemRozchod.setMenu(menuRozchod);

		menuItemRozchodProdukt = new MenuItem(menuRozchod, SWT.NONE);
		menuItemRozchodProdukt.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormRozchodTowar form = new FormRozchodTowar(parent);
				if (form.doModal() == SWT.OK) {
					UpdateProdukt up = new UpdateProdukt();
					InsertHistoria ih = new InsertHistoria();
					if(form.getUpdate()) {
						ih.saveHistoryChange(form.getListaProduktow().get(0), form.getUpdateAmount()[1], form.getPerson());
						TableFreeStuff tfs = map_free.get(form.getListaProduktow().get(0).getKategoria().getName());
						tfs.clearItems();
						tfs.addItems(form.getListaProduktow().get(0).getKategoria().getId());
						TablePeopleStuff tps = map_people.get(form.getListaProduktow().get(0).getKategoria().getName());
						tps.clearItems();
						tps.addItems(form.getListaProduktow().get(0).getKategoria().getId());
						pack();

					}		
				}
			}
		});
		menuItemRozchodProdukt.setText("Rozchód towaru");
		
		menuItemEdytuj = new MenuItem(menuBar, SWT.CASCADE);
		menuItemEdytuj.setText("Edytuj");

		menuEdytuj = new Menu(menuItemEdytuj);
		menuItemEdytuj.setMenu(menuEdytuj);

		menuItemEdytujNazwaFaktura = new MenuItem(menuEdytuj, SWT.NONE);
		menuItemEdytujNazwaFaktura.setText("Zmień nazwę i dostawcę faktury");
		menuItemEdytujNazwaFaktura.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormEditFaktura form = new FormEditFaktura(parent);
				if (form.doModal() == SWT.OK) {
					UpdateFaktura uf = new UpdateFaktura();
					if(uf.setNewParametrFaktura(form.getDaneFormularza()) > 0) {
						MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.ZMIANA_FAKTURA.getValue());
						pack();
					}			
				}
			}
		});

		menuItemEdytujDostawca = new MenuItem(menuEdytuj, SWT.NONE);
		menuItemEdytujDostawca.setText("Zmień nazwę dostawcy");
		menuItemEdytujDostawca.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormEditDostawca form = new FormEditDostawca(parent);
				if (form.doModal() == SWT.OK) {
					UpdateDostawca ud = new UpdateDostawca();
					if(ud.setNameDostawca(form.getDaneFormularza()) > 0) {
						MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.ZMIANA_DOSTAWCA.getValue());
						pack();
					}			
				}
			}
		});
		
		menuItemEdytujPersonel = new MenuItem(menuEdytuj, SWT.NONE);
		menuItemEdytujPersonel.setText("Zmień imię i nazwisko osoby");
		menuItemEdytujPersonel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormEditPersonel form = new FormEditPersonel(parent);
				if (form.doModal() == SWT.OK) {
					UpdatePersonel up = new UpdatePersonel();
					if(up.setNamePersonel(form.getDaneFormularza()) > 0) {
						MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.ZMIANA_PERSONEL.getValue());
						pack();
					}			
				}
			}
		});			

		menuItemEdytujUnit = new MenuItem(menuEdytuj, SWT.NONE);
		menuItemEdytujUnit.setText("Zmień nazwę jednostki");
		menuItemEdytujUnit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormEditUnit form = new FormEditUnit(parent);
				if (form.doModal() == SWT.OK) {
					UpdateJednostka uu = new UpdateJednostka();
					if(uu.setNameUnit(form.getDaneFormularza()) > 0) {
						MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.ZMIANA_UNIT.getValue());
						pack();
					}			
				}
			}
		});	
		
		menuItemPrzepiszTowar = new MenuItem(menuEdytuj, SWT.NONE);
		menuItemPrzepiszTowar.setText("Przypisz towar innej osobie");
		menuItemPrzepiszTowar.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormEditOwnerItem form = new FormEditOwnerItem(parent);
				if (form.doModal() == SWT.OK) {
					UpdateHistoria uh = new UpdateHistoria();
					uh.updateOwner(form.getIdHistoria(), form.getOsoba());
					TablePeopleStuff tps = map_people.get(form.getKategoria());
					tps.clearItems();
					tps.addItems(form.getKategoria());
					MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.AKTUALIZACJA.getValue());
					pack();
					}
			}
		});	
		
		menuItemPowrotTowar = new MenuItem(menuEdytuj, SWT.NONE);
		menuItemPowrotTowar.setText("Przywróć do magazynu błędnie wydany towar");
		menuItemPowrotTowar.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormReturnItemToStore form = new FormReturnItemToStore(parent);
				if (form.doModal() == SWT.OK) {
					UpdateHistoria uh = new UpdateHistoria();
					uh.returnToStore(form.getIdHistoria());
					TablePeopleStuff tps = map_people.get(form.getKategoria());
					tps.clearItems();
					tps.addItems(form.getKategoria());
					TableFreeStuff tfs = map_free.get(form.getKategoria());
					tfs.clearItems();
					tfs.addItems(form.getKategoria());
					MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.AKTUALIZACJA.getValue());
					pack();
					}
			}
		});	
			
		menuItemEdytujPozycjaFaktura = new MenuItem(menuEdytuj, SWT.NONE);
		menuItemEdytujPozycjaFaktura.setText("Usuń pozycję z faktury");
		menuItemEdytujPozycjaFaktura.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormDeleteItemsFaktura form = new FormDeleteItemsFaktura(parent);
				if (form.doModal() == SWT.OK) {
					ListProdukt sp = new ListProdukt();
					List<Produkt> listp = form.getListaProduktow();
					List<String> listo= sp.listProduktOut(listp.get(0));
					if(listo.size()==0) {
						UpdateProdukt up = new UpdateProdukt();
						up.deleteProdukt(listp.get(0));
						TableFreeStuff tfs = map_free.get(form.getListaProduktow().get(0).getKategoria().getName());
						tfs.clearItems();
						tfs.addItems(form.getListaProduktow().get(0).getKategoria().getId());
						MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.AKTUALIZACJA.getValue());
						pack();
					}
					else{
						MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.PRODUKT_OUT.getValue());
					}
				}
			}
		});	
		
		menuItemUsunPersonel = new MenuItem(menuEdytuj, SWT.NONE);
		menuItemUsunPersonel.setText("Usuń personel");
		menuItemUsunPersonel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				MessageBoxesWarrning mw = new MessageBoxesWarrning(parent, MessageInfo.USUN_PRZYPISANIA_PERSONEL.getValue());
				FormDeletePersonel form = new FormDeletePersonel(parent);
				if (form.doModal() == SWT.OK) {
					UpdateHistoria uh = new UpdateHistoria();
					UpdatePersonel up = new UpdatePersonel();
					if(uh.deletetHistoriaBytOsoba(form.getDaneFormularza()) > 0) {
						if(up.deletePersonel(form.getDaneFormularza()) > 0) {
							
//							TablePeopleStuff tps = map_people.get(form.getKategoria());
//							tps.clearItems();
//							tps.addItems(form.getKategoria());
							MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.ZMIANA_PERSONEL.getValue());
							pack();
						}	
					}			
				}
			}
		});		
		
		menuItemRaport = new MenuItem(menuBar, SWT.CASCADE);
		menuItemRaport.setText("Raporty");

		menuRaport = new Menu(menuItemRaport);
		menuItemRaport.setMenu(menuRaport);

		menuItemRaportStanMagazynu = new MenuItem(menuRaport, SWT.NONE);
		menuItemRaportStanMagazynu.setText("Stan magazynu za okres");
		menuItemRaportStanMagazynu.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormStateStorePeriod form = new FormStateStorePeriod(parent, "Wybór okresu za który ma być wyliczony stan magazynu");
				form.setLabel("Wybór dnia stanu magazynowego");
				if (form.doModal() == SWT.OK) {
						ConvertTime ct = new ConvertTime();
						ReportsPDF rpdf = new ReportsPDF();
						OpenPDF opdf = new OpenPDF();
						StanMagazyn sm = new StanMagazyn();
						List<Object[]> his = sm.selectProduktWithAmountPeriod(ct.arrayIntegerToDateEnd(form.getTimEnd()));
						String s = ct.prettySimpleDate(ct.arrayIntToDate(form.getTimEnd()));
						opdf.openUri(rpdf.pdfStanMagazyn(SystemVariable.PATHPDF.getValue()+"StanMagazynu_"+new NazwaZlobka().getNazwa()+".pdf", his, s));		
				}
			}
		});		

//		
//		menuItemRaportStanMagazynu.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent arg0) {
//				FormStateStoreLong fatd = new FormStateStoreLong(parent, "Wybór dnia");
//				fatd.setLabel("Wybór dnia stanu magazynowego");
//				if (fatd.doModal() == SWT.OK) {
//					if(fatd.getTime().length==3) {
//							ConvertTime ct = new ConvertTime();
//							MakePDF rpdf = new MakePDF();
//							OpenPDF opdf = new OpenPDF();
//							StanMagazyn sp = new StanMagazyn();
//							List<Object[]> his = sp.listProduktWithAmountToDay(ct.arrayIntegerToDateEnd(fatd.getTime()));
//							opdf.openUri(rpdf.pdfStanMagazyn("c:/Program Files/magazyn/pdf/StanMagazynu.pdf", his));
//
//					
////						MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.AKTUALIZACJA.getValue());
//					}
//					
//				}
//			}
//		});			
		
		menuItemRaportHistoriaFakturZaOkres = new MenuItem(menuRaport, SWT.NONE);
		menuItemRaportHistoriaFakturZaOkres.setText("Historia faktur dostawcy za okres");
		menuItemRaportHistoriaFakturZaOkres.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormSearchDostawcaPeriod form = new FormSearchDostawcaPeriod(parent, "Wyszukiwanie faktur");
				form.setLabel("Wybór dostawcy i zakresu wyszukiwania");
				if (form.doModal() == SWT.OK) {
					if(!form.getDostawca().isEmpty() && form.getTimeBegin().length==3) {
							ConvertTime ct = new ConvertTime();
							ReportsPDF rpdf = new ReportsPDF();
							OpenPDF opdf = new OpenPDF();
							SelectFaktura sf = new SelectFaktura();
							List<Object[]> lobj = sf.selectFakturaByDostawcaPeriod(ct.arrayIntegerToDateBegin(form.getTimeBegin()), ct.arrayIntegerToDateEnd(form.getTimEnd()), form.getDostawca());
							opdf.openUri(rpdf.pdfHistoriaFaktur(SystemVariable.PATHPDF.getValue()+"HistoriaFaktur_"+form.getDostawca()+".pdf", lobj));
					}
					else {
						MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.ONE_VALUE.getValue());					
					}
				}
			}
		});	
		
		menuItemRaportPozycjeFaktury = new MenuItem(menuRaport, SWT.NONE);
		menuItemRaportPozycjeFaktury.setText("Pozycje faktury oraz ich stany początkowe");
		menuItemRaportPozycjeFaktury.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormViewFaktura form = new FormViewFaktura(parent);
				if (form.doModal() == SWT.OK) {
					if(!form.getDaneFormularza().isEmpty()) {
							ReportsPDF rpdf = new ReportsPDF();
							OpenPDF opdf = new OpenPDF();
							SelectProdukt sp = new SelectProdukt();
							List<Object[]> lobj = sp.selectProduktByDostawcaFaktura(form.getDaneFormularza());						
							if(lobj.size()>0) {
								opdf.openUri(
										rpdf.pdfPozycjeFaktura(SystemVariable.PATHPDF.getValue()+"PozycjeFaktury-"+form.getDaneFormularza().split("<>")[0]+"_"+form.getDaneFormularza().split("<>")[1]+".pdf", lobj)
										);
							}
							else {
								MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.NO_ITEM_BILL.getValue());
							}
					}
					else {
						MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.ONE_VALUE.getValue());
					}
				}
			}
		});			

		menuItemRaportCoMaOsoba = new MenuItem(menuRaport, SWT.NONE);
		menuItemRaportCoMaOsoba.setText("Co zostało przydzielone danej osobie");
		menuItemRaportCoMaOsoba.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormSearchProduktsPersonPeriod form = new FormSearchProduktsPersonPeriod(parent, "Wyszukiwanie produktów przydzielonych danej osobie");
				form.setLabel("Wybór osoby i zakresu wyszukiwania");
				if (form.doModal() == SWT.OK) {
					if(!form.getOsoba().isEmpty() && form.getTimeBegin().length==3) {
							ConvertTime ct = new ConvertTime();
							ReportsPDF rpdf = new ReportsPDF();
							OpenPDF opdf = new OpenPDF();
							SelectProdukt sp = new SelectProdukt();
							List<Object[]> lobj = sp.selectProduktByPersonelPeriod(ct.arrayIntegerToDateBegin(form.getTimeBegin()), ct.arrayIntegerToDateEnd(form.getTimEnd()), form.getOsoba());									
							opdf.openUri(rpdf.pdfCoMaOsoba(SystemVariable.PATHPDF.getValue()+"ProduktyOsoby_"+form.getOsoba()+".pdf", lobj));
					}
					else {
						MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.ONE_VALUE.getValue());					
					}
				}
			}
		});	
		
		menuItemRaportHistoriaProdukt = new MenuItem(menuRaport, SWT.NONE);
		menuItemRaportHistoriaProdukt.setText("Wyświetl historię danego towaru");
		menuItemRaportHistoriaProdukt.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormSearchHistoriaTowar form = new FormSearchHistoriaTowar(parent);
				if (form.doModal() == SWT.OK) {
					List<Produkt> lp = form.getListaProduktow();
					if(lp!=null) {
						SelectHistoria sh = new SelectHistoria();
						List<Object[]> lo = sh.selectHistoriaByProdukt(lp.get(0).getId());						
						ReportsPDF rpdf = new ReportsPDF();
						OpenPDF opdf = new OpenPDF();
						opdf.openUri(rpdf.pdfHistoriaProdukt(SystemVariable.PATHPDF.getValue()+"HistoriaProduktu"+".pdf", lo));						
					}	
				}
			}
		});

		menuItemRaportListPerson = new MenuItem(menuRaport, SWT.NONE);
		menuItemRaportListPerson.setText("Lista osób zarejestrowanych w magazynie");
		menuItemRaportListPerson.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				PersonelPDF rpdf = new PersonelPDF();
				OpenPDF opdf = new OpenPDF();
				opdf.openUri(rpdf.pdfLista(SystemVariable.PATHPDF.getValue()+"ListaOsob"+".pdf", ConvertList.listToArray(new SelectPersonel().listPersonel())));					
				}
		});			
		
//				FormSearchProduktsPersonHelp fspph = new FormSearchProduktsPersonHelp(parent, "Wyszukiwanie produktów przydzielonych danej osobie");
//				fspph.setLabel("Wybór osoby i zakresu wyszukiwania");
//				if (fspph.doModal() == SWT.OK) {
//					if(!fspph.getOsoba().isEmpty() && fspph.getTime().length==3) {
//							ConvertTime ct = new ConvertTime();
//							MakePDF rpdf = new MakePDF();
//							OpenPDF opdf = new OpenPDF();
//							SelectProdukt sp = new SelectProdukt();
//							List<Object[]> lobj = sp.listProduktByPersonel(ct.arrayFormToDate(fspph.getTime()), fspph.getOsoba());									
//							opdf.openUri(rpdf.pdfCoMaOsoba("c:/Program Files/magazyn/pdf/ProduktyOsoby_"+fspph.getOsoba()+".pdf", lobj));
//					}
//					else {
//						MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.ONE_VALUE.getValue());					
//					}
//				}

		menuItemPomoc = new MenuItem(menuBar, SWT.CASCADE);
		menuItemPomoc.setText("Pomoc");

		menuPomoc = new Menu(menuItemPomoc);
		menuItemPomoc.setMenu(menuPomoc);

		menuItemPomocPDF = new MenuItem(menuPomoc, SWT.NONE);
		menuItemPomocPDF.setText("Otwórz pomoc");		
		menuItemPomocPDF.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				OpenPDF opdf = new OpenPDF(); 
				HelpPDF h = new HelpPDF();
				opdf.openUri(h.make(SystemVariable.PATHPDF.getValue()+"pomoc_do_magazynu.pdf"));
			}
		});	
		
		
		menuItemPomocZmiany = new MenuItem(menuPomoc, SWT.NONE);
		menuItemPomocZmiany.setText("Historia zmian");		
		menuItemPomocZmiany.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FormShowVersion form = new FormShowVersion(parent, "Historia zmian w programie");
				if (form.doModal() == SWT.OK) {
				}
			}
		});			
		
//		menuItemUstaw = new MenuItem(menuBar, SWT.CASCADE);
//		menuItemUstaw.setText("Ustaw");
//
//		menuUstaw = new Menu(menuItemUstaw);
//		menuItemUstaw.setMenu(menuUstaw);
//
//		menuItemUstawFakturaDead = new MenuItem(menuUstaw, SWT.NONE);
//		menuItemUstawFakturaDead.setText("Ustaw fakturę jako rozliczoną");
//		menuItemUstawFakturaDead.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent arg0) {
//				FormSetDeadFaktura fsdf = new FormSetDeadFaktura(parent);
//				if (fsdf.doModal() == SWT.OK) {
//					UpdateFaktura uf = new UpdateFaktura();
//					if(uf.setDeadFaktura(fsdf.getDaneFormularza()) > 0) {
//						MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.FAKTURA_ROZLICZ.getValue());
//					}
////					tabsfs.updateTable("obuwie");
////					//działa stąd, bo mamy obiekt ref tabsfs
//				}
//			}
//		});
		
		
	}
}
