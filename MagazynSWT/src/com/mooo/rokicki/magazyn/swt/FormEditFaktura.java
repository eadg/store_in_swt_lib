package com.mooo.rokicki.magazyn.swt;


import org.eclipse.swt.SWT;

import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.mooo.rokicki.magazyn.hql.SelectDostawca;
import com.mooo.rokicki.magazyn.hql.SelectFaktura;
import com.mooo.rokicki.magazyn.util.ConvertList;

import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.graphics.Point;


public class FormEditFaktura {
	
	private Shell nowySzel;
	private Display display;
	private Shell parent;

	private int modalResult = SWT.CANCEL;
	private Label fakturaLabel;	
	private Combo fakturaCombo;	
	
	private Button buttonOK;
	private Button buttonCancel;
	
	private String[] daneFormularza;
	
	private final FormToolkit formToolkit = new FormToolkit(
			Display.getDefault());
	private Label labelNowaNazwa;
	private Text fakturaNewText;
	private Label labelDostawca;
	private Combo dostawcaNewCombo;

	public FormEditFaktura(Shell parentShell) {
		display = Display.getCurrent();
		parent = parentShell;
		nowySzel = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		nowySzel.setMinimumSize(new Point(132, 50));
		init();
	}

	public void init() {
		nowySzel.setSize(370, 216);
		nowySzel.setText("Zmiana parametrów faktury");
		nowySzel.setLayout(new GridLayout(2, false));
		
		fakturaLabel = new Label(nowySzel, SWT.NONE);
		fakturaLabel.setAlignment(SWT.CENTER);
		GridData gd_fakturaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_fakturaLabel.widthHint = 140;
		fakturaLabel.setLayoutData(gd_fakturaLabel);
		formToolkit.adapt(fakturaLabel, true, true);
		fakturaLabel.setText("Wybierz fakturę do edycji");
		
		fakturaCombo = new Combo(nowySzel, SWT.READ_ONLY);
		GridData gd_fakturaCombo = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_fakturaCombo.widthHint = 148;
		fakturaCombo.setLayoutData(gd_fakturaCombo);
		String[] tabFaktura=new SelectFaktura().toArrayString();		
		fakturaCombo.setItems(tabFaktura);
		
		
		formToolkit.adapt(fakturaCombo);
		formToolkit.paintBordersFor(fakturaCombo);
		
		labelNowaNazwa = new Label(nowySzel, SWT.NONE);
		labelNowaNazwa.setAlignment(SWT.CENTER);
		GridData gd_lblNowaNazwa = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblNowaNazwa.widthHint = 156;
		labelNowaNazwa.setLayoutData(gd_lblNowaNazwa);
		formToolkit.adapt(labelNowaNazwa, true, true);
		labelNowaNazwa.setText("Nowa nazwa faktury");
		
		fakturaNewText = new Text(nowySzel, SWT.BORDER);
		fakturaNewText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		formToolkit.adapt(fakturaNewText, true, true);
				
				labelDostawca = new Label(nowySzel, SWT.NONE);
				labelDostawca.setAlignment(SWT.CENTER);
				labelDostawca.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
				formToolkit.adapt(labelDostawca, true, true);
				labelDostawca.setText("Wybierz dostawcę");
				
				dostawcaNewCombo = new Combo(nowySzel, SWT.READ_ONLY);
				dostawcaNewCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
				String[] tabDostawca=ConvertList.listToArray(new SelectDostawca().listDostawca());
				dostawcaNewCombo.setItems(tabDostawca);				
				formToolkit.adapt(dostawcaNewCombo);
				formToolkit.paintBordersFor(dostawcaNewCombo);
				new Label(nowySzel, SWT.NONE);
				new Label(nowySzel, SWT.NONE);
				
						buttonOK = new Button(nowySzel, SWT.PUSH);
						GridData gd_buttonOK = new GridData(SWT.LEFT, SWT.CENTER, false, false,	1, 1);
						gd_buttonOK.widthHint = 102;
						buttonOK.setLayoutData(gd_buttonOK);
						buttonOK.setText("OK");
						buttonOK.addSelectionListener(new SelectionAdapter() {
							public void widgetSelected(SelectionEvent arg0) {
								controls2Data();
								modalResult = SWT.OK;
								nowySzel.close();
							}
						});
				
						buttonCancel = new Button(nowySzel, SWT.CENTER);
						GridData gd_buttonCancel = new GridData(SWT.CENTER, SWT.CENTER, false,
								false, 1, 1);
						gd_buttonCancel.widthHint = 102;
						buttonCancel.setLayoutData(gd_buttonCancel);
						buttonCancel.setText("Anuluj");
						buttonCancel.addSelectionListener(new SelectionAdapter() {
							public void widgetSelected(SelectionEvent arg0) {
								controls2Data();
								modalResult = SWT.CANCEL;
								nowySzel.close();
							}
						});

	}

	public void startEventLoop() {
		while (!nowySzel.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	public int doModal() {
		nowySzel.open();
		// data2Controls();
		startEventLoop();
		return modalResult;
	}

	public void controls2Data() {
		setDaneFormularza(new String[]{ 
				fakturaCombo.getText(), fakturaNewText.getText().trim(), dostawcaNewCombo.getText()
				});
	}

	public String[] getDaneFormularza() {
		return daneFormularza;
	}

	public void setDaneFormularza(String[] daneFormularza) {
		this.daneFormularza = daneFormularza;
	}




}
