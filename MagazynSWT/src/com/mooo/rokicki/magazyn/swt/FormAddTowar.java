package com.mooo.rokicki.magazyn.swt;


import org.eclipse.swt.SWT;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.swt.events.KeyAdapter;

import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.mooo.rokicki.magazyn.hql.SelectFaktura;
import com.mooo.rokicki.magazyn.hql.SelectJednostka;
import com.mooo.rokicki.magazyn.hql.SelectKategoria;
import com.mooo.rokicki.magazyn.hql.SelectProdukt;
import com.mooo.rokicki.magazyn.util.ConvertList;
import com.mooo.rokicki.magazyn.util.SwtUtil;;


public class FormAddTowar {
	
	private Shell nowySzel;
	private Display display;
	private Shell parent;

	private int modalResult = SWT.CANCEL;

	private Text produktText;
	private Text stanText;
	private Text cenaText;
	private Label produktLabel;
	private Label stanLabel;
	private Label cenaLabel;
	private Label fakturaLabel;	
	private Label kategoriaLabel;
	private Label dostawcaLabelLeft;
	private Label dostawcaLabelRight;
	private Label jednostkaLabel;
	private Combo fakturaCombo;
	private Combo jednostkaCombo;
	private Combo kategoriaCombo;
	
	private Button buttonOK;
	private Button buttonCancel;
	
	private String[] daneFormularza;
	
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	
	public FormAddTowar(Shell parentShell) {
		display = Display.getCurrent();
		parent = parentShell;
		nowySzel = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		nowySzel.setToolTipText("szybkie wpisanie: Lewy Ctrl+Spacja");
		init();
	}

	public void init() {
		nowySzel.setSize(423, 281);
		nowySzel.setText("Dodaj nowy produkt");
		nowySzel.setLayout(new GridLayout(2, false));
		
		fakturaLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_fakturaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_fakturaLabel.widthHint = 103;
		fakturaLabel.setLayoutData(gd_fakturaLabel);
		formToolkit.adapt(fakturaLabel, true, true);
		fakturaLabel.setText("Faktura");
		
		fakturaCombo = new Combo(nowySzel, SWT.READ_ONLY);
		fakturaCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		String[] tabFaktura=new SelectFaktura().toArrayString();		
		fakturaCombo.setItems(tabFaktura);		
		formToolkit.adapt(fakturaCombo);
		formToolkit.paintBordersFor(fakturaCombo);
		fakturaCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				dostawcaLabelRight.setText(fakturaCombo.getText().split("<>")[0]);
			}
		});		

		produktLabel = new Label(nowySzel, SWT.NONE);
		produktLabel.setToolTipText("szybkie wpisanie: Lewy Ctrl+Spacja");
		GridData gd_produktLabel = new GridData(SWT.FILL, SWT.CENTER, false,	false, 1, 1);
		gd_produktLabel.widthHint = 102;
		produktLabel.setLayoutData(gd_produktLabel);
		formToolkit.adapt(produktLabel, true, true);
		produktLabel.setText("Nazwa produktu");

		produktText = new Text(nowySzel, SWT.BORDER | SWT.SINGLE);
		produktText.setToolTipText("szybkie wpisanie: Lewy Ctrl+Spacja");
		GridData gd_text0 = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,	1);
		gd_text0.widthHint = 165;
		
		produktText.setLayoutData(gd_text0);
		produktText.addKeyListener(new KeyAdapter() {
			@Override
            public void keyReleased(KeyEvent ke) {
         			String[] produkty = ConvertList.listObjectToArrayString(new SelectProdukt().selectAllProdukt());
         			new SwtUtil().addAutoComplete(produktText, produkty);
            }
        });

		stanLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_stanLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_stanLabel.widthHint = 103;
		stanLabel.setLayoutData(gd_stanLabel);
		formToolkit.adapt(stanLabel, true, true);
		stanLabel.setText("Ilość");

		stanText = new Text(nowySzel, SWT.BORDER);
		GridData gd_stanText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_stanText.widthHint = 133;
		stanText.setLayoutData(gd_stanText);
		formToolkit.adapt(stanText, true, true);
		
		cenaLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_cenaLabel = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_cenaLabel.widthHint = 103;
		cenaLabel.setLayoutData(gd_cenaLabel);
		formToolkit.adapt(cenaLabel, true, true);
		cenaLabel.setText("Cena");

		cenaText = new Text(nowySzel, SWT.BORDER);
		GridData gd_cenaText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_cenaText.widthHint = 133;
		cenaText.setLayoutData(gd_cenaText);
		formToolkit.adapt(cenaText, true, true);		
		
		jednostkaLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_jednostkaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_jednostkaLabel.widthHint = 100;
		jednostkaLabel.setLayoutData(gd_jednostkaLabel);
		formToolkit.adapt(jednostkaLabel, true, true);
		jednostkaLabel.setText("Jednostka miary");
		jednostkaCombo = new Combo(nowySzel, SWT.READ_ONLY);
		String[] tabJednostka=ConvertList.listToArray(new SelectJednostka().listUnit());		
		jednostkaCombo.setItems(tabJednostka);				
		jednostkaCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		formToolkit.adapt(jednostkaCombo);
		formToolkit.paintBordersFor(jednostkaCombo);

		kategoriaLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_kategoriaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_kategoriaLabel.widthHint = 102;
		kategoriaLabel.setLayoutData(gd_kategoriaLabel);
		formToolkit.adapt(kategoriaLabel, true, true);
		kategoriaLabel.setText("Kategoria");
		
		kategoriaCombo = new Combo(nowySzel, SWT.READ_ONLY);
		kategoriaCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		String[] tabKategoria=ConvertList.listToArray(new SelectKategoria().listStringKategoria());
		kategoriaCombo.setItems(tabKategoria);
		formToolkit.adapt(kategoriaCombo);
		formToolkit.paintBordersFor(kategoriaCombo);

		dostawcaLabelLeft = new Label(nowySzel, SWT.NONE);
		GridData gd_dostawcaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_dostawcaLabel.heightHint = 18;
		gd_dostawcaLabel.widthHint = 103;
		dostawcaLabelLeft.setLayoutData(gd_dostawcaLabel);
		formToolkit.adapt(dostawcaLabelLeft, true, true);
		dostawcaLabelLeft.setText("Dostawca");
		
		dostawcaLabelRight = new Label(nowySzel, SWT.NONE);
		GridData gd_dos = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_dos.heightHint = 18;
		gd_dos.widthHint = 133;
		dostawcaLabelRight.setLayoutData(gd_dos);
		formToolkit.adapt(dostawcaLabelRight, true, true);
		
		
		new Label(nowySzel, SWT.NONE);
		new Label(nowySzel, SWT.NONE);

		buttonOK = new Button(nowySzel, SWT.PUSH);
		GridData gd_buttonOK = new GridData(SWT.CENTER, SWT.CENTER, false, false,	1, 1);
		gd_buttonOK.widthHint = 114;
		buttonOK.setLayoutData(gd_buttonOK);
		buttonOK.setText("OK");
		buttonOK.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				controls2Data();
				modalResult = SWT.OK;
				nowySzel.close();
			}
		});

		buttonCancel = new Button(nowySzel, SWT.CENTER);
		GridData gd_buttonCancel = new GridData(SWT.CENTER, SWT.CENTER, true,
				false, 1, 1);
		gd_buttonCancel.widthHint = 102;
		buttonCancel.setLayoutData(gd_buttonCancel);
		buttonCancel.setText("Anuluj");
		buttonCancel.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				controls2Data();
				modalResult = SWT.CANCEL;
				nowySzel.close();
			}
		});

	}

	public void startEventLoop() {
		while (!nowySzel.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	public int doModal() {
		nowySzel.open();
		// data2Controls();
		startEventLoop();
		return modalResult;
	}

	public void controls2Data() {
		
		String faktura="";
		if(!fakturaCombo.getText().isEmpty()) {
			faktura=fakturaCombo.getText().split("<>")[1];
		}
		
	
		setDaneFormularza(new String[]{
				produktText.getText().trim(), 
				stanText.getText().trim(),
//				dostawcaCombo.getText(),
				faktura,
				jednostkaCombo.getText(),
				kategoriaCombo.getText(),
				cenaText.getText().trim().replace(',', '.')
				});
	}

	public String[] getDaneFormularza() {
		return daneFormularza;
	}

	public void setDaneFormularza(String[] daneFormularza) {
		this.daneFormularza = daneFormularza;
	}
}
