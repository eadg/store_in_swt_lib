package com.mooo.rokicki.magazyn.swt;

import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.layout.GridLayout;

public class TabsPeopleStuff {

	private TabFolder tabfol;
	private Table table;
	private Shell parent;
	private TabItem tabitem;
	private TablePeopleStuff tableGener;
	private Group grp;	
	private Map<String, TablePeopleStuff> mapa_tabelek;
	
	public TabsPeopleStuff(Shell shell) {

		parent = shell;
		grp = new Group(parent, SWT.NONE);
		grp.setText("Przydzielone środki");
		grp.setLayout(new GridLayout(1, false));
		setTabfol(new TabFolder(grp, SWT.BAR));
		mapa_tabelek = EnumMapPeopleStuff.MAPA.getSingiel();
	}

	public Map<String, TablePeopleStuff> getMapa_tabelek() {
		return mapa_tabelek;
	}

	public void setMapa_tabelek(Map<String, TablePeopleStuff> mapa_tabelek) {
		this.mapa_tabelek = mapa_tabelek;
	}

	
	public TabFolder getTabfol() {
		return tabfol;
	}

	public void setTabfol(TabFolder tabfol) {
		this.tabfol = tabfol;
	}

	public void addTabItem(String[] kategorie) {
		
//		mapa_tabelek = new HashMap<String, TablePeopleStuff>();
		for (int ile = 0; ile < kategorie.length; ile++) {
			tabitem = new TabItem(tabfol, SWT.NONE);
			tabitem.setText(kategorie[ile]);
			table = new Table(tabfol, SWT.BORDER | SWT.FULL_SELECTION);
			table.setHeaderVisible(true);
			table.setLinesVisible(true);
			tableGener = new TablePeopleStuff(table);
			tableGener.addingColumn();
			tableGener.addItems(kategorie[ile]);
			tabitem.setControl(table);
			mapa_tabelek.put(kategorie[ile], tableGener);
		}
		//tabfol.pack();
	}

	public void addTabItem(String kategoria) {
		tabitem = new TabItem(tabfol, SWT.NONE);
		tabitem.setText(kategoria);
		table = new Table(tabfol, SWT.BORDER | SWT.FULL_SELECTION);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		tableGener = new TablePeopleStuff(table);
		tableGener.addingColumn();
		tableGener.addItems(kategoria);
		tabitem.setControl(table);
		mapa_tabelek.put(kategoria, tableGener);
	}

	public void removeAllTabItem() {
	}
}