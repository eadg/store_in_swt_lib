package com.mooo.rokicki.magazyn.swt;


import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import com.mooo.rokicki.magazyn.hql.SelectPersonel;

import com.mooo.rokicki.magazyn.hql.ListProdukt;
import com.mooo.rokicki.magazyn.hql.SelectKategoria;
import com.mooo.rokicki.magazyn.hql.HqlEditOwnerItem;
import com.mooo.rokicki.magazyn.util.ConvertList;
import com.mooo.rokicki.magazyn.util.MessageErrors;
import com.mooo.rokicki.magazyn.util.Uniq;
import com.mooo.rokicki.magazyn.db.Personel;

public class FormEditOwnerItem {

	private Shell shell;
	private Display display;
	private Shell parent;

	private int modalResult = SWT.CANCEL;
	private Label kategoriaLabel;	
	private Label fakturaLabel;
	private Label produktLabel;
	private Combo kategoriaCombo;
	private Combo fakturaCombo;
	private Combo produktCombo;
	private Combo personelCombo;
	private Button buttonOK;
	private Button buttonCancel;
	private Label personelLabel;
	private Label amountLabel;
	private Combo wlascicielObecnyCombo;
	private Combo amountCombo;
	private Label dataLabel;
	private Combo dataCombo;
	private Label wlascicielObecnyLabel;	
	
	private Personel osoba;
	private Integer idHistoria;
	private String kategoria;

	private HashMap<String, Integer> map;

	private final FormToolkit formToolkit = new FormToolkit(
			Display.getDefault());

	public FormEditOwnerItem(Shell parentShell) {
		display = Display.getCurrent();
		parent = parentShell;
		shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setToolTipText("Ustawienie nowego właściela dla wydanego towaru.");
		init();
	}

	public void init() {
		
		shell.setSize(363, 301);
		shell.setText("Ustawienie nowego właściela dla wydanego towaru");
		shell.setLayout(new GridLayout(2, false));
		final HqlEditOwnerItem heoi = new HqlEditOwnerItem();

		kategoriaLabel = new Label(shell, SWT.NONE);
		GridData gd_kategoriaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_kategoriaLabel.widthHint = 115;
		kategoriaLabel.setLayoutData(gd_kategoriaLabel);
		formToolkit.adapt(kategoriaLabel, true, true);
		kategoriaLabel.setText("Kategoria");
		
		kategoriaCombo = new Combo(shell, SWT.READ_ONLY);
		kategoriaCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,	false, 1, 1));
		kategoriaCombo.setItems(ConvertList.listToArray(new SelectKategoria().listStringKategoria()));
		kategoriaCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				ListProdukt lp = new ListProdukt(); 
				fakturaCombo.setItems(Uniq.arrayToUniqArray(lp.arrayFakturaByKategoria(kategoriaCombo.getText())));
					produktCombo.setItems(new String[0]);
					wlascicielObecnyCombo.setItems(new String[0]);
					amountCombo.setItems(new String[0]);
					dataCombo.setItems(new String[0]);
			}
		});

		formToolkit.adapt(kategoriaCombo);
		formToolkit.paintBordersFor(kategoriaCombo);		
		

		fakturaLabel = new Label(shell, SWT.NONE);
		GridData gd_fakturaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_fakturaLabel.widthHint = 131;
		fakturaLabel.setLayoutData(gd_fakturaLabel);
		formToolkit.adapt(fakturaLabel, true, true);
		fakturaLabel.setText("Faktura");

		fakturaCombo = new Combo(shell, SWT.READ_ONLY);
		fakturaCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,	false, 1, 1));
		fakturaCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				produktCombo.setItems(ConvertList.listObjectToArrayString(heoi.selectProduktByFakturaKategoria(fakturaCombo.getText().split("<>")[1], kategoriaCombo.getText())));
				wlascicielObecnyCombo.setItems(new String[0]);
				amountCombo.setItems(new String[0]);
				dataCombo.setItems(new String[0]);
			}
		});
		formToolkit.adapt(fakturaCombo);
		formToolkit.paintBordersFor(fakturaCombo);

		produktLabel = new Label(shell, SWT.NONE);
		GridData gd_jednostkaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_jednostkaLabel.widthHint = 135;
		produktLabel.setLayoutData(gd_jednostkaLabel);
		formToolkit.adapt(produktLabel, true, true);
		produktLabel.setText("Wybór produktu");

		produktCombo = new Combo(shell, SWT.READ_ONLY);
		produktCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,	false, 1, 1));
		produktCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				wlascicielObecnyCombo.setItems(ConvertList.listObjectToArrayString(heoi.selectHistoriaByProdukt(produktCombo.getText())));
				amountCombo.setItems(new String[0]);
				dataCombo.setItems(new String[0]);
			}
		});
		formToolkit.adapt(produktCombo);
		formToolkit.paintBordersFor(produktCombo);
		
		wlascicielObecnyLabel = new Label(shell, SWT.NONE);
		wlascicielObecnyLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		formToolkit.adapt(wlascicielObecnyLabel, true, true);
		wlascicielObecnyLabel.setText("Obecny właściciel");
		
		wlascicielObecnyCombo = new Combo(shell, SWT.READ_ONLY);
		wlascicielObecnyCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		wlascicielObecnyCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				amountCombo.setItems(ConvertList.listArrayStringToArrayString(ConvertList.listArrayObjectToListArrayString(heoi.selectHistoriaByProduktOsoba(produktCombo.getText(), wlascicielObecnyCombo.getText()))));
				dataCombo.setItems(new String[0]);
			}
		});
		formToolkit.adapt(wlascicielObecnyCombo);
		formToolkit.paintBordersFor(wlascicielObecnyCombo);
		
		amountLabel = new Label(shell, SWT.NONE);
		amountLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		formToolkit.adapt(amountLabel, true, true);
		amountLabel.setText("Ilość jednostek");
		
		amountCombo = new Combo(shell, SWT.READ_ONLY);
		amountCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		amountCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
					Integer ile = Integer.decode(amountCombo.getText().split(" ")[0])*(-1);
					map = heoi.listArrayObjectToMap(heoi.selectHistoriaByProduktOsobaIlosc(produktCombo.getText(), wlascicielObecnyCombo.getText(), ile));
					String[] key_date = new String[map.size()];
					key_date = map.keySet().toArray(key_date); 					
					dataCombo.setItems(key_date);
			}
		});
		formToolkit.adapt(amountCombo);
		formToolkit.paintBordersFor(amountCombo);
		
		dataLabel = new Label(shell, SWT.NONE);
		dataLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		formToolkit.adapt(dataLabel, true, true);
		dataLabel.setText("Data rozchodu");
		
		dataCombo = new Combo(shell, SWT.READ_ONLY);
		dataCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		dataCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				setIdHistoria(map.get(dataCombo.getText()));
			}
		});
		
		formToolkit.adapt(dataCombo);
		formToolkit.paintBordersFor(dataCombo);
		
		personelLabel = new Label(shell, SWT.NONE);
		personelLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		formToolkit.adapt(personelLabel, true, true);
		personelLabel.setText("Ustaw nowego właściciela");
		
		personelCombo = new Combo(shell, SWT.READ_ONLY);
		personelCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		String[] tabPerson=ConvertList.listToArray(new SelectPersonel().listPersonel());
		personelCombo.setItems(tabPerson);
		personelCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				SelectPersonel sper = new SelectPersonel(); 
				setOsoba(sper.listPersonelString(personelCombo.getText()).get(0));
			}
		});
		formToolkit.adapt(personelCombo);
		formToolkit.paintBordersFor(personelCombo);
		new Label(shell, SWT.NONE);
				new Label(shell, SWT.NONE);
		
				buttonOK = new Button(shell, SWT.PUSH);
				GridData gd_buttonOK = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
				gd_buttonOK.widthHint = 125;
				buttonOK.setLayoutData(gd_buttonOK);
				buttonOK.setText("OK");
				buttonOK.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent arg0) {
						controls2Data();
						modalResult = SWT.OK;
						shell.close();
					}
				});
		
				buttonCancel = new Button(shell, SWT.NONE);
				GridData gd_buttonCancel = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
				gd_buttonCancel.widthHint = 120;
				buttonCancel.setLayoutData(gd_buttonCancel);
				buttonCancel.setText("Anuluj");
				buttonCancel.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent arg0) {
						modalResult = SWT.CANCEL;
						shell.close();
					}
				});
	}

	public void startEventLoop() {
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	public int doModal() {
		shell.open();
		startEventLoop();
		return modalResult;
	}

	public void controls2Data() {
		
		if(idHistoria==null || osoba==null) {
			MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.ALL_VALUE.getValue());
		}
		else {
			setKategoria(kategoriaCombo.getText());
		}
	}

	public Integer getIdHistoria() {
		return idHistoria;
	}

	public void setIdHistoria(Integer idHistoria) {
		this.idHistoria = idHistoria;
	}

	public Personel getOsoba() {
		return osoba;
	}

	public void setOsoba(Personel osoba) {
		this.osoba = osoba;
	}

	public String getKategoria() {
		return kategoria;
	}

	public void setKategoria(String kategoria) {
		this.kategoria = kategoria;
	}

	

}