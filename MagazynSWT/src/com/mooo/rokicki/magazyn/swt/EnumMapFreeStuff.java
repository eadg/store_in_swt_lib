package com.mooo.rokicki.magazyn.swt;

import java.util.Map;
import java.util.HashMap;

public enum EnumMapFreeStuff {
	
	MAPA;
	
	private final Map<String, TableFreeStuff> mfs;
	
	EnumMapFreeStuff() {
		mfs=new HashMap<String, TableFreeStuff>();
	}
	
	public Map<String, TableFreeStuff> getSingiel() {
		return mfs;
	}


}