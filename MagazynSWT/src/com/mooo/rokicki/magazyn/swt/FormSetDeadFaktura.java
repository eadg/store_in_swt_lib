package com.mooo.rokicki.magazyn.swt;


import org.eclipse.swt.SWT;

import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.mooo.rokicki.magazyn.hql.SelectFaktura;


public class FormSetDeadFaktura {
	
	private Shell nowySzel;
	private Display display;
	private Shell parent;

	private int modalResult = SWT.CANCEL;
	private Label fakturaLabel;	
	private Combo fakturaCombo;	
	
	private Button buttonOK;
	private Button buttonCancel;
	
	private String daneFormularza;
	
	private final FormToolkit formToolkit = new FormToolkit(
			Display.getDefault());
	private Label lblNewLabel;

	public FormSetDeadFaktura(Shell parentShell) {
		display = Display.getCurrent();
		parent = parentShell;
		nowySzel = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		init();
	}

	public void init() {
		nowySzel.setSize(305, 175);
		nowySzel.setText("Ustaw fakturę jako rozliczoną");
		nowySzel.setLayout(new GridLayout(2, false));
		
		lblNewLabel = new Label(nowySzel, SWT.NONE);
		lblNewLabel.setAlignment(SWT.CENTER);
		GridData gd_lblNewLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		gd_lblNewLabel.heightHint = 45;
		gd_lblNewLabel.widthHint = 244;
		lblNewLabel.setLayoutData(gd_lblNewLabel);
		formToolkit.adapt(lblNewLabel, true, true);
		lblNewLabel.setText("Uwaga! Faktura zostanie ustawiona jako rozliczona i nie będzie widoczna.");
		String[] tabFaktura=new SelectFaktura().toArrayString();
		
		fakturaLabel = new Label(nowySzel, SWT.NONE);
		fakturaLabel.setAlignment(SWT.CENTER);
		GridData gd_fakturaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_fakturaLabel.widthHint = 103;
		fakturaLabel.setLayoutData(gd_fakturaLabel);
		formToolkit.adapt(fakturaLabel, true, true);
		fakturaLabel.setText("Faktura");
		
		fakturaCombo = new Combo(nowySzel, SWT.READ_ONLY);
		GridData gd_fakturaCombo = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_fakturaCombo.widthHint = 148;
		fakturaCombo.setLayoutData(gd_fakturaCombo);
		fakturaCombo.setItems(tabFaktura);
		
		
		formToolkit.adapt(fakturaCombo);
		formToolkit.paintBordersFor(fakturaCombo);
				new Label(nowySzel, SWT.NONE);
						new Label(nowySzel, SWT.NONE);
				
						buttonOK = new Button(nowySzel, SWT.PUSH);
						GridData gd_buttonOK = new GridData(SWT.LEFT, SWT.CENTER, false, false,	1, 1);
						gd_buttonOK.widthHint = 102;
						buttonOK.setLayoutData(gd_buttonOK);
						buttonOK.setText("OK");
						buttonOK.addSelectionListener(new SelectionAdapter() {
							public void widgetSelected(SelectionEvent arg0) {
								controls2Data();
								modalResult = SWT.OK;
								nowySzel.close();
							}
						});
				
						buttonCancel = new Button(nowySzel, SWT.CENTER);
						GridData gd_buttonCancel = new GridData(SWT.CENTER, SWT.CENTER, false,
								false, 1, 1);
						gd_buttonCancel.widthHint = 102;
						buttonCancel.setLayoutData(gd_buttonCancel);
						buttonCancel.setText("Anuluj");
						buttonCancel.addSelectionListener(new SelectionAdapter() {
							public void widgetSelected(SelectionEvent arg0) {
								controls2Data();
								modalResult = SWT.CANCEL;
								nowySzel.close();
							}
						});

	}

	public void startEventLoop() {
		while (!nowySzel.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	public int doModal() {
		nowySzel.open();
		// data2Controls();
		startEventLoop();
		return modalResult;
	}

	public void controls2Data() {
		setDaneFormularza(fakturaCombo.getText());
	}

	public String getDaneFormularza() {
		return daneFormularza;
	}

	public void setDaneFormularza(String daneFormularza) {
		this.daneFormularza = daneFormularza;
	}


}
