package com.mooo.rokicki.magazyn.swt;

import java.util.List;
import org.eclipse.swt.SWT;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import com.mooo.rokicki.magazyn.hql.ListProdukt;
import com.mooo.rokicki.magazyn.hql.SelectKategoria;
import com.mooo.rokicki.magazyn.hql.SelectProdukt;
import com.mooo.rokicki.magazyn.util.ConvertList;
import com.mooo.rokicki.magazyn.util.MessageErrors;
import com.mooo.rokicki.magazyn.util.Uniq;
import com.mooo.rokicki.magazyn.db.Produkt;

public class FormDeleteItemsFaktura {

	private Shell shell;
	private Display display;
	private Shell parent;

	private int modalResult = SWT.CANCEL;
	private Label kategoriaLabel;	
	private Label fakturaLabel;
	private Label produktLabel;
	private Combo kategoriaCombo;
	private Combo fakturaCombo;
	private Combo produktCombo;

	private Button buttonOK;
	private Button buttonCancel;

	private ListProdukt lp;
	private SelectProdukt sp;	
	private List<Produkt> listaProduktow;

	private Boolean update;

	private final FormToolkit formToolkit = new FormToolkit(
			Display.getDefault());

	public FormDeleteItemsFaktura(Shell parentShell) {
		display = Display.getCurrent();
		parent = parentShell;
		shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setToolTipText("Towar nie może zostać wcześniej wydany.");
		init();
	}

	public void init() {
		setUpdate(false);
		lp = new ListProdukt();
		sp = new SelectProdukt();
		shell.setSize(324, 186);
		shell.setText("Kasowanie pozycji z faktury");
		shell.setLayout(new GridLayout(2, false));

		kategoriaLabel = new Label(shell, SWT.NONE);
		GridData gd_kategoriaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_kategoriaLabel.widthHint = 115;
		kategoriaLabel.setLayoutData(gd_kategoriaLabel);
		formToolkit.adapt(kategoriaLabel, true, true);
		kategoriaLabel.setText("Kategoria");
		
		kategoriaCombo = new Combo(shell, SWT.READ_ONLY);
		kategoriaCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,	false, 1, 1));
		kategoriaCombo.setItems(ConvertList.listToArray(new SelectKategoria().listStringKategoria()));
		kategoriaCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				fakturaCombo.setItems(Uniq.arrayToUniqArray(lp.arrayFakturaByKategoria(kategoriaCombo.getText())));
					produktCombo.setItems(new String[0]);	
			}
		});

		formToolkit.adapt(kategoriaCombo);
		formToolkit.paintBordersFor(kategoriaCombo);		
		

		fakturaLabel = new Label(shell, SWT.NONE);
		GridData gd_fakturaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_fakturaLabel.widthHint = 131;
		fakturaLabel.setLayoutData(gd_fakturaLabel);
		formToolkit.adapt(fakturaLabel, true, true);
		fakturaLabel.setText("Faktura");

		fakturaCombo = new Combo(shell, SWT.READ_ONLY);
		fakturaCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,	false, 1, 1));
		fakturaCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				produktCombo.setItems(sp.arraySelectProduktByFaktura(fakturaCombo.getText().split("<>")[1]));				
//				produktCombo.setItems(lp.arrayProduktByFaktura(fakturaCombo.getText().split("<>")[1]));
			}
		});
		formToolkit.adapt(fakturaCombo);
		formToolkit.paintBordersFor(fakturaCombo);

		produktLabel = new Label(shell, SWT.NONE);
		GridData gd_jednostkaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_jednostkaLabel.widthHint = 135;
		produktLabel.setLayoutData(gd_jednostkaLabel);
		formToolkit.adapt(produktLabel, true, true);
		produktLabel.setText("Wybór produktu");

		produktCombo = new Combo(shell, SWT.READ_ONLY);
		produktCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,	false, 1, 1));
		produktCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				setLp(lp.listProduktByFakturaProdukt(produktCombo.getText(), fakturaCombo.getText().split("<>")[1]));
			}
		});
		formToolkit.adapt(produktCombo);
		formToolkit.paintBordersFor(produktCombo);

		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);

		buttonOK = new Button(shell, SWT.PUSH);
		GridData gd_buttonOK = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_buttonOK.widthHint = 125;
		buttonOK.setLayoutData(gd_buttonOK);
		buttonOK.setText("OK");
		buttonOK.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				controls2Data();
				modalResult = SWT.OK;
				shell.close();
			}
		});

		buttonCancel = new Button(shell, SWT.NONE);
		GridData gd_buttonCancel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_buttonCancel.widthHint = 120;
		buttonCancel.setLayoutData(gd_buttonCancel);
		buttonCancel.setText("Anuluj");
		buttonCancel.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				modalResult = SWT.CANCEL;
				shell.close();
			}
		});
	}

	public void startEventLoop() {
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	public int doModal() {
		shell.open();
		startEventLoop();
		return modalResult;
	}

//	public void data2Controls() {}

	public void controls2Data() {
		
		if(listaProduktow!=null) {
			}
		else {
			MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.ALL_VALUE.getValue());			
		}
	}

	public List<Produkt> getListaProduktow() {
		return listaProduktow;
	}

	public void setLp(List<Produkt> lp) {
		this.listaProduktow = lp;
	}

	public Boolean getUpdate() {
		return update;
	}

	public void setUpdate(Boolean update) {
		this.update = update;
	}
}
