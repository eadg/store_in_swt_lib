package com.mooo.rokicki.magazyn.swt;

import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.mooo.rokicki.magazyn.hql.SelectHistoria;
import com.mooo.rokicki.magazyn.util.ConvertList;

public class TablePeopleStuff {
	private Table table;
	private TableColumn tblcProd;
	private TableColumn tblAmount;
	private TableColumn tblPerson;
	private TableColumn tblTime;
	private TableItem tableItem;
	private SelectHistoria sh;	
	
	public TablePeopleStuff(Table table) {
		this.table=table;
		GridData gd_table = new GridData(SWT.LEFT, SWT.FILL, true, true, 1, 1);
		gd_table.widthHint = 605;
		table.setLayoutData(gd_table);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
	}
	
	public void addingColumn() {
		tblcProd = new TableColumn(table, SWT.NONE);
		tblcProd.setWidth(100);
		tblcProd.setText("Produkt");
		
		tblAmount = new TableColumn(table, SWT.NONE);
		tblAmount.setWidth(60);
		tblAmount.setText("Ilość");
		
		tblPerson = new TableColumn(table, SWT.NONE);
		tblPerson.setWidth(140);		
		tblPerson.setText("Dla kogo");
		
		tblTime = new TableColumn(table, SWT.NONE);
		tblTime.setWidth(120);
		tblTime.setText("Kiedy");
	}
	
	void addItems(String kat) {
//		table.removeAll();
		sh = new SelectHistoria();
		List<String[]> listHistoria=ConvertList.historiaToListString(sh.listHistoriaByKategoria(kat));
		for (Iterator<String[]> iterator = listHistoria.iterator(); iterator.hasNext();) {
			tableItem = new TableItem(table, SWT.NONE);
			tableItem.setText(iterator.next());
//			System.out.println("Dodaje wiersze");
		}
	}
	
	void addItems(Integer idkat) {
//		table.removeAll();
		sh = new SelectHistoria();
		List<String[]> listHistoria=ConvertList.historiaToListString(sh.listHistoriaByKategoria(idkat));
		for (Iterator<String[]> iterator = listHistoria.iterator(); iterator.hasNext();) {
			tableItem = new TableItem(table, SWT.NONE);
			tableItem.setText(iterator.next());
//			System.out.println("Dodaje wiersze");
		}
	}	
	
	void clearItems() {
		table.removeAll();
	}	
	
	

}
