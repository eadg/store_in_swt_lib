package com.mooo.rokicki.magazyn.swt;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.DateTime;

public class FormStateStorePeriod {
    private Shell shellAddString;
    private Display display;

    private int modalResult = SWT.CANCEL;
    private Button buttonOK;
    private Button buttonCancel;
    private Label labelka;
    private DateTime datemeEnd;
//    private Integer[] timeBegin;
    private Integer[] timeEnd;
    private Label labelEnd;

    public FormStateStorePeriod(Shell parentShell, String title) {
        display = Display.getCurrent();
        shellAddString = new Shell(parentShell, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        shellAddString.setText(title);  
        init();
    }

    public void init() {
        shellAddString.setSize(272, 289);

        labelka = new Label(shellAddString, SWT.BORDER);
        labelka.setAlignment(SWT.CENTER);
        labelka.setBounds(10, 8, 235, 25);

        buttonOK = new Button(shellAddString, SWT.PUSH);
        buttonOK.setBounds(44, 226, 70, 25);
        buttonOK.setText("OK");
        buttonOK.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
                controls2Data();
                modalResult = SWT.OK;
                shellAddString.close();
            }
        });

        buttonCancel = new Button(shellAddString, SWT.PUSH);
        buttonCancel.setBounds(160, 226, 70, 25);
        buttonCancel.setText("Anuluj");
        
        labelEnd = new Label(shellAddString, SWT.SHADOW_NONE | SWT.CENTER);
        labelEnd.setText("stan magazynu na dzień");
        labelEnd.setBounds(46, 39, 184, 15);
        
        datemeEnd = new DateTime(shellAddString, SWT.BORDER | SWT.CALENDAR);
        datemeEnd.setBounds(44, 59, 184, 161);
            
        buttonCancel.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
                controls2Data();
                modalResult = SWT.CANCEL;
                shellAddString.close();
            }
        });

    }

    public void startEventLoop() {
        while (!shellAddString.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
    }

    public int doModal() {
        shellAddString.open();
        startEventLoop();
        return modalResult;
    }

 
    public void controls2Data() {
//      setTimeBegin(new Integer[]{datemeBegin.getYear(), datemeBegin.getMonth(), datemeBegin.getDay()});
        setTimeEnd(new Integer[]{datemeEnd.getYear(), datemeEnd.getMonth(), datemeEnd.getDay()});
    }
    
    public void setLabel(String text) {
    	labelka.setText(text);
    }

//	public Integer[] getTimeBegin() {
//		return timeBegin;
//	}
//
//	public void setTimeBegin(Integer[] time) {
//		this.timeBegin = time;
//	}

	public Integer[] getTimEnd() {
		return timeEnd;
	}

	public void setTimeEnd(Integer[] time_end) {
		this.timeEnd = time_end;
	}	
}
