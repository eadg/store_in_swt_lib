package com.mooo.rokicki.magazyn.swt;


import org.eclipse.swt.SWT;

import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.mooo.rokicki.magazyn.hql.SelectPersonel;
import com.mooo.rokicki.magazyn.util.ConvertList;

import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.graphics.Point;


public class FormEditPersonel {
	
	private Shell nowySzel;
	private Display display;
	private Shell parent;

	private int modalResult = SWT.CANCEL;
	private Label personelLabel;	
	private Combo personelCombo;	
	
	private Button buttonOK;
	private Button buttonCancel;
	
	private String[] daneFormularza;
	
	private final FormToolkit formToolkit = new FormToolkit(
			Display.getDefault());
	private Label labelNowaNazwa;
	private Text personelNewText;

	public FormEditPersonel(Shell parentShell) {
		display = Display.getCurrent();
		parent = parentShell;
		nowySzel = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		nowySzel.setMinimumSize(new Point(132, 50));
		init();
	}

	public void init() {
		nowySzel.setSize(370, 157);
		nowySzel.setText("Zmiana imienia i nazwiska osoby");
		nowySzel.setLayout(new GridLayout(2, false));
		
		personelLabel = new Label(nowySzel, SWT.NONE);
		personelLabel.setAlignment(SWT.CENTER);
		GridData gd_personelLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_personelLabel.widthHint = 140;
		personelLabel.setLayoutData(gd_personelLabel);
		formToolkit.adapt(personelLabel, true, true);
		personelLabel.setText("Wybierz osobę do edycji");
		
		personelCombo = new Combo(nowySzel, SWT.READ_ONLY);
		GridData gd_personelCombo = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_personelCombo.widthHint = 148;
		personelCombo.setLayoutData(gd_personelCombo);
		String[] tabPersonel=ConvertList.listToArray(new SelectPersonel().listPersonel());		
		personelCombo.setItems(tabPersonel);
		
		
		formToolkit.adapt(personelCombo);
		formToolkit.paintBordersFor(personelCombo);
		
		labelNowaNazwa = new Label(nowySzel, SWT.NONE);
		labelNowaNazwa.setAlignment(SWT.CENTER);
		GridData gd_lblNowaNazwa = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblNowaNazwa.widthHint = 156;
		labelNowaNazwa.setLayoutData(gd_lblNowaNazwa);
		formToolkit.adapt(labelNowaNazwa, true, true);
		labelNowaNazwa.setText("Nowe imię i nazwisko");
		
		personelNewText = new Text(nowySzel, SWT.BORDER);
		personelNewText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		formToolkit.adapt(personelNewText, true, true);
				
				new Label(nowySzel, SWT.NONE);
				new Label(nowySzel, SWT.NONE);
				
						buttonOK = new Button(nowySzel, SWT.PUSH);
						GridData gd_buttonOK = new GridData(SWT.LEFT, SWT.CENTER, false, false,	1, 1);
						gd_buttonOK.widthHint = 102;
						buttonOK.setLayoutData(gd_buttonOK);
						buttonOK.setText("OK");
						buttonOK.addSelectionListener(new SelectionAdapter() {
							public void widgetSelected(SelectionEvent arg0) {
								controls2Data();
								modalResult = SWT.OK;
								nowySzel.close();
							}
						});
				
						buttonCancel = new Button(nowySzel, SWT.CENTER);
						GridData gd_buttonCancel = new GridData(SWT.CENTER, SWT.CENTER, false,
								false, 1, 1);
						gd_buttonCancel.widthHint = 102;
						buttonCancel.setLayoutData(gd_buttonCancel);
						buttonCancel.setText("Anuluj");
						buttonCancel.addSelectionListener(new SelectionAdapter() {
							public void widgetSelected(SelectionEvent arg0) {
								controls2Data();
								modalResult = SWT.CANCEL;
								nowySzel.close();
							}
						});

	}

	public void startEventLoop() {
		while (!nowySzel.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	public int doModal() {
		nowySzel.open();
		// data2Controls();
		startEventLoop();
		return modalResult;
	}

	public void controls2Data() {
		setDaneFormularza(new String[]{ 
				personelCombo.getText(), personelNewText.getText().trim()
				});
	}

	public String[] getDaneFormularza() {
		return daneFormularza;
	}

	public void setDaneFormularza(String[] daneFormularza) {
		this.daneFormularza = daneFormularza;
	}




}
