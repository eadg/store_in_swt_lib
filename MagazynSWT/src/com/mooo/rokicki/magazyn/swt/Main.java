package com.mooo.rokicki.magazyn.swt;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import com.mooo.rokicki.magazyn.net.NazwaZlobka;
import com.mooo.rokicki.magazyn.util.Version;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.SWT;

public class Main {

	protected Shell shell;
	private MainMenu mm;
	
	public static void main(String[] args) {
		try {
			Main window = new Main();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}


	protected void createContents() {
		shell = new Shell();
		shell.setMinimumSize(new Point(132, 50));
		shell.setSize(1024, 768);
		shell.setText("Witaj w magazynie: "+ new NazwaZlobka().getNazwa() + new Version().getVersion());
		shell.setImage(SWTResourceManager.getImage(Main.class, "/com/mooo/rokicki/magazyn/ico/wzz.gif"));
		shell.setLayout(new FillLayout(SWT.HORIZONTAL));
		mm = new MainMenu(shell);
	}
}