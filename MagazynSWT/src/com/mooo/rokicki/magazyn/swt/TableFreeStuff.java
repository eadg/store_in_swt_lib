package com.mooo.rokicki.magazyn.swt;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.mooo.rokicki.magazyn.hql.InsertHistoria;
import com.mooo.rokicki.magazyn.hql.SelectProdukt;
import com.mooo.rokicki.magazyn.util.ConvertList;

public class TableFreeStuff {
	private Table table;
	private TableColumn tblclmnProd;
	private TableColumn tblclmnIlo;
	private TableColumn tblclmnDostawca;
	private TableColumn tblclmnFaktura;
	private TableColumn tblclmnJednostka;
	private TableColumn tblclmnCena;
	private TableItem tableItem;
	private SelectProdukt sp;
	private String kategoria;
	private Shell shell;
	

	public TableFreeStuff(Table table, String kategoria, Shell shell) {
		this.table = table;
		GridData gd_table = new GridData(SWT.LEFT, SWT.FILL, true, true, 1, 1);
		gd_table.widthHint = 605;
		table.setLayoutData(gd_table);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		this.kategoria = kategoria;
		this.shell=shell;
	}

	void addingColumn() {
		tblclmnProd = new TableColumn(table, SWT.NONE);
		tblclmnProd.setWidth(95);
		tblclmnProd.setText("Produkt");

		tblclmnIlo = new TableColumn(table, SWT.NONE);
		tblclmnIlo.setWidth(50);
		tblclmnIlo.setText("Ilość");

		tblclmnDostawca = new TableColumn(table, SWT.NONE);
		tblclmnDostawca.setWidth(110);
		tblclmnDostawca.setText("Dostawca");

		tblclmnFaktura = new TableColumn(table, SWT.NONE);
		tblclmnFaktura.setWidth(105);
		tblclmnFaktura.setText("Faktura");

		tblclmnJednostka = new TableColumn(table, SWT.NONE);
		tblclmnJednostka.setWidth(70);
		tblclmnJednostka.setText("Jednostka");
		
		tblclmnCena  = new TableColumn(table, SWT.NONE);
		tblclmnCena.setWidth(70);
		tblclmnCena.setText("Cena");
	}

	void addItems(String kat) {
		// table.removeAll();
		sp = new SelectProdukt();
		List<String[]> listProdukt = ConvertList.listArrayObjectToListArrayString(sp.selectProduktByKategoria(kat));		
//		List<String[]> listProdukt = ConvertList.produktToListString(sp.listProduktByKategoria(kat));
		String[] sArray = null;
		for (Iterator<String[]> iterator = listProdukt.iterator(); 
				iterator.hasNext();) {
			sArray = iterator.next();
			tableItem = new TableItem(table, SWT.NONE);
			tableItem.setText(sArray);
			tableItem.setData(sArray);
		}
	}

	void addItems(Integer idkat) {
		// table.removeAll();
		sp = new SelectProdukt();
		List<String[]> listProdukt = ConvertList.listArrayObjectToListArrayString(sp.selectProduktByKategoria(idkat));
//		List<String[]> listProdukt = ConvertList.produktToListString(sp.listProduktByKategoria(idkat));
		String[] sArray = null;
		for (Iterator<String[]> iterator = listProdukt.iterator(); iterator
				.hasNext();) {
			sArray = iterator.next();
			tableItem = new TableItem(table, SWT.NONE);
			tableItem.setText(sArray);
			tableItem.setData(sArray);
		}
	}

	void clearItems() {
		table.removeAll();
	}
	
	void addMouseMenu() {
		Menu menu = new Menu(table);
		table.setMenu(menu);
		MenuItem mntmPop = new MenuItem(menu, SWT.NONE);
		mntmPop.setText("Rozchód towaru");
		mntmPop.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				TableItem[] selection = table.getSelection();
				for (int i = 0; i < selection.length; i++) {
					String[] sarry = (String[])selection[i].getData();
					FormPopRozchodTowar form = new FormPopRozchodTowar(shell, sarry, kategoria);
					if (form.doModal() == SWT.OK) {
						InsertHistoria ih = new InsertHistoria();
						if(form.getUpdate()) {
							ih.saveHistoryChange(form.getListaProduktow().get(0), form.getUpdateAmount()[1], form.getPerson());
							clearItems();
							addItems(kategoria);
							refreshTablePeopleStaff(kategoria);
						}		
					}
				}
			}
		});	
	}
	
	void refreshTablePeopleStaff(String s) {
		Map<String, TablePeopleStuff> map_people =  EnumMapPeopleStuff.MAPA.getSingiel();
		TablePeopleStuff tps  = map_people.get(s);
		tps.clearItems();
		tps.addItems(s);
	}
}


