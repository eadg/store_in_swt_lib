package com.mooo.rokicki.magazyn.swt;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Combo;

import com.mooo.rokicki.magazyn.hql.SelectPersonel;
import com.mooo.rokicki.magazyn.util.ConvertList;

public class FormSearchProduktsPersonLong {
    private Shell shellAddString;
    private Display display;

    private int modalResult = SWT.CANCEL;
    private Button buttonOK;
    private Button buttonCancel;
    private Label labelka;
    private Combo personelCombo;
    private DateTime dateTime;
    private Integer[] time;
    private String osoba;

    public FormSearchProduktsPersonLong(Shell parentShell, String title) {
        display = Display.getCurrent();
        shellAddString = new Shell(parentShell, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        shellAddString.setText(title);  
        init();
    }

    public void init() {
        shellAddString.setSize(267, 340);

        labelka = new Label(shellAddString, SWT.BORDER);
        labelka.setAlignment(SWT.CENTER);
        labelka.setBounds(10, 8, 241, 25);

        buttonOK = new Button(shellAddString, SWT.PUSH);
        buttonOK.setBounds(47, 261, 70, 25);
        buttonOK.setText("OK");
        buttonOK.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
                controls2Data();
                modalResult = SWT.OK;
                shellAddString.close();
            }
        });

        buttonCancel = new Button(shellAddString, SWT.PUSH);
        buttonCancel.setBounds(141, 261, 70, 25);
        buttonCancel.setText("Anuluj");
        
        dateTime = new DateTime(shellAddString, SWT.BORDER | SWT.CALENDAR);
        dateTime.setBounds(37, 86, 184, 169);
        
        personelCombo = new Combo(shellAddString, SWT.READ_ONLY);
        personelCombo.setToolTipText("Dostawca");
        personelCombo.setBounds(10, 45, 241, 23);
		String[] tabPerson=ConvertList.listToArray(new SelectPersonel().listPersonel());
		personelCombo.setItems(tabPerson);        
            
        buttonCancel.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
                controls2Data();
                modalResult = SWT.CANCEL;
                shellAddString.close();
            }
        });

    }

    public void startEventLoop() {
        while (!shellAddString.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
    }

    public int doModal() {
        shellAddString.open();
        startEventLoop();
        return modalResult;
    }

 
    public void controls2Data() {
    	setOsoba(personelCombo.getText());
        setTime(new Integer[]{dateTime.getYear(), dateTime.getMonth(), dateTime.getDay()});
    }
    
    public void setLabel(String text) {
    	labelka.setText(text);
    }

	public Integer[] getTime() {
		return time;
	}

	public void setTime(Integer[] time) {
		this.time = time;
	}

	public String getOsoba() {
		return osoba;
	}

	public void setOsoba(String osoba) {
		this.osoba = osoba;
	}
}
