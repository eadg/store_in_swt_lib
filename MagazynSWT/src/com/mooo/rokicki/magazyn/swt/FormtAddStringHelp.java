package com.mooo.rokicki.magazyn.swt;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;

public class FormtAddStringHelp {
    private Shell shellAddString;
    private Display display;

    private int modalResult = SWT.CANCEL;

    private Text poleText;
    private Button buttonOK;
    private Button buttonCancel;
    private String stringValue;
    private Label labelka;

    public FormtAddStringHelp(Shell parentShell, String title) {
        display = Display.getCurrent();
        shellAddString = new Shell(parentShell, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        shellAddString.setText(title);  
        init();
    }

    public void init() {
        shellAddString.setSize(454, 152);

        poleText = new Text(shellAddString, SWT.BORDER | SWT.SINGLE);
        poleText.setBounds(10, 39, 399, 32);

        labelka = new Label(shellAddString, SWT.BORDER);
        labelka.setBounds(10, 8, 399, 25);

        buttonOK = new Button(shellAddString, SWT.PUSH);
        buttonOK.setBounds(95, 89, 70, 25);
        buttonOK.setText("OK");
        buttonOK.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
                controls2Data();
                modalResult = SWT.OK;
                shellAddString.close();
            }
        });

        buttonCancel = new Button(shellAddString, SWT.PUSH);
        buttonCancel.setBounds(262, 89, 70, 25);
        buttonCancel.setText("Anuluj");
            
        buttonCancel.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
                controls2Data();
                modalResult = SWT.CANCEL;
                shellAddString.close();
            }
        });

    }

    public void startEventLoop() {
        while (!shellAddString.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
    }

    public int doModal() {
        shellAddString.open();
        startEventLoop();
        return modalResult;
    }

    public String getTextValue() {
        return stringValue;
    }

    public void setTextValue(String textValue) {
        this.stringValue = textValue;
    }
    
    public void controls2Data() {
        stringValue = poleText.getText().trim();
    }
    
    public void setLabel(String text) {
    	labelka.setText(text);
    }
}
