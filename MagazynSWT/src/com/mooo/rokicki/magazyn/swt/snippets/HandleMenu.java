package com.mooo.rokicki.magazyn.swt.snippets;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import com.mooo.rokicki.magazyn.swt.FormPopRozchodTowar;

public class HandleMenu {

	protected Shell shell;
	private Table table;
	private TableItem tableItem;

	public static void main(String[] args) {
		try {
			HandleMenu window = new HandleMenu();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	protected void createContents() {

		shell = new Shell();
		shell.setSize(450, 300);
		shell.setText("SWT Application");

		table = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION);
		table.setBounds(55, 31, 205, 180);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableColumn tblclmnNewColumn = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn.setWidth(100);
		tblclmnNewColumn.setText("kol 1");

		TableColumn tblclmnNewColumn_1 = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn_1.setWidth(100);
		tblclmnNewColumn_1.setText("kol 2");

		tableItem = new TableItem(table, SWT.NONE);
		tableItem.setText(new String[] { "ala", "bala" });

		tableItem.setData(new String[] { "D aa", "bbb" });

		tableItem = new TableItem(table, SWT.NONE);
		tableItem.setText(new String[] { "ala1", "bala1" });
		tableItem.setData(new String[] { "D ala1", "bala1" });

		Menu menu = new Menu(table);
		table.setMenu(menu);

		MenuItem mntmPop = new MenuItem(menu, SWT.NONE);
		mntmPop.setText("Pop!");
		mntmPop.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				TableItem[] selection = table.getSelection();
				for (int i = 0; i < selection.length; i++) {
					System.out.println(((String[]) selection[i].getData())[0]);
					System.out.println(((String[]) selection[i].getData())[1]);
					FormPopRozchodTowar dialog = new FormPopRozchodTowar(shell, (String[])selection[i].getData(), "testDupa");
					if (dialog.doModal() == SWT.OK) {
						
					}
				}
			}
		});


	}
}
