package com.mooo.rokicki.magazyn.swt.snippets;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

public class HandleTableItemEvent {

	protected Shell shell;
	private Table table;
	private TableItem tableItem;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			HandleTableItemEvent window = new HandleTableItemEvent();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		
		
		shell = new Shell();
		shell.setSize(450, 300);
		shell.setText("SWT Application");

		Listener listener = new Listener () {
			public void handleEvent (Event e) {
				if(e.button > 2) {
				String strings="";
				e.widget.getData("kuku");
				TableItem[] selection = table.getSelection();				
				//System.out.println(e.widget.getData("kuku"));
		        for (int i = 0; i < selection.length; i++) {
		            strings += selection[i] + " ";
//		          System.out.println(strings);
		          System.out.println(((String[])selection[i].getData("fiu"))[0]);
		          System.out.println(((String[])selection[i].getData("fiu"))[1]);
		          
		        }				
				}

			}
		};		
		
		
		table = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION);
		table.setBounds(55, 31, 205, 180);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		TableColumn tblclmnNewColumn = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn.setWidth(100);
		tblclmnNewColumn.setText("kol 1");
		
		TableColumn tblclmnNewColumn_1 = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn_1.setWidth(100);
		tblclmnNewColumn_1.setText("kol 2");
		
		tableItem = new TableItem(table, SWT.NONE);
		tableItem.setText(new String[]{"ala", "bala"});
		table.setData("kuku", "aaaa");
		table.addListener(SWT.MouseUp, listener);
		tableItem.setData("fiu", (new String[]{"aa", "bbb"}));

		tableItem = new TableItem(table, SWT.NONE);
		tableItem.setText(new String[]{"ala1", "bala1"});
		tableItem.setData("fiu", (new String[]{"ala1", "bala1"}));
		
//		table.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mouseUp(MouseEvent arg0) {
//				
//				if(arg0.button>1) {
////					arg0.widget.setData("kuku", new String("a"));
//					System.out.println("dupa");
//					System.out.println(arg0.button);
//					System.out.println(arg0.widget.getData("kuku"));
//				}
//				
//
//			}
//		});

		
//		table.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mouseDoubleClick(MouseEvent arg0) {
//				
//				System.out.println("dupa");
//			}
//			
//			
//		});



	}
}
