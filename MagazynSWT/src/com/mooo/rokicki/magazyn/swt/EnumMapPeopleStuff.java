package com.mooo.rokicki.magazyn.swt;

import java.util.Map;
import java.util.HashMap;

public enum EnumMapPeopleStuff {
	
	MAPA;
	
	private final Map<String, TablePeopleStuff> mps;
	
	EnumMapPeopleStuff() {
		mps=new HashMap<String, TablePeopleStuff>();
	}
	
	public Map<String, TablePeopleStuff> getSingiel() {
		return mps;
	}


}