package com.mooo.rokicki.magazyn.swt;


import org.eclipse.swt.SWT;

import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.mooo.rokicki.magazyn.hql.SelectDostawca;
import com.mooo.rokicki.magazyn.util.ConvertList;

public class FormAddFaktura {
	
	private Shell nowySzel;
	private Display display;
	private Shell parent;

	private int modalResult = SWT.CANCEL;

	private Text fakturaText;
	private Label fakturaLabel;
	private Label dostawcaLabel;	
	private Combo dostawcaCombo;	
	
	private Button buttonOK;
	private Button buttonCancel;
	
	private String[] daneFormularza;
	
	private final FormToolkit formToolkit = new FormToolkit(
			Display.getDefault());

	public FormAddFaktura(Shell parentShell) {
		display = Display.getCurrent();
		parent = parentShell;
		nowySzel = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		init();
	}

	public void init() {
		nowySzel.setSize(432, 157);
		nowySzel.setText("Dodaj nową fakturę");
		nowySzel.setLayout(new GridLayout(2, false));
		
		fakturaLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_fakturaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_fakturaLabel.widthHint = 103;
		fakturaLabel.setLayoutData(gd_fakturaLabel);
		formToolkit.adapt(fakturaLabel, true, true);
		fakturaLabel.setText("Nazwa faktury");
		
		fakturaText = new Text(nowySzel, SWT.BORDER | SWT.SINGLE);
		GridData gd_text0 = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1,
				1);
		gd_text0.widthHint = 334;
		fakturaText.setLayoutData(gd_text0);


		dostawcaLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_dostawcaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_dostawcaLabel.widthHint = 103;
		dostawcaLabel.setLayoutData(gd_dostawcaLabel);
		formToolkit.adapt(dostawcaLabel, true, true);
		dostawcaLabel.setText("Wybierz dostawcę");
		
		dostawcaCombo = new Combo(nowySzel, SWT.READ_ONLY);
		dostawcaCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		String[] tabDostawca=ConvertList.listToArray(new SelectDostawca().listDostawca());
		dostawcaCombo.setItems(tabDostawca);
		formToolkit.adapt(dostawcaCombo);
		formToolkit.paintBordersFor(dostawcaCombo);
		new Label(nowySzel, SWT.NONE);
		new Label(nowySzel, SWT.NONE);

		buttonOK = new Button(nowySzel, SWT.PUSH);
		GridData gd_buttonOK = new GridData(SWT.LEFT, SWT.CENTER, false, false,	1, 1);
		gd_buttonOK.widthHint = 102;
		buttonOK.setLayoutData(gd_buttonOK);
		buttonOK.setText("OK");
		buttonOK.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				controls2Data();
				modalResult = SWT.OK;
				nowySzel.close();
			}
		});

		buttonCancel = new Button(nowySzel, SWT.CENTER);
		GridData gd_buttonCancel = new GridData(SWT.CENTER, SWT.CENTER, true,
				false, 1, 1);
		gd_buttonCancel.widthHint = 102;
		buttonCancel.setLayoutData(gd_buttonCancel);
		buttonCancel.setText("Anuluj");
		buttonCancel.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				controls2Data();
				modalResult = SWT.CANCEL;
				nowySzel.close();
			}
		});

	}

	public void startEventLoop() {
		while (!nowySzel.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	public int doModal() {
		nowySzel.open();
		// data2Controls();
		startEventLoop();
		return modalResult;
	}

	public void controls2Data() {
		setDaneFormularza(new String[]{ 
				fakturaText.getText().trim(), dostawcaCombo.getText().trim()
				});
	}

	public String[] getDaneFormularza() {
		return daneFormularza;
	}

	public void setDaneFormularza(String[] daneFormularza) {
		this.daneFormularza = daneFormularza;
	}
}
