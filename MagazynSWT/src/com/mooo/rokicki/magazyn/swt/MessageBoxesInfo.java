package com.mooo.rokicki.magazyn.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class MessageBoxesInfo {
	
	Shell parent;
	
	public MessageBoxesInfo(Shell parrent, String s) {
		this.parent=parrent;
		MessageBox mb = new MessageBox(parent, SWT.ICON_INFORMATION);
		mb.setText("Operacja pomyślna");
		mb.setMessage(s);
		mb.open();
	}
	
}
