package com.mooo.rokicki.magazyn.swt;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Combo;

import com.mooo.rokicki.magazyn.hql.SelectPersonel;
import com.mooo.rokicki.magazyn.util.ConvertList;

public class FormSearchProduktsPersonPeriod {
    private Shell shell;
    private Display display;

    private int modalResult = SWT.CANCEL;
    private Button buttonOK;
    private Button buttonCancel;
    private Label labelka;
    private Combo personelCombo;
    private DateTime datemeBegin;
    private DateTime datemeEnd;
    private Integer[] timeBegin;
    private Integer[] timeEnd;
    private String osoba;

    public FormSearchProduktsPersonPeriod(Shell parentShell, String title) {
        display = Display.getCurrent();
        shell = new Shell(parentShell, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        shell.setText(title);  
        init();
    }

    public void init() {
        shell.setSize(423, 340);
        
        labelka = new Label(shell, SWT.CENTER);
        labelka.setBounds(10, 10, 401, 15);

        Label labelEnd = new Label(shell, SWT.SHADOW_NONE | SWT.CENTER);
        labelEnd.setText("koniec okresu");
        labelEnd.setBounds(227, 91, 184, 15);
        
        Label labelBegin = new Label(shell, SWT.SHADOW_NONE | SWT.CENTER);
        labelBegin.setBounds(10, 91, 184, 15);
        labelBegin.setText("początek okresu");

        buttonOK = new Button(shell, SWT.PUSH);
        buttonOK.setBounds(124, 277, 70, 25);
        buttonOK.setText("OK");
        buttonOK.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
                controls2Data();
                modalResult = SWT.OK;
                shell.close();
            }
        });

        buttonCancel = new Button(shell, SWT.PUSH);
        buttonCancel.setBounds(227, 277, 70, 25);
        buttonCancel.setText("Anuluj");
        
        datemeBegin = new DateTime(shell, SWT.BORDER | SWT.CALENDAR);
        datemeBegin.setBounds(10, 102, 184, 169);
        
        personelCombo = new Combo(shell, SWT.READ_ONLY);
        personelCombo.setToolTipText("Dostawca");
        personelCombo.setBounds(10, 45, 401, 23);
		String[] tabPerson=ConvertList.listToArray(new SelectPersonel().listPersonel());
		personelCombo.setItems(tabPerson);        
		
		datemeEnd = new DateTime(shell, SWT.BORDER | SWT.CALENDAR);
		datemeEnd.setBounds(227, 102, 184, 169);
            
        buttonCancel.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
                controls2Data();
                modalResult = SWT.CANCEL;
                shell.close();
            }
        });

    }

    public void startEventLoop() {
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
    }

    public int doModal() {
        shell.open();
        startEventLoop();
        return modalResult;
    }

 
    public void controls2Data() {
    	setOsoba(personelCombo.getText());
        setTimeBegin(new Integer[]{datemeBegin.getYear(), datemeBegin.getMonth(), datemeBegin.getDay()});
        setTimeEnd(new Integer[]{datemeEnd.getYear(), datemeEnd.getMonth(), datemeEnd.getDay()});
    }
    
    public void setLabel(String text) {
    	labelka.setText(text);
    }

	public Integer[] getTimeBegin() {
		return timeBegin;
	}

	public void setTimeBegin(Integer[] time) {
		this.timeBegin = time;
	}


	public Integer[] getTimEnd() {
		return timeEnd;
	}

	public void setTimeEnd(Integer[] time_end) {
		this.timeEnd = time_end;
	}

	public String getOsoba() {
		return osoba;
	}

	public void setOsoba(String osoba) {
		this.osoba = osoba;
	}
}
