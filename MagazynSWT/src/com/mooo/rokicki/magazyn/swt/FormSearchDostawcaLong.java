package com.mooo.rokicki.magazyn.swt;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Combo;

import com.mooo.rokicki.magazyn.hql.SelectDostawca;
import com.mooo.rokicki.magazyn.util.ConvertList;

public class FormSearchDostawcaLong {
    private Shell shellAddString;
    private Display display;

    private int modalResult = SWT.CANCEL;
    private Button buttonOK;
    private Button buttonCancel;
    private Label labelka;
    private Combo dostawcaCombo;
    private DateTime dateTime;
    private Integer[] time;
    private String dostawca;

    public FormSearchDostawcaLong(Shell parentShell, String title) {
        display = Display.getCurrent();
        shellAddString = new Shell(parentShell, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        shellAddString.setText(title);  
        init();
    }

    public void init() {
        shellAddString.setSize(267, 340);

        labelka = new Label(shellAddString, SWT.BORDER);
        labelka.setAlignment(SWT.CENTER);
        labelka.setBounds(10, 8, 241, 25);

        buttonOK = new Button(shellAddString, SWT.PUSH);
        buttonOK.setBounds(47, 261, 70, 25);
        buttonOK.setText("OK");
        buttonOK.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
                controls2Data();
                modalResult = SWT.OK;
                shellAddString.close();
            }
        });

        buttonCancel = new Button(shellAddString, SWT.PUSH);
        buttonCancel.setBounds(141, 261, 70, 25);
        buttonCancel.setText("Anuluj");
        
        dateTime = new DateTime(shellAddString, SWT.BORDER | SWT.CALENDAR);
        dateTime.setBounds(37, 86, 184, 169);
        
        dostawcaCombo = new Combo(shellAddString, SWT.READ_ONLY);
        dostawcaCombo.setToolTipText("Dostawca");
        dostawcaCombo.setBounds(10, 45, 241, 23);
		String[] tabDostawca=ConvertList.listToArray(new SelectDostawca().listDostawca());
		dostawcaCombo.setItems(tabDostawca);        
            
        buttonCancel.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
                controls2Data();
                modalResult = SWT.CANCEL;
                shellAddString.close();
            }
        });

    }

    public void startEventLoop() {
        while (!shellAddString.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
    }

    public int doModal() {
        shellAddString.open();
        startEventLoop();
        return modalResult;
    }

 
    public void controls2Data() {
    	setDostawca(dostawcaCombo.getText());
        setTime(new Integer[]{dateTime.getYear(), dateTime.getMonth(), dateTime.getDay()});
    }
    
    public void setLabel(String text) {
    	labelka.setText(text);
    }

	public Integer[] getTime() {
		return time;
	}

	public void setTime(Integer[] time) {
		this.time = time;
	}

	public String getDostawca() {
		return dostawca;
	}

	public void setDostawca(String dostawca) {
		this.dostawca = dostawca;
	}
}
