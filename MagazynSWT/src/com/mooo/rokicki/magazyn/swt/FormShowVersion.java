package com.mooo.rokicki.magazyn.swt;


import java.util.LinkedHashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;

import com.mooo.rokicki.magazyn.util.Version;
import com.mooo.rokicki.magazyn.util.ConvertMap;
import org.eclipse.wb.swt.SWTResourceManager;

public class FormShowVersion {
    private Shell shellAddString;
    private Display display;

    private int modalResult = SWT.CANCEL;
    private Button buttonOK;
    private Label labelka;

    public FormShowVersion(Shell parentShell, String title) {
        display = Display.getCurrent();
        shellAddString = new Shell(parentShell, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        shellAddString.setText(title);  
        init();
    }

    public void init() {
        shellAddString.setSize(657, 389);

        labelka = new Label(shellAddString, SWT.BORDER);
        labelka.setFont(SWTResourceManager.getFont("Calibri", 8, SWT.NORMAL));
        labelka.setBounds(10, 8, 630, 312);
        
		Version v = new Version();
		LinkedHashMap<String, String> mapa = v.getVerMap();
		labelka.setText((ConvertMap.arrayToString(ConvertMap.getArrayString(mapa))));

        buttonOK = new Button(shellAddString, SWT.CENTER);
        buttonOK.setBounds(274, 326, 70, 25);
        buttonOK.setText("OK");
        buttonOK.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
                controls2Data();
                modalResult = SWT.OK;
                shellAddString.close();
            }
        });

    }

    public void startEventLoop() {
        while (!shellAddString.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
//		display.dispose(); no way -> closing Main application
    }

    public int doModal() {
        shellAddString.open();
        startEventLoop();
        return modalResult;
    }


    public void controls2Data() {
    }

}
