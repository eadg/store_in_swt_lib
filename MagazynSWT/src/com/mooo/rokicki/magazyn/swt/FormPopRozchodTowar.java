package com.mooo.rokicki.magazyn.swt;

import java.util.List;

import org.eclipse.swt.SWT;

import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.mooo.rokicki.magazyn.db.Produkt;
import com.mooo.rokicki.magazyn.hql.ListProdukt;
import com.mooo.rokicki.magazyn.hql.SelectPersonel;
import com.mooo.rokicki.magazyn.hql.SelectProdukt;
import com.mooo.rokicki.magazyn.util.ConvertList;
import com.mooo.rokicki.magazyn.util.MessageErrors;
import com.mooo.rokicki.magazyn.util.MessageInfo;

import org.eclipse.swt.widgets.Combo;

public class FormPopRozchodTowar {

	private Shell nowySzel;
	private Display display;
	private Shell parent;

	private int modalResult = SWT.CANCEL;

	private Text stanText;
	private Label stanLabel;
	private Label kategoriaLabelL;
	private Label kategoriaLabelR;
	private Label fakturaLabelL;	
	private Label fakturaLabelR;
	private Label magazynLabelLeft;
	private Label magazynLabelRight;	
	private Label cenaLabelL;
	private Label cenaLabelR;
	private Label jednostkaLabelL;
	private Label jednostkaLabelR;	
	private Label produktLabelL;
	private Label produktLabelR;
	private Label personelLabelL;
	private Combo personelCombo;

	private Button buttonOK;
	private Button buttonCancel;

	private ListProdukt lp;
	private SelectProdukt sp;
	private List<Produkt> listaProduktow;
	private Integer[] updateAmount;
	private Boolean update;
	private String person;

	private final FormToolkit formToolkit = new FormToolkit(
			Display.getDefault());

	

	public FormPopRozchodTowar(Shell parentShell, String[] data, String kat) {
		display = Display.getCurrent();
		parent = parentShell;
		nowySzel = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		init();
		kategoriaLabelR.setText(kat);
		data2Controls(data);
	}

	public void init() {
		setUpdate(false);
		sp = new SelectProdukt();
		lp = new ListProdukt();
		nowySzel.setSize(292, 300);
		nowySzel.setText("Rozchód produktu z faktury");
		nowySzel.setLayout(new GridLayout(2, false));

		kategoriaLabelL = new Label(nowySzel, SWT.NONE);
		GridData gd_kategoriaLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_kategoriaLabel.widthHint = 103;
		kategoriaLabelL.setLayoutData(gd_kategoriaLabel);
		formToolkit.adapt(kategoriaLabelL, true, true);
		kategoriaLabelL.setText("Kategoria");
		
		kategoriaLabelR = new Label(nowySzel, SWT.BORDER | SWT.READ_ONLY);
		kategoriaLabelR.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1));
		formToolkit.adapt(kategoriaLabelR, true, true);	
		

		fakturaLabelL = new Label(nowySzel, SWT.NONE);
		GridData gd_fakturaLabel = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_fakturaLabel.widthHint = 103;
		fakturaLabelL.setLayoutData(gd_fakturaLabel);
		formToolkit.adapt(fakturaLabelL, true, true);
		fakturaLabelL.setText("Faktura");

		fakturaLabelR = new Label(nowySzel, SWT.BORDER | SWT.READ_ONLY);
		fakturaLabelR.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1));
		formToolkit.adapt(fakturaLabelR, true, true);

		produktLabelL = new Label(nowySzel, SWT.NONE);
		GridData gd_jednostkaLabel = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_jednostkaLabel.widthHint = 100;
		produktLabelL.setLayoutData(gd_jednostkaLabel);
		formToolkit.adapt(produktLabelL, true, true);
		produktLabelL.setText("Produkt");

		produktLabelR = new Label(nowySzel, SWT.BORDER | SWT.READ_ONLY);
		produktLabelR.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1));
		formToolkit.adapt(produktLabelR, true, true);

		magazynLabelLeft = new Label(nowySzel, SWT.NONE);
		formToolkit.adapt(magazynLabelLeft, true, true);
		magazynLabelLeft.setText("Ilość na magazynie");
		
		magazynLabelRight = new Label(nowySzel, SWT.BORDER | SWT.READ_ONLY);
		magazynLabelRight.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1));
		formToolkit.adapt(magazynLabelRight, true, true);		
		
		cenaLabelL = new Label(nowySzel, SWT.NONE);
		cenaLabelL.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		formToolkit.adapt(cenaLabelL, true, true);
		cenaLabelL.setText("Cena");
		
		cenaLabelR = new Label(nowySzel, SWT.NONE);
		cenaLabelR.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		formToolkit.adapt(cenaLabelR, true, true);
		
		jednostkaLabelL = new Label(nowySzel, SWT.NONE);
		GridData gd_lblJednostka = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_lblJednostka.widthHint = 103;
		jednostkaLabelL.setLayoutData(gd_lblJednostka);
		formToolkit.adapt(jednostkaLabelL, true, true);
		jednostkaLabelL.setText("Jednostka");
		
		jednostkaLabelR = new Label(nowySzel, SWT.BORDER | SWT.READ_ONLY);
		jednostkaLabelR.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		formToolkit.adapt(jednostkaLabelR, true, true);

		stanLabel = new Label(nowySzel, SWT.NONE);
		GridData gd_stanLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_stanLabel.widthHint = 103;
		stanLabel.setLayoutData(gd_stanLabel);
		formToolkit.adapt(stanLabel, true, true);
		stanLabel.setText("Wydana ilość");

		stanText = new Text(nowySzel, SWT.BORDER);
		GridData gd_stanText = new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1);
		gd_stanText.widthHint = 133;
		stanText.setLayoutData(gd_stanText);
		formToolkit.adapt(stanText, true, true);

		personelLabelL = new Label(nowySzel, SWT.NONE);
		GridData gd_personelLabel = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_personelLabel.widthHint = 102;
		personelLabelL.setLayoutData(gd_personelLabel);
		formToolkit.adapt(personelLabelL, true, true);
		personelLabelL.setText("Osoba");
		
		personelCombo = new Combo(nowySzel, SWT.READ_ONLY);
		personelCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));
		personelCombo.setItems(ConvertList.listToArray(new SelectPersonel()
				.listPersonel()));
		personelCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				setLp(lp.listProduktByFakturaProdukt(produktLabelR.getText(), fakturaLabelR.getText()));
			}
		});	
		formToolkit.adapt(personelCombo);
		formToolkit.paintBordersFor(personelCombo);

		new Label(nowySzel, SWT.NONE);
		new Label(nowySzel, SWT.NONE);
		new Label(nowySzel, SWT.NONE);
		new Label(nowySzel, SWT.NONE);

		buttonOK = new Button(nowySzel, SWT.PUSH);
		GridData gd_buttonOK = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_buttonOK.widthHint = 102;
		buttonOK.setLayoutData(gd_buttonOK);
		buttonOK.setText("OK");
		buttonOK.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				controls2Data();
				modalResult = SWT.OK;
				nowySzel.close();
			}
		});

		buttonCancel = new Button(nowySzel, SWT.CENTER);
		GridData gd_buttonCancel = new GridData(SWT.CENTER, SWT.CENTER, false,
				false, 1, 1);
		gd_buttonCancel.widthHint = 102;
		buttonCancel.setLayoutData(gd_buttonCancel);
		buttonCancel.setText("Anuluj");
		buttonCancel.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				//controls2Data();
				modalResult = SWT.CANCEL;
				nowySzel.close();
			}
		});
	}

	public void startEventLoop() {
		while (!nowySzel.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	public int doModal() {
		nowySzel.open();
		// data2Controls();
		startEventLoop();
		return modalResult;
	}

	public void data2Controls(String[] array) {
		produktLabelR.setText(array[0]);
		magazynLabelRight.setText(array[1]);
		fakturaLabelR.setText(array[3]);
		jednostkaLabelR.setText(array[4]);
		cenaLabelR.setText(array[5]);
	}
	
	public void controls2Data() {
		
		if(listaProduktow!=null && !personelCombo.getText().isEmpty() && !stanText.getText().isEmpty()) {
			
			String oDB = sp.getAmountForProdukt(listaProduktow.get(0).getId()).get(0).toString();
			Integer ileDB = Integer.decode(oDB);
//			Integer ileDB = sp.getAmountForProdukt(listaProduktow.get(0).getId()).get(0);
			Integer ileForm = Integer.decode(stanText.getText().trim());

			if(ileDB < ileForm){
				MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.OUTPASS_AMOUNT.getValue());
			}
			else {
				setUpdate(true);
				Integer updateProdukt = ileDB - ileForm;
				Integer updateHistoria= ileForm*(-1);
				setUpdateAmount(new Integer[] {updateProdukt,  updateHistoria});
				setPerson(personelCombo.getText());
				MessageBoxesInfo mb = new MessageBoxesInfo(parent, MessageInfo.TOWAR_PRZYPISANY.getValue());
			}			
		}
		else {
			MessageBoxesError mb = new MessageBoxesError(parent, MessageErrors.ALL_VALUE.getValue());			
		}
	}	

	public List<Produkt> getListaProduktow() {
		return listaProduktow;
	}

	public void setLp(List<Produkt> lp) {
		this.listaProduktow = lp;
	}

	public Boolean getUpdate() {
		return update;
	}

	public void setUpdate(Boolean update) {
		this.update = update;
	}

	public String getPerson() {
		return person;
	}

	public void setPerson(String person) {
		this.person = person;
	}

	public Integer[] getUpdateAmount() {
		return updateAmount;
	}

	public void setUpdateAmount(Integer[] updateAmount) {
		this.updateAmount = updateAmount;
	}	
	
}
