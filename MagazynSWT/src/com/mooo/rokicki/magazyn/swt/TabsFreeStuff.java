package com.mooo.rokicki.magazyn.swt;
//podejrzane są singletony które nie są prawidłowo aktualizowane po operacji clear item
//co robić? wcześniejsze wersje też miały ten błąd :(

import java.util.Map;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.layout.GridLayout;

public class TabsFreeStuff {

	private TabFolder tabfol;
	private Table table;
	private Shell parent;
	private TabItem tabitem;
	private TableFreeStuff tableGener;
	private Group grp;	
	private Map<String, TableFreeStuff> mapa_tabelek;

	public TabsFreeStuff(Shell shell) {

		parent = shell;
		grp = new Group(parent, SWT.NONE);
		grp.setText("Wolne środki na magazynie");
		grp.setLayout(new GridLayout(1, false));
		setTabfol(new TabFolder(grp, SWT.BAR));
		mapa_tabelek = EnumMapFreeStuff.MAPA.getSingiel();
	}

	public Map<String, TableFreeStuff> getMapa_tabelek() {
		return mapa_tabelek;
	}

	public void setMapa_tabelek(Map<String, TableFreeStuff> mapa_tabelek) {
		this.mapa_tabelek = mapa_tabelek;
	}	
	
	public TabFolder getTabfol() {
		return tabfol;
	}

	public void setTabfol(TabFolder tabfol) {
		this.tabfol = tabfol;
	}

	public void addTabItem(String[] kategorie) {
			for (int ile = 0; ile < kategorie.length; ile++) {
			tabitem = new TabItem(tabfol, SWT.NONE);
			tabitem.setText(kategorie[ile]);
			table = new Table(tabfol, SWT.BORDER | SWT.FULL_SELECTION);
			table.setHeaderVisible(true);
			table.setLinesVisible(true);
			tableGener = new TableFreeStuff(table, kategorie[ile], parent);
			tableGener.addingColumn();
			tableGener.addItems(kategorie[ile]);
			tableGener.addMouseMenu();
			tabitem.setControl(table);
			mapa_tabelek.put(kategorie[ile], tableGener);
		}
	}

	public void addTabItem(String kategoria) {
		tabitem = new TabItem(tabfol, SWT.NONE);
		tabitem.setText(kategoria);
		table = new Table(tabfol, SWT.BORDER | SWT.FULL_SELECTION);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		tableGener = new TableFreeStuff(table, kategoria, parent);
		tableGener.addingColumn();
		tableGener.addItems(kategoria);
		tableGener.addMouseMenu();		
		tabitem.setControl(table);
		mapa_tabelek.put(kategoria, tableGener);
	}

}