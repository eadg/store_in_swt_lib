package com.mooo.rokicki.magazyn.hql;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.Kategoria;
import com.mooo.rokicki.magazyn.db.HibernateUtil;

public class InsertKategoria {
	
	public Integer saveKategoria(String nazwa) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Integer id = null;
		List<Kategoria> lista = null;
		try {
			transaction = session.beginTransaction();
			String selectKategoria = "from Kategoria P where P.name = :kate";
			Query query = session.createQuery(selectKategoria);
			query.setString("kate", nazwa);
			lista = query.list();
			if (lista.isEmpty()) {
				Kategoria obj = new Kategoria();
				obj.setName(nazwa);
				id = (Integer) session.save(obj);
				transaction.commit();
			}
			else {
				id=0;
			}
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			System.out.println("kategoria już istnieje");
		} finally {
			session.close();
			System.out.println("finally");
		}
		return new Integer(id);
	}	

//	public Integer saveKategoria(String nazwa) {
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		Transaction transaction = null;
//		Integer id_kategoria = null;
//		try {
//			transaction = session.beginTransaction();
//			Kategoria kategoria = new Kategoria();
//			kategoria.setName(nazwa);
//			id_kategoria = (Integer) session.save(kategoria);
//			transaction.commit();
//		} catch (HibernateException e) {
//			transaction.rollback();
//			e.printStackTrace();
//			System.out.println("kategoria już istnieje");
//		} finally {
//			session.close();
//			System.out.println("finally");
//		}
//		return new Integer(id_kategoria);		
//	}

}
