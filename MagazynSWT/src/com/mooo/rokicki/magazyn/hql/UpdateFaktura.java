package com.mooo.rokicki.magazyn.hql;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.HibernateUtil;
import com.mooo.rokicki.magazyn.db.Faktura;
import com.mooo.rokicki.magazyn.db.Dostawca;
import com.mooo.rokicki.magazyn.db.HistoriaFaktura;

public class UpdateFaktura {

	public Integer setDeadFaktura(Faktura faktura) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			String selectFaktura = "from Faktura f where f.id = :idFak";
			Query query = session.createQuery(selectFaktura);
			query.setInteger("idFak", faktura.getId());
			List<Faktura> res_faktura = query.list();
			Faktura fak =  res_faktura.get(0);
			fak.setDead(true);
			session.save(fak);
			
			HistoriaFaktura hf = new HistoriaFaktura();
			hf.setFaktura(fak);
			hf.setTime(new Date());
			session.save(hf);	
			
			transaction.commit();
			
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return 0;
		} finally {
			session.disconnect();
			session.close();
		}
		return 1;
	}
	
	
	public Integer setNewParametrFaktura(String[] data) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			String selectFaktura = "from Faktura f where f.name =:nazwaf and f.Dostawca.name =:nazwad";
			Query query = session.createQuery(selectFaktura);
			query.setString("nazwad", data[0].split("<>")[0]);
			query.setString("nazwaf", data[0].split("<>")[1]);
			List<Faktura> res_faktura = query.list();
			Faktura fak =  res_faktura.get(0);
			fak.setName(data[1]);
			
			String selectDostawca = "from Dostawca d where d.name =:nazwad";
			query = session.createQuery(selectDostawca);
			query.setString("nazwad", data[2]);
			List<Dostawca> res_dostawca = query.list();
			fak.setDostawca(res_dostawca.get(0));
			session.save(fak);
			
//			HistoriaFaktura hf = new HistoriaFaktura();
//			hf.setFaktura(fak);
//			hf.setTime(new Date());
//			session.save(hf);	
						
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return 0;
		} finally {
			session.disconnect();
			session.close();
		}
		return 1;		
	}	
}
