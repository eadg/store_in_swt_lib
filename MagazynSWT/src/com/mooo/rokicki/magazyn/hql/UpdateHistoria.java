package com.mooo.rokicki.magazyn.hql;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.Historia;
import com.mooo.rokicki.magazyn.db.Personel;
import com.mooo.rokicki.magazyn.db.HibernateUtil;

public class UpdateHistoria {
	
	
	public Integer returnToStore(Integer id) {
		//drop Product in the way set property Historia.operation on "d" 
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Integer idhist = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String queryString = "from Historia h where h.id = :idHis";
			query = session.createQuery(queryString);
			query.setInteger("idHis", id);
			List<Historia> hlist = query.list();
			Historia histob =  hlist.get(0);
			histob.setOperation("d");
			idhist = (Integer) session.save(histob);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return idhist;
	}

	public Integer updateOwner(Integer id, Personel per) {
		//drop Product in the way set property Historia.operation on "d" 
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Integer idhist = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String queryString = "from Historia h where h.id = :idHis";
			query = session.createQuery(queryString);
			query.setInteger("idHis", id);
			List<Historia> hlist = query.list();
			Historia histob =  hlist.get(0);
			histob.setPersonel(per);
			idhist = (Integer) session.save(histob);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return idhist;
	}
	
	public Integer deletetHistoriaBytOsoba(String osoba) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Query query = null;
		int result;
		try {
			transaction = session.beginTransaction();				
			String squery = "delete from Historia h where h.id in " +
					"(select h.id from Historia h where (h.operation='c' or h.operation='d') and h.Personel.name =:sq)";			
			query = session.createQuery(squery);
			query.setString("sq", osoba);
			result = query.executeUpdate();
			transaction.commit();
			return result;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return 0;
		} finally {
			session.disconnect();
			session.close();
		}
	}	
	
}
