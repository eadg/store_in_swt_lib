package com.mooo.rokicki.magazyn.hql;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.Dostawca;
import com.mooo.rokicki.magazyn.db.Faktura;
import com.mooo.rokicki.magazyn.db.HibernateUtil;
import com.mooo.rokicki.magazyn.db.HistoriaFaktura;

public class InsertFaktura {
	
	public Integer saveFaktura(String nazwa, String dostawca) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Faktura fak = new Faktura();
			fak.setName(nazwa);
//			fak.setTime(new Date());
			fak.setDead(false);

			String selectDostawca = "from Dostawca d where d.name = :dost";
			Query query = session.createQuery(selectDostawca);
			query.setString("dost", dostawca);
			List<Dostawca> res_dostawca = query.list();
			Dostawca dos = res_dostawca.get(0);
			fak.setDostawca(dos);

			session.save(fak);
			
			HistoriaFaktura hf = new HistoriaFaktura();
			hf.setFaktura(fak);
			hf.setTime(new Date());
			session.save(hf);	
						
			transaction.commit();
			
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			System.out.println("Faktura nie dodana");
			return 0;
		} finally {
			session.close();
			System.out.println("finally");
		}
		return 1;
	}
	
	public Integer saveFaktura(String[] data) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Faktura fak = new Faktura();
			fak.setName(data[0]);
//			fak.setTime(new Date());
			fak.setDead(false);

			String selectDostawca = "from Dostawca d where d.name = :dost";
			Query query = session.createQuery(selectDostawca);
			query.setString("dost", data[1]);
			List<Dostawca> res_dostawca = query.list();
			Dostawca dos = res_dostawca.get(0);
			fak.setDostawca(dos);

			session.save(fak);
			
			HistoriaFaktura hf = new HistoriaFaktura();
			hf.setFaktura(fak);
			hf.setTime(new Date());
			session.save(hf);	
						
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			System.out.println("Faktura nie dodana");
			return 0;
		} finally {
			session.close();
//			System.out.println("finally");
		}
		return 1;
	}


}
