package com.mooo.rokicki.magazyn.hql;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.Personel;
import com.mooo.rokicki.magazyn.db.HibernateUtil;

public class InsertPersonel {

	public Integer savePersonel(String nazwa) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Integer id_personel = null;
		List<Personel> lista = null;
		try {
			transaction = session.beginTransaction();
			String selectPersonel = "from Personel P where P.name = :pers";
			Query query = session.createQuery(selectPersonel);
			query.setString("pers", nazwa);
			lista = query.list();
			if (lista.isEmpty()) {
				Personel per = new Personel();
				per.setName(nazwa);
				per.setSystem(false);
				id_personel = (Integer) session.save(per);
				transaction.commit();
			}
			else {
				id_personel=0;
			}
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			System.out.println("osoba już istnieje");
		} finally {
			session.close();
			System.out.println("finally");
		}
		return new Integer(id_personel);
	}

	public Integer saveUserSystem() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Integer id_personel = null;
		List<Personel> lista = null;
		try {
			transaction = session.beginTransaction();
			lista = session.createQuery("from Personel p where p.name='system'").list();
			transaction.commit();
			if (lista.isEmpty()) {
				transaction = session.beginTransaction();
				Personel per = new Personel();
				per.setName("system");
				per.setSystem(true);
				id_personel = (Integer) session.save(per);
				transaction.commit();
			} else {
				id_personel = 0;
			}
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			System.out.println("rollback");
		} finally {
			session.close();
			System.out.println("session close");
		}
		return new Integer(id_personel);
	}

}
