package com.mooo.rokicki.magazyn.hql;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.HashMap;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.HibernateUtil;
import com.mooo.rokicki.magazyn.util.ConvertTime;;


public class HqlEditOwnerItem {
	
	public List<Object> selectProduktByFakturaKategoria(String fak, String kat) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object> lprodukt = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
//			String squery = "select distinct p.name from Produkt p, Historia h where p.id=h.Produkt.id and p.Faktura.name =:fakt and p.Kategoria.name =:kate and p.amount > 0 and h.operation='c' order by p.name";			
//			String squery = "select distinct h.Produkt.name from Historia h where h.Produkt.Faktura.name =:fakt and h.Produkt.Kategoria.name =:kate and h.operation='c' group by h.Produkt.name having sum(h.amount) > 0 order by h.Produkt.name";			
			String squery = "select distinct h.Produkt.name from Historia h where h.Produkt.Faktura.name =:fakt and h.Produkt.Kategoria.name =:kate and h.operation='c' order by h.Produkt.name";			
			query = session.createQuery(squery);
			query.setString("fakt", fak);
			query.setString("kate", kat);
			lprodukt = query.list();
			for (Iterator<Object> iterator = lprodukt.iterator(); iterator
					.hasNext();) {
				Object obj = (Object)iterator.next();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
	
	public List<Object> selectHistoriaByProdukt(String produkt) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object> lprodukt = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String squery = "select distinct h.Personel.name from Historia h where h.Produkt.name =:sq and h.operation='c' order by h.Personel.name";			
			query = session.createQuery(squery);
			query.setString("sq", produkt);
			lprodukt = query.list();
			for (Iterator<Object> iterator = lprodukt.iterator(); 
					iterator.hasNext();) {
				Object hist = (Object) iterator.next();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
	
	public List<Object[]> selectHistoriaByProduktOsoba(String produkt, String osoba) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object[]> lprodukt = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();				
			String squery = "select distinct abs(h.amount), p.Jednostka.name from Historia h, Produkt p where h.Produkt.id = p.id and h.Produkt.name =:sq and h.operation='c' and h.Personel.name =:sq2";			
			query = session.createQuery(squery);
			query.setString("sq", produkt);
			query.setString("sq2", osoba);
			lprodukt = query.list();
			for (Iterator<Object[]> iterator = lprodukt.iterator(); 
					iterator.hasNext();) {
				Object[] hist = (Object[]) iterator.next();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
	
	public List<Object[]> selectHistoriaByProduktOsobaIlosc(String produkt, String osoba, Integer ilosc) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object[]> lprodukt = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String squery = "select h.time, h.id from Historia h where h.Produkt.name =:sq and h.operation='c' and h.Personel.name =:sq2 and h.amount =:sq3 order by h.time";			
			query = session.createQuery(squery);
			query.setString("sq", produkt);
			query.setString("sq2", osoba);
			query.setInteger("sq3", ilosc);
			lprodukt = query.list();
			for (Iterator<Object[]> iterator = lprodukt.iterator(); 
					iterator.hasNext();) {
				Object[] hist = (Object[]) iterator.next();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
	
	public HashMap<String, Integer> listArrayObjectToMap(List<Object[]> dl) {
		HashMap<String, Integer> map_out = new HashMap<String, Integer>();
		for(Object[] oarray : dl){
			map_out.put(ConvertTime.prettyFullDate((Date)oarray[0]), (Integer)oarray[1]);
		}
		return map_out;
	}	
}
