package com.mooo.rokicki.magazyn.hql;

import java.util.Iterator;
import java.util.List;
import java.util.Date;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.mooo.rokicki.magazyn.db.HibernateUtil;

public class StanMagazyn {


	public List<Object[]> selectProduktWithAmountToDay(Date time) {
		
		//All object from Begining Store :) First method

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object[]> lprodukt = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String selectProdukt = "select h.Produkt.id, h.Produkt.name, h.Produkt.Faktura.name, h.Produkt.value, h.Produkt.Faktura.Dostawca.name, sum(h.amount) from Historia as h where h.time < :times group by h.Produkt.id, h.Produkt.name, h.Produkt.Faktura.name, h.Produkt.Faktura.Dostawca.name, h.Produkt.value";

			query = session.createQuery(selectProdukt);
			query.setDate("times", time);
			lprodukt = query.list();
			for (Iterator<Object[]> iterator = lprodukt.iterator(); iterator
					.hasNext();) {
				Object obj[] = (Object[])iterator.next();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
	
	public List<Object[]> selectProduktWithAmountPeriod(Date time_e) {
		
		//Object from period

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object[]> lprodukt = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String selectProdukt = "select h.Produkt.id, h.Produkt.name, h.Produkt.Faktura.name, h.Produkt.Faktura.Dostawca.name, h.Produkt.value, sum(h.amount), h.Produkt.value*sum(h.amount) from Historia as h where h.operation!='d' and h.time < :timee group by h.Produkt.id, h.Produkt.name, h.Produkt.Faktura.name, h.Produkt.Faktura.Dostawca.name, h.Produkt.value order by h.Produkt.Faktura.name";

			query = session.createQuery(selectProdukt);
//			query.setDate("timeb", time_b);
			query.setDate("timee", time_e);
			lprodukt = query.list();
			for (Iterator<Object[]> iterator = lprodukt.iterator(); iterator
					.hasNext();) {
				Object obj[] = (Object[])iterator.next();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
	
	
	
//	public List<Object[]> selectProduktWithAmountPeriod(Date time_b, Date time_e) {
//		
//		//Object from period - orginalna metoda, która działa do 2013-11-22, ale dawała błędne wyniki
//
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		Transaction transaction = null;
//		List<Object[]> lprodukt = null;
//		Query query = null;
//		try {
//			transaction = session.beginTransaction();
//			String selectProdukt = "select h.Produkt.id, h.Produkt.name, h.Produkt.Faktura.name, h.Produkt.Faktura.Dostawca.name, h.Produkt.value, sum(h.amount), h.Produkt.value*sum(h.amount) from Historia as h where h.operation!='d' and h.time < :timee and h.time > :timeb group by h.Produkt.id, h.Produkt.name, h.Produkt.Faktura.name, h.Produkt.Faktura.Dostawca.name, h.Produkt.value order by h.Produkt.Faktura.name";
//
//			query = session.createQuery(selectProdukt);
//			query.setDate("timeb", time_b);
//			query.setDate("timee", time_e);
//			lprodukt = query.list();
//			for (Iterator<Object[]> iterator = lprodukt.iterator(); iterator
//					.hasNext();) {
//				Object obj[] = (Object[])iterator.next();
//			}
//			transaction.commit();
//			return lprodukt;
//		} catch (HibernateException e) {
//			transaction.rollback();
//			e.printStackTrace();
//			return null;
//		} finally {
//			session.disconnect();
//			session.close();
//		}
//	}
//	

}
