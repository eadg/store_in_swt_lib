package com.mooo.rokicki.magazyn.hql;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.Produkt;
import com.mooo.rokicki.magazyn.db.Personel;
import com.mooo.rokicki.magazyn.db.Historia;
import com.mooo.rokicki.magazyn.db.HibernateUtil;

public class InsertHistoria {

	public Integer saveHistoryChange(Produkt p, Integer update, String person) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Integer idHistory = null;
		try {
			transaction = session.beginTransaction();
			
			Historia history = new Historia();
	
			history.setAmount(update);
			
			String selectProdukt = "from Produkt p where p.id = :idPro";
			Query query = session.createQuery(selectProdukt);
			query.setInteger("idPro", p.getId());
			List<Produkt> res_produkt = query.list();
			Produkt pdb =  res_produkt.get(0);
			history.setProdukt(pdb);
			
			String selectPersonel = "from Personel p where p.name = :personel";
			query = session.createQuery(selectPersonel);
			query.setString("personel", person);
			List<Personel> res_personel = query.list();
			Integer idPersonel = res_personel.get(0).getId();
			Personel personel = (Personel) session.get(Personel.class, idPersonel);
			history.setPersonel(personel);

			history.setTime(new Date());
			history.setOperation("c");
			
			idHistory = (Integer) session.save(history);		
			transaction.commit();
			
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			System.out.println("Błąd przy aktualizacji historii");
		} finally {
			session.close();
		}
		return new Integer(idHistory);
	}	

	public Integer saveHistoryAdd(Integer id, Integer update, String person) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Integer idHistory = null;
		try {
			transaction = session.beginTransaction();
			
			Historia history = new Historia();
	
			history.setAmount(update);
			
			String selectProdukt = "from Produkt p where p.id = :idPro";
			Query query = session.createQuery(selectProdukt);
			query.setInteger("idPro", id);
			List<Produkt> res_produkt = query.list();
			Produkt pdb =  res_produkt.get(0);
			history.setProdukt(pdb);
			
			String selectPersonel = "from Personel p where p.name = :personel";
			query = session.createQuery(selectPersonel);
			query.setString("personel", person);
			List<Personel> res_personel = query.list();
			Integer idPersonel = res_personel.get(0).getId();
			Personel personel = (Personel) session.get(Personel.class, idPersonel);
			history.setPersonel(personel);

			history.setTime(new Date());
			history.setOperation("a");
			
			idHistory = (Integer) session.save(history);		
			transaction.commit();
			
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			System.out.println("Błąd przy aktualizacji historii");
		} finally {
			session.close();
		}
		return new Integer(idHistory);
	}
}
