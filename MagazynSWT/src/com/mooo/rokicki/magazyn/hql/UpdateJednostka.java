package com.mooo.rokicki.magazyn.hql;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.HibernateUtil;
import com.mooo.rokicki.magazyn.db.Jednostka;


public class UpdateJednostka {

	public Integer setNameUnit(String[] data) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			String selectJednostka = "from Jednostka j where j.name =:nazwa";
			Query query = session.createQuery(selectJednostka);			
			query = session.createQuery(selectJednostka);
			query.setString("nazwa", data[0]);
			List<Jednostka> lista = query.list();
			Jednostka per = lista.get(0);
			per.setName(data[1]);
			
			session.save(per);

			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return 0;
		} finally {
			session.disconnect();
			session.close();
		}
		return 1;		
	}	
}
