package com.mooo.rokicki.magazyn.hql;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.Historia;
import com.mooo.rokicki.magazyn.db.HibernateUtil;

public class SelectHistoria {
	
	public List<Object[]> selectHistoriaByProdukt(Integer idp) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object[]> lhist = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String selectHistoriaByKategoria = "select h.Produkt.Faktura.name, h.Produkt.Faktura.Dostawca.name, h.Produkt.name, h.amount, h.time, h.operation, h.Personel.name from Historia h where h.Produkt.id =:id and h.operation!='d'";			
			query = session.createQuery(selectHistoriaByKategoria);
			query.setInteger("id", idp);
			lhist = query.list();
			for (Iterator<Object[]> iterator = lhist.iterator(); 
					iterator.hasNext();) {
				Object[] hist = (Object[]) iterator.next();
			}
			transaction.commit();
			return lhist;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
	
	
	
	public List<Historia> listHistoriaByKategoria(String kategoria) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Historia> lprodukt = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String selectHistoriaByKategoria = "from Historia h where h.Produkt.Kategoria.name =:kat and h.Personel.name !='system' and h.operation!='d' order by h.time desc";			
			query = session.createQuery(selectHistoriaByKategoria);
			query.setString("kat", kategoria);
			lprodukt = query.list();
			for (Iterator<Historia> iterator = lprodukt.iterator(); 
					iterator.hasNext();) {
				Historia hist = (Historia) iterator.next();
				hist.getProdukt().getName();
				hist.getAmount();
				hist.getPersonel().getName();
				hist.getTime();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
	
	public List<Historia> listHistoriaByKategoria(Integer kategoria) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Historia> lprodukt = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String selectHistoriaByKategoria = "from Historia h where h.Produkt.Kategoria.id =:kat and h.Personel.name !='system'  and h.operation!='d' order by h.time desc";			
			query = session.createQuery(selectHistoriaByKategoria);
			query.setInteger("kat", kategoria);
			lprodukt = query.list();
			for (Iterator<Historia> iterator = lprodukt.iterator(); 
					iterator.hasNext();) {
				Historia hist = (Historia) iterator.next();
				hist.getProdukt().getName();
				hist.getAmount();
				hist.getPersonel().getName();
				hist.getTime();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
	
}
