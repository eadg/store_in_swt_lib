package com.mooo.rokicki.magazyn.hql;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.Faktura;
import com.mooo.rokicki.magazyn.db.Jednostka;
import com.mooo.rokicki.magazyn.db.Kategoria;
import com.mooo.rokicki.magazyn.db.Produkt;
import com.mooo.rokicki.magazyn.db.HibernateUtil;

import com.mooo.rokicki.magazyn.util.EnumInsert;

public class InsertProdukt {

	public Integer saveProduktString(String[] infoProdukt) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Integer id_produkt = null;
		try {
			
			String selectKategoria = "from Kategoria K where K.name = :kat";
			String selectFaktura = "from Faktura F where F.name = :fakt";			
			String selectJednostka = "from Jednostka J where J.name = :jedn";
			
			transaction = session.beginTransaction();
			Produkt produkt = new Produkt();
			
			produkt.setName(infoProdukt[EnumInsert.NAME.getValue()]);
//			produkt.setAmount(Integer.decode(infoProdukt[EnumInsert.AMOUNT.getValue()]));
			produkt.setValue(Double.valueOf(infoProdukt[EnumInsert.VALUE.getValue()]));
			
			Query query = session.createQuery(selectKategoria);
			query.setString("kat", infoProdukt[EnumInsert.CATEGORY.getValue()]);	
			List<Kategoria> res_kat = query.list();
			Integer idKategoria = res_kat.get(0).getId();
			Kategoria kategoria = (Kategoria) session.get(Kategoria.class, idKategoria);	
			produkt.setKategoria(kategoria);			
			
			query = session.createQuery(selectFaktura);
			query.setString("fakt", infoProdukt[EnumInsert.BILL.getValue()]);
			List<Faktura> res_fakt = query.list();
			Integer idFaktura = res_fakt.get(0).getId();	
			Faktura faktura = (Faktura) session.get(Faktura.class, idFaktura);
			produkt.setFaktura(faktura);
			
			query = session.createQuery(selectJednostka);
			query.setString("jedn", infoProdukt[EnumInsert.UNIT.getValue()]);
			List<Jednostka> res_jedn = query.list();
			Integer idJednostka = res_jedn.get(0).getId();			
			Jednostka jednostka = (Jednostka) session.get(Jednostka.class, idJednostka);
			produkt.setJednostka(jednostka);			
				
			id_produkt = (Integer) session.save(produkt);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			System.out.println("Błąd przy dodawaniu produktu");
		} finally {
			session.close();
		}
		return new Integer(id_produkt);
	}	
}
