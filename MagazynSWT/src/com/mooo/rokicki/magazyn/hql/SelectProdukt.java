package com.mooo.rokicki.magazyn.hql;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.mooo.rokicki.magazyn.db.HibernateUtil;

public class SelectProdukt {
	
	public List<Object> getAmountForProdukt(Integer id) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object> lhist = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String stringQuery = "select sum(h.amount) from Historia as h where h.operation!='d' and h.Produkt.id =:id group by h.Produkt.id having sum(h.amount) > 0";			
			query = session.createQuery(stringQuery);
			query.setInteger("id", id);
			lhist = query.list();
			for (Iterator<Object> iterator = lhist.iterator(); 
					iterator.hasNext();) {
				Object hist = (Object) iterator.next();
			}
			transaction.commit();
			return lhist;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}	
	
	public List<Object> selectAllProdukt() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object> lprodukt = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String stringQuery = "select p.name from Produkt p order by p.name";
			query = session.createQuery(stringQuery);
			lprodukt = query.list();
			for (Iterator<Object> iterator = lprodukt.iterator(); iterator
					.hasNext();) {
				Object str = iterator.next();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}	
	
	public String[] arraySelectProduktByFaktura(String s) {
		List<Object[]> lobj = selectProduktByFaktura(s);
		String[] out = new String[lobj.size()];
		int i=0;
		for(Object[] obj : lobj ) {
			out[i] = obj[0].toString();
			i++;
		}
		return out;
	}

	public List<Object[]> selectProduktByKategoria(Integer id) {
		//Object from period
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object[]> lprodukt = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
//			String stringQuery = "select p.name, p.amount, p.Faktura.Dostawca.name, p.Faktura.name, p.Jednostka.name, p.value from Produkt p, Historia h where p.id=h.Produkt.id and p.Kategoria.id =:kat and p.amount > 0 and h.operation='a' order by p.name";
			String stringQuery = "select h.Produkt.name, sum(h.amount), h.Produkt.Faktura.Dostawca.name, h.Produkt.Faktura.name, h.Produkt.Jednostka.name, h.Produkt.value from Historia as h where h.operation!='d' and h.Produkt.Kategoria.id =:kat group by h.Produkt.name, h.Produkt.Faktura.Dostawca.name, h.Produkt.Faktura.name, h.Produkt.Jednostka.name, h.Produkt.value having sum(h.amount) > 0 order by h.Produkt.name";
			query = session.createQuery(stringQuery);
			query.setInteger("kat", id);
			lprodukt = query.list();
			for (Iterator<Object[]> iterator = lprodukt.iterator(); iterator
					.hasNext();) {
				Object obj[] = (Object[])iterator.next();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}		
}	
	
	public List<Object[]> selectProduktByKategoria(String kategoria) {
		//Object from period
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object[]> lprodukt = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
//			String stringQuery = "select p.name, p.amount, p.Faktura.Dostawca.name, p.Faktura.name, p.Jednostka.name, p.value from Produkt p, Historia h where p.id=h.Produkt.id and p.Kategoria.name =:kat and p.amount > 0 and h.operation='a' order by p.name";
			String stringQuery = "select h.Produkt.name, sum(h.amount), h.Produkt.Faktura.Dostawca.name, h.Produkt.Faktura.name, h.Produkt.Jednostka.name, h.Produkt.value from Historia as h where h.operation!='d' and h.Produkt.Kategoria.name =:kat group by h.Produkt.name, h.Produkt.Faktura.Dostawca.name, h.Produkt.Faktura.name, h.Produkt.Jednostka.name, h.Produkt.value having sum(h.amount) > 0 order by h.Produkt.name";
			query = session.createQuery(stringQuery);
			query.setString("kat", kategoria);
			lprodukt = query.list();
			for (Iterator<Object[]> iterator = lprodukt.iterator(); iterator
					.hasNext();) {
				Object obj[] = (Object[])iterator.next();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}		

	public List<Object[]> selectProduktByFaktura(String fak) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object[]> lprodukt = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
//			String stringQuery = "select p.name, p.amount, p.Faktura.Dostawca.name, p.Faktura.name, p.Jednostka.name, p.value from Produkt p, Historia h where p.id=h.Produkt.id and p.Faktura.name =:fakt and p.amount > 0 and h.operation='a' order by p.name";			
			String stringQuery = "select h.Produkt.name, sum(h.amount), h.Produkt.Faktura.Dostawca.name, h.Produkt.Faktura.name, h.Produkt.Jednostka.name, h.Produkt.value from Historia as h where h.Produkt.Faktura.name =:fakt and h.operation='a' group by h.Produkt.name, h.Produkt.Faktura.Dostawca.name, h.Produkt.Faktura.name, h.Produkt.Jednostka.name, h.Produkt.value having sum(h.amount) > 0 order by h.Produkt.name";			
			query = session.createQuery(stringQuery);
			query.setString("fakt", fak);
			lprodukt = query.list();
			for (Iterator<Object[]> iterator = lprodukt.iterator(); iterator
					.hasNext();) {
				Object obj[] = (Object[])iterator.next();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
	
	public List<Object[]> selectProduktByPersonelPeriod(Date time_b, Date time_e, String osoba) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object[]> lprodukt = null;
		Query query = null;
		Object[] tabobj=null;
		
		try {
			transaction = session.beginTransaction();	
			String stringQuery = " select h.Produkt.id, p.name, h.Produkt.name, sum(h.amount), h.Produkt.Faktura.name, h.Produkt.Faktura.Dostawca.name from Historia as h, Personel as p where h.Personel.id=p.id and p.name =:pers and h.time < :timee and h.time > :timeb and h.operation='c' group by p.name, h.Produkt.id, h.Produkt.name, h.Produkt.Faktura.name, h.Produkt.Faktura.Dostawca.name";			
			query = session.createQuery(stringQuery);
			query.setString("pers", osoba);
			query.setDate("timeb", time_b);
			query.setDate("timee", time_e);
			lprodukt = query.list();
			for (Iterator<Object[]> iterator = lprodukt.iterator(); iterator
					.hasNext();) {
				tabobj = (Object[])iterator.next();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
	
	public List<Object[]> selectProduktByDostawcaFaktura(String dosfak) {

//		Dostępna poniżej wersja public List<Produkt> listProduktByDostawcaFaktura(String dosfak) {}
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object[]> lprodukt = null;
		Query query = null;
		
		try {
			transaction = session.beginTransaction();
			String[] input = dosfak.split("<>");
			
//			String stringQuery = "select p.name, h.amount, p.value, p.Kategoria.name, p.Jednostka.name from Produkt p, Historia h where p.id=h.Produkt.id and h.operation='a' and p.Faktura.name =:fakt and p.Faktura.Dostawca.name =:dost order by p.name";			
			String stringQuery = "select h.Produkt.name, sum(h.amount), h.Produkt.value, h.Produkt.Kategoria.name, h.Produkt.Jednostka.name from Historia h where h.operation='a' and h.Produkt.Faktura.name =:fakt and h.Produkt.Faktura.Dostawca.name =:dost group by h.Produkt.name, h.Produkt.value, h.Produkt.Kategoria.name, h.Produkt.Jednostka.name having sum(h.amount) > 0 order by h.Produkt.name";			
			query = session.createQuery(stringQuery);
			query.setString("fakt", input[1]);
			query.setString("dost", input[0]);
			lprodukt = query.list();
			for (Iterator<Object[]> iterator = lprodukt.iterator(); iterator
					.hasNext();) {
				Object obj[] = (Object[])iterator.next();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}

	
}
