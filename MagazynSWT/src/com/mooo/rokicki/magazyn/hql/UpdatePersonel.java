package com.mooo.rokicki.magazyn.hql;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.HibernateUtil;
import com.mooo.rokicki.magazyn.db.Personel;


public class UpdatePersonel {

	public Integer deletePersonel(String data) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		int result;
		try {
			transaction = session.beginTransaction();
			String selectPersonel = "delete Personel p where p.name =:nazwa";
			Query query = session.createQuery(selectPersonel);			
			query = session.createQuery(selectPersonel);
			query.setString("nazwa", data);
			result = query.executeUpdate();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return 0;
		} finally {
			session.disconnect();
			session.close();
		}
		return result;		
	}	
	
	public Integer setNamePersonel(String[] data) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			String selectPersonel = "from Personel p where p.name =:nazwa";
			Query query = session.createQuery(selectPersonel);			
			query = session.createQuery(selectPersonel);
			query.setString("nazwa", data[0]);
			List<Personel> res_dostawca = query.list();
			Personel per = res_dostawca.get(0);
			per.setName(data[1]);
			
			session.save(per);

			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return 0;
		} finally {
			session.disconnect();
			session.close();
		}
		return 1;		
	}	
}
