package com.mooo.rokicki.magazyn.hql;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.Dostawca;
import com.mooo.rokicki.magazyn.db.HibernateUtil;

public class InsertDostawca {

//	public Integer saveDostawca(String nazwa) {
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		Transaction transaction = null;
//		Integer id_Dostawca = null;
//		try {
//			transaction = session.beginTransaction();
//			Dostawca dostawca = new Dostawca();
//			dostawca.setName(nazwa);
//			id_Dostawca = (Integer) session.save(dostawca);
//			transaction.commit();
//		} catch (HibernateException e) {
//			transaction.rollback();
//			e.printStackTrace();
//			System.out.println("Dostawca już istnieje");
//		} finally {
//			session.close();
//			System.out.println("finally");
//		}
//		return new Integer(id_Dostawca);		
//	}
	
	public Integer saveDostawca(String nazwa) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Integer id = null;
		List<Dostawca> lista = null;
		try {
			transaction = session.beginTransaction();
			String selectDostawca = "from Dostawca P where P.name = :dost";
			Query query = session.createQuery(selectDostawca);
			query.setString("dost", nazwa);
			lista = query.list();
			if (lista.isEmpty()) {
				Dostawca per = new Dostawca();
				per.setName(nazwa);
				id = (Integer) session.save(per);
				transaction.commit();
			}
			else {
				id=0;
			}
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			System.out.println("dostawca już istnieje");
		} finally {
			session.close();
			System.out.println("finally");
		}
		return new Integer(id);
	}	

}
