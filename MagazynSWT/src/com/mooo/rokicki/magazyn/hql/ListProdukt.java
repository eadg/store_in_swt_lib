package com.mooo.rokicki.magazyn.hql;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.Produkt;
import com.mooo.rokicki.magazyn.db.HibernateUtil;


public class ListProdukt {
	
	public List<Produkt> listProduktByFaktura(String fak) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Produkt> lprodukt = null;
		Query query = null;
		
		try {
			transaction = session.beginTransaction();
			
			String stringQuery = "from Produkt p where p.Faktura.name =:fakt and p.amount > 0 order by p.name";			
			query = session.createQuery(stringQuery);
			query.setString("fakt", fak);

			lprodukt = query.list();
			for (Iterator<Produkt> iterator = lprodukt.iterator(); 
					iterator.hasNext();) {
				Produkt produkt = (Produkt) iterator.next();
				produkt.getKategoria().getName();
				produkt.getFaktura().getName();
				produkt.getFaktura().getDostawca().getName();				
				produkt.getJednostka().getName();
				produkt.getValue();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
	
	
	public List<String> listProduktOut(Produkt p) {
		//Sprawdza czy są operacje "a" na produkcie p
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<String> lprodukt = null;
		Query query = null;
		String s=null;
		try {
			transaction = session.beginTransaction();	
			String stringQuery = "select h.operation from Historia as h where h.Produkt.id =:idPro and h.operation='c' ";			
			query = session.createQuery(stringQuery);
			query.setInteger("idPro", p.getId());
			lprodukt = query.list();
			for (Iterator<String> iterator = lprodukt.iterator(); iterator
					.hasNext();) {
				s = (String)iterator.next();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}	
	

	public List<Produkt> listProduktByFakturaProdukt(String pro, String fak) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Produkt> lprodukt = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String stringQuery = "from Produkt p where p.Faktura.name =:fakt and p.name =:prod and p.Faktura.dead=0 order by p.Faktura.id";			
			query = session.createQuery(stringQuery);
			query.setString("fakt", fak);
			query.setString("prod", pro);
			lprodukt = query.list();
			for (Iterator<Produkt> iterator = lprodukt.iterator(); 
					iterator.hasNext();) {
				Produkt produkt = (Produkt) iterator.next();
				produkt.getKategoria().getName();
				produkt.getFaktura().getName();
				produkt.getFaktura().getDostawca().getName();				
				produkt.getJednostka().getName();
				produkt.getValue();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}	
	
	
	public List<Produkt> listProduktByFakturaProdukt(String pro, String fak, String dos) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Produkt> lprodukt = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String stringQuery = "from Produkt p where p.Faktura.name =:fakt and p.name =:prod " +
					"and p.Faktura.Dostawca.name =:dost and p.Faktura.dead=0 order by p.Faktura.id";			
			query = session.createQuery(stringQuery);
			query.setString("fakt", fak);
			query.setString("prod", pro);
			query.setString("dost", dos);
			lprodukt = query.list();
			for (Iterator<Produkt> iterator = lprodukt.iterator(); 
					iterator.hasNext();) {
				Produkt produkt = (Produkt) iterator.next();
				produkt.getKategoria().getName();
				produkt.getFaktura().getName();
				produkt.getFaktura().getDostawca().getName();				
				produkt.getJednostka().getName();
				produkt.getValue();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}	
	
	public String[] arrayFakturaByKategoria(String s) {
		List<Object[]> lobj = new SelectFaktura().selectFakturaByKategoria(s);
		String[] out = new String[lobj.size()];
		int i = 0;
		for(Object[] obj : lobj) {
			out[i] = obj[0].toString() +"<>"+ obj[1].toString();
			i++;
		}
		return out;
	}
	
}