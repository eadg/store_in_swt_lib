package com.mooo.rokicki.magazyn.hql;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.HibernateUtil;
import com.mooo.rokicki.magazyn.db.Kategoria;

public class SelectKategoria {
	

	public List<Kategoria> listStringKategoria() {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Kategoria> lkategoria = null;
		try {
			transaction = session.beginTransaction();
			lkategoria = session.createQuery("from Kategoria").list();
			for (Iterator<Kategoria> iterator = lkategoria.iterator(); iterator
					.hasNext();) {
				Kategoria Kategoria = (Kategoria) iterator.next();
				Kategoria.getId();
				Kategoria.getName();
			}
			transaction.commit();
			return lkategoria;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
}
