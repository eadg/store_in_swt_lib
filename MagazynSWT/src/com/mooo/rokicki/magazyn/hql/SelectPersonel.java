package com.mooo.rokicki.magazyn.hql;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.HibernateUtil;
import com.mooo.rokicki.magazyn.db.Personel;

public class SelectPersonel {

	public List<Personel> listPersonel() {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Personel> lpersonel = null;
		try {
			transaction = session.beginTransaction();
			lpersonel = session.createQuery("from Personel p where p.system='false'").list();
			for (Iterator<Personel> iterator = lpersonel.iterator(); iterator
					.hasNext();) {
				Personel Personel = (Personel) iterator.next();
				Personel.getId();
				Personel.getName();
			}
			transaction.commit();
			return lpersonel;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
	
	public List<Personel> listPersonelString(String arg) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Personel> lpersonel = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String selectPersonel = "from Personel p where p.system='false' and p.name=:pername";
			query = session.createQuery(selectPersonel);
			query.setString("pername", arg);			
			lpersonel = query.list();
			for (Iterator<Personel> iterator = lpersonel.iterator(); iterator
					.hasNext();) {
				Personel Personel = (Personel) iterator.next();
				Personel.getId();
				Personel.getName();
			}
			transaction.commit();
			return lpersonel;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}
