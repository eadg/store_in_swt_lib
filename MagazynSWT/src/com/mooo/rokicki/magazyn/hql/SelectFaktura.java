package com.mooo.rokicki.magazyn.hql;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.HibernateUtil;
import com.mooo.rokicki.magazyn.db.Faktura;

public class SelectFaktura {
	
	public List<Object[]> selectFakturaByKategoria(String kategoria) {
		//Object from period
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object[]> lprodukt = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String stringQuery = "select h.Produkt.Faktura.Dostawca.name, h.Produkt.Faktura.name from Historia as h where h.Produkt.Kategoria.name =:kat group by h.Produkt.Faktura.Dostawca.name, h.Produkt.Faktura.name having sum(h.amount) > 0";
			query = session.createQuery(stringQuery);
			query.setString("kat", kategoria);
			lprodukt = query.list();
			for (Iterator<Object[]> iterator = lprodukt.iterator(); iterator
					.hasNext();) {
				Object obj[] = (Object[])iterator.next();
			}
			transaction.commit();
			return lprodukt;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
	
	
	
	public List<Object[]> selectFakturaByDostawcaPeriod(Date time_b, Date time_e, String dos_name) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Object[]> lfak = null;
		Query query = null;		
		try {
			transaction = session.beginTransaction();
			
//			String sf = "select f.Dostawca.name, f.name, hifa.time, sum(pr.value) from " +
//					"Produkt as pr, Faktura as f, HistoriaFaktura as hifa, Historia h where h.Produkt.id=pr.id and pr.Faktura.id=f.id and f.id=hifa.Faktura.id and f.id in " +
//					"(select hf.id from HistoriaFaktura hf where hf.time < :timee and hf.time > :timeb) and f.Dostawca.name = :dos_name and h.operation!='d'" +
//					" group by f.name, f.Dostawca.name, hifa.time order by hifa.time asc";


			String sf = "select h.Produkt.Faktura.Dostawca.name, h.Produkt.Faktura.name, hifi.time, sum(h.amount*h.Produkt.value) from Historia h, HistoriaFaktura hifi where h.Produkt.Faktura.id=hifi.Faktura.id and h.operation='a' and h.Produkt.id in " +
					"(select distinct p.id from Produkt p where p.Faktura.id in " +
					"(select distinct hf.Faktura.id from HistoriaFaktura hf where hf.time < :timee and hf.time > :timeb and hf.Faktura.Dostawca.name =:dos_name)) group by h.Produkt.Faktura.Dostawca.name, h.Produkt.Faktura.name, hifi.time order by hifi.time" ;
			query = session.createQuery(sf);
			query.setDate("timeb", time_b);
			query.setDate("timee", time_e);
			query.setString("dos_name", dos_name);
			lfak = query.list();
			for (Iterator<Object[]> iterator = lfak.iterator(); iterator
					.hasNext();) {
				Object obj[] = (Object[])iterator.next();
			}
			transaction.commit();
			return lfak;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
	
	public List<Faktura> listFakturaAlive() {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Faktura> lfak = null;
		try {
			transaction = session.beginTransaction();
			lfak = session.createQuery("from Faktura f where f.dead='false' order by f.Dostawca.name").list();
			for (Iterator<Faktura> iterator = lfak.iterator(); iterator
					.hasNext();) {
				Faktura fak = (Faktura) iterator.next();
				fak.getId();
				fak.getName();
//				fak.getTime();
				fak.getDostawca().getName();
			}
			transaction.commit();
			return lfak;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.disconnect();
			session.close();
		}
	}
	
	public String[] toArrayString() {
		List<Faktura> list = listFakturaAlive();
		String[] out = new String[list.size()];
		for (int i = 0; i < list.size(); i++) {
			out[i] = list.get(i).toString();
		}
		return out;
	}

}
