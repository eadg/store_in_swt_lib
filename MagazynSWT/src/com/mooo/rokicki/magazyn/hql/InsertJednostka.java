package com.mooo.rokicki.magazyn.hql;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.Jednostka;
import com.mooo.rokicki.magazyn.db.HibernateUtil;

public class InsertJednostka {
	
	public Integer saveUnit(String nazwa) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Integer id_unit = null;
		List<Jednostka> lista = null;
		try {
			transaction = session.beginTransaction();
			String selectJednostka = "from Jednostka P where P.name = :jedn";
			Query query = session.createQuery(selectJednostka);
			query.setString("jedn", nazwa);
			lista = query.list();
			if (lista.isEmpty()) {
				Jednostka per = new Jednostka();
				per.setName(nazwa);
				id_unit = (Integer) session.save(per);
				transaction.commit();
			}
			else {
				id_unit=0;
			}
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			System.out.println("jednostka już istnieje");
		} finally {
			session.close();
			System.out.println("finally");
		}
		return new Integer(id_unit);
	}	

//	public Integer saveUnit(String nazwa) {
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		Transaction transaction = null;
//		Integer id_jednostka = null;
//		try {
//			transaction = session.beginTransaction();
//			Jednostka jednostka = new Jednostka();
//			jednostka.setName(nazwa);
//			id_jednostka = (Integer) session.save(jednostka);
//			transaction.commit();
//		} catch (HibernateException e) {
//			transaction.rollback();
//			e.printStackTrace();
//			System.out.println("jednostka już istnieje");
//		} finally {
//			session.close();
//			System.out.println("finally");
//		}
//		return new Integer(id_jednostka);
//	}

	public Integer saveUnitDefault() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Integer id_jednostka = null;
		List<Jednostka> ljednostka = null;		
		String[] units = new String[] { "sztuka", "para", "1 litr",
				"0.5 litra", "0.25 litra", "2 litry", "paczka", "worek",
				"pudełko" };
		String selectJednostka = "from Jednostka j where j.name =:nazwa";		
		for (String nazwa : units) {
			try {
				transaction = session.beginTransaction();
				Query query = session.createQuery(selectJednostka);
				query.setString("nazwa", nazwa);				
				ljednostka = query.list();
				transaction.commit();
				id_jednostka=0;
				if(ljednostka.isEmpty()) {
					transaction = session.beginTransaction();
					Jednostka jednostka = new Jednostka();
					jednostka.setName(nazwa);
					id_jednostka = (Integer) session.save(jednostka);
					transaction.commit();					
				}
			} catch (HibernateException e) {
				transaction.rollback();
				e.printStackTrace();
				System.out.println("rollback");
			}
		}
	 
		session.close();
	
		return new Integer(id_jednostka);
	}
}
