package com.mooo.rokicki.magazyn.hql;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.Historia;
import com.mooo.rokicki.magazyn.db.Produkt;
import com.mooo.rokicki.magazyn.db.HibernateUtil;

public class UpdateProdukt {

//	public Integer updateAmountProdukt(Produkt p, Integer update) {
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		Transaction transaction = null;
//		Integer id_produkt = null;
//		try {
//			transaction = session.beginTransaction();
//			String selectProdukt = "from Produkt p where p.id = :idPro";
//			Query query = session.createQuery(selectProdukt);
//			query.setInteger("idPro", p.getId());
//			List<Produkt> res_produkt = query.list();
//			Produkt pdb =  res_produkt.get(0);
//			pdb.setAmount(update);
//			id_produkt = (Integer) session.save(pdb);
//			transaction.commit();			
//		} catch (HibernateException e) {
//			transaction.rollback();
//			e.printStackTrace();
//			System.out.println("Błąd przy update produktu");
//		} finally {
//			session.close();
//		}
//		return id_produkt;
//	}	
	
	public Integer deleteProdukt(Produkt p) {
		//drop Product in the way set property Historia.operation on "d" 
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Integer idhist = null;
		Query query = null;
		try {
			transaction = session.beginTransaction();
			String queryString = "from Historia h where h.Produkt.id = :idPro";
			query = session.createQuery(queryString);
			query.setInteger("idPro", p.getId());
			List<Historia> hlist = query.list();
			Historia histob =  hlist.get(0);
			histob.setOperation("d");
			idhist = (Integer) session.save(histob);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return idhist;
	}	
}
