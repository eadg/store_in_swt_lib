package com.mooo.rokicki.magazyn.hql;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.HibernateUtil;
import com.mooo.rokicki.magazyn.db.Dostawca;

public class SelectDostawca {

	public List<Dostawca> listDostawca() {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Dostawca> ldostawca = null;
		try {
			transaction = session.beginTransaction();
			ldostawca = session.createQuery("from Dostawca").list();
			for (Iterator<Dostawca> iterator = ldostawca.iterator(); iterator
					.hasNext();) {
				Dostawca Dostawca = (Dostawca) iterator.next();
				Dostawca.getId();
				Dostawca.getName();
			}
			transaction.commit();
			return ldostawca;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
//			System.out.println("close listDostawca()");
			session.disconnect();
			session.close();
		}
	}
}
