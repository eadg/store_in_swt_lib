package com.mooo.rokicki.magazyn.hql;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.HibernateUtil;
import com.mooo.rokicki.magazyn.db.Dostawca;


public class UpdateDostawca {

	public Integer setNameDostawca(String[] data) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			String selectDostawca = "from Dostawca d where d.name =:nazwad";
			Query query = session.createQuery(selectDostawca);			
			query = session.createQuery(selectDostawca);
			query.setString("nazwad", data[0]);
			List<Dostawca> res_dostawca = query.list();
			Dostawca dos = res_dostawca.get(0);
			dos.setName(data[1]);
			
			session.save(dos);

			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return 0;
		} finally {
			session.disconnect();
			session.close();
		}
		return 1;		
	}	
}
