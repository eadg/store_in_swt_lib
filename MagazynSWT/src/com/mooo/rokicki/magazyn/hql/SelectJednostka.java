package com.mooo.rokicki.magazyn.hql;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mooo.rokicki.magazyn.db.HibernateUtil;
import com.mooo.rokicki.magazyn.db.Jednostka;

public class SelectJednostka {

	public List<Jednostka> listUnit() {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Jednostka> ljednostka = null;
		try {
			transaction = session.beginTransaction();
			ljednostka = session.createQuery("from Jednostka").list();
			for (Iterator<Jednostka> iterator = ljednostka.iterator(); iterator
					.hasNext();) {
				Jednostka Jednostka = (Jednostka) iterator.next();
				Jednostka.getId();
				Jednostka.getName();
			}
			transaction.commit();
			return ljednostka;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			return null;
		} finally {
//			System.out.println("close listJednostka()");
			session.disconnect();
			session.close();
		}
	}
}
