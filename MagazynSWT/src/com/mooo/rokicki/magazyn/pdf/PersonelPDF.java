package com.mooo.rokicki.magazyn.pdf;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class PersonelPDF extends FontPDF{
	
	Document document;
	public PersonelPDF() {
		document = new Document();
	}	
	
	public String pdfLista(String uri, String[] lisob) {
		
		try {
			PdfWriter.getInstance(document, new FileOutputStream(uri));
			Paragraph par = new Paragraph();

			document.open();
			String title = "Lista osób zarejestrowanych w Magazynie";
			document.addTitle(title);
			ToolPDF tp = new ToolPDF();
			document.add(tp.getTime());
			document.add(Chunk.NEWLINE);
			document.add(tp.getOffice());
			document.add(tp.getTitle(title));
			document.add(Chunk.NEWLINE);
			
			float[] columnWidths = {1f, 2f};
			String[] headers = new String[] { "LP", "Osoba" };
			PdfPTable table = new PdfPTable(headers.length);
			table.setWidthPercentage(50);
			table.setWidths(columnWidths);
			PdfPCell[] cell = new PdfPCell[headers.length];

			for (int i = 0; i < headers.length; i++) {
				cell[i] = new PdfPCell(new Paragraph(headers[i], header_font));
				cell[i].setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell[i]);
			}
			
//			for (int i = 0; i < headers.length; i++) {
//				par= new Paragraph(headers[i]);
//				par.setFont(header_font);
//				par.setAlignment(Element.ALIGN_JUSTIFIED);
//				cell[i] = new PdfPCell(par);
//				table.addCell(cell[i]);
//			}

			Integer amount=0;
			for (String str : lisob) {
				amount++;
				cell[0] = new PdfPCell(new Paragraph(amount.toString(), body_font));
				cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell[0]);			
				cell[1] = new PdfPCell(new Paragraph(str, body_font));
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell[1]);
			}

			document.add(table);
			document.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		  catch (DocumentException e) {
			e.printStackTrace();
		}		
		return uri;
	}	
	

}
