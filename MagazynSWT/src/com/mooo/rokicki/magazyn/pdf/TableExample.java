package com.mooo.rokicki.magazyn.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mooo.rokicki.magazyn.hql.StanMagazyn;
import com.mooo.rokicki.magazyn.util.ConvertTime;

import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;

public class TableExample {
    public static void main(String[] args) {
        Document document = new Document();
        OpenPDF opdf = new OpenPDF();
        try {
            PdfWriter.getInstance(document,
                new FileOutputStream("c:/tmp/stanMagazyn.pdf"));
    
            document.open();
            
            PdfPTable table = new PdfPTable(6);
            PdfPCell[] cell = new PdfPCell[6];
            
    		Integer[] in = new Integer[]{2013, 12, 29}; 
    		ConvertTime ct = new ConvertTime();
    		Date dt = ct.arrayIntToDate(in);
    		StanMagazyn sp = new StanMagazyn();
    		List<Object[]> hlist = sp.selectProduktWithAmountToDay(dt);
    		for(Object[] obj : hlist) {
    			for(int i=0 ; i<obj.length ; i++) {
    	            cell[i] = new PdfPCell(new Paragraph(obj[i].toString()));            
    	            table.addCell(cell[i]);  	            
    			}
    		}            
            document.add(table);
            document.close();
        } catch(Exception e){
        	
        }
        opdf.openUri("c:/tmp/stanMagazyn.pdf");
    }
}
