package com.mooo.rokicki.magazyn.pdf;

import java.io.IOException;
import java.util.Date;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.mooo.rokicki.magazyn.net.NazwaZlobka;
import com.mooo.rokicki.magazyn.util.ConvertTime;

public class ToolPDF {
	
	private Font titleFont;
	private Font officeFont;
	private Font dateFont;
	private BaseFont bf;
	
	public ToolPDF() {
		try {
			bf = BaseFont.createFont("Helvetica", "ISO-8859-2", false);
			titleFont = new Font(bf, 12, Font.UNDERLINE);
			officeFont = new Font(bf, 12, Font.BOLD);
			dateFont = new Font(bf, 12, Font.ITALIC);
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Paragraph getTime() {
		String s = ConvertTime.prettyFullDate(new Date());
		Paragraph p = new Paragraph(s, dateFont);
		p.setAlignment(Element.ALIGN_RIGHT);
		return p;		
	}
	
	public Paragraph getTitle(String s) {
		Paragraph p = new Paragraph(s , titleFont);
		p.setAlignment(Element.ALIGN_CENTER);
		return p;		
	}
	
	public Paragraph getOffice() {
		Paragraph p = new Paragraph(new NazwaZlobka().getNazwa(), officeFont);
		p.setAlignment(Element.ALIGN_CENTER);
		return p;		
	}

}
