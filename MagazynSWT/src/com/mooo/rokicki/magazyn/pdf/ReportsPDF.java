package com.mooo.rokicki.magazyn.pdf;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mooo.rokicki.magazyn.util.ConvertData;
import com.mooo.rokicki.magazyn.util.ConvertTime;

public class ReportsPDF extends FontPDF{
	
	Document document;
	public ReportsPDF() {
		document = new Document();
	}


	public String pdfHistoriaProdukt(String uri, List<Object[]> lisob) {
		
		try {
			PdfWriter.getInstance(document, new FileOutputStream(uri));

			document.open();
			String title = "Historia produktu";
			document.addTitle(title);
			ToolPDF tp = new ToolPDF();
			document.add(tp.getTime());
			document.add(Chunk.NEWLINE);
			document.add(tp.getOffice());
			document.add(tp.getTitle(title));
			document.add(Chunk.NEWLINE);
			
			float[] columnWidths = {2f, 2f, 2f, 1f, 2f, 2f, 2f};
			String[] headers = new String[] { "Faktura", "Dostawca", "Produkt", "Ilość", "Data", "Operacja", "Osoba" };
			PdfPTable table = new PdfPTable(headers.length);
			table.setWidthPercentage(100);
			table.setWidths(columnWidths);
			PdfPCell[] cell = new PdfPCell[headers.length];

			for (int i = 0; i < headers.length; i++) {
				cell[i] = new PdfPCell(new Paragraph(headers[i], header_font));
				table.addCell(cell[i]);
			}

			Integer amount=null;
			for (Object[] obj : lisob) {
				for (int i = 0; i < obj.length; i++) {
					
					switch(i) {
					case 3: 
						amount=Math.abs((Integer)(obj[i]));
						table.addCell(amount.toString());
						break;
					case 4:
						cell[i] = new PdfPCell(new Paragraph(ConvertTime.prettyFullDate((Date) obj[i])));
						table.addCell(cell[i]);
						break;
					case 5:
						System.out.println();
						if(((String)(obj[i])).equals("a")) {
							cell[i] = new PdfPCell(new Paragraph("Dodanie do systemu", body_font));
							table.addCell(cell[i]);
						}
						else if(((String)(obj[i])).equals("c")) {
							cell[i] = new PdfPCell(new Paragraph("Przydzielenie osobie", body_font));
							table.addCell(cell[i]);
						}
						break;
					default:
						cell[i] = new PdfPCell(new Paragraph(obj[i].toString(), body_font));
						table.addCell(cell[i]);
						break;	
					}
					
				}
			}

			document.add(table);
			document.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		  catch (DocumentException e) {
			e.printStackTrace();
		}		
		return uri;
	}	

	public String pdfCoMaOsoba(String uri, List<Object[]> lisob) {


		try {
			PdfWriter.getInstance(document, new FileOutputStream(uri));

			document.open();
			
			String title = "Produkty przydzielone danej osobie";
			document.addTitle(title);
			ToolPDF tp = new ToolPDF();
			document.add(tp.getTime());
			document.add(Chunk.NEWLINE);
			document.add(tp.getOffice());
			document.add(tp.getTitle(title));
			document.add(Chunk.NEWLINE);

			String[] headers = new String[] { "Produkt ID", "Osoba", "Produkt", "Ilość", "Faktura", "Dostawca" };
			PdfPTable table = new PdfPTable(headers.length);
			table.setWidthPercentage(100);
			PdfPCell[] cell = new PdfPCell[headers.length];

			for (int i = 0; i < headers.length; i++) {
				cell[i] = new PdfPCell(new Paragraph(headers[i], header_font));
				table.addCell(cell[i]);
			}

			Long amount=null;
			for (Object[] obj : lisob) {
				for (int i = 0; i < obj.length; i++) {
					if(i==3) {
						amount=Math.abs((Long)(obj[i]));
						table.addCell(amount.toString());
					}
					else{
					cell[i] = new PdfPCell(new Paragraph(obj[i].toString(), body_font));
					table.addCell(cell[i]);
					}
				}
			}

			document.add(table);
			document.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		  catch (DocumentException e) {
			e.printStackTrace();
		}	
		return uri;
	}	

	public String pdfPozycjeFaktura(String uri, List<Object[]> lisob) {


		try {
			PdfWriter.getInstance(document, new FileOutputStream(uri));

			document.open();
			String title = "Pozycje z faktury";
			document.addTitle(title);
			ToolPDF tp = new ToolPDF();
			document.add(tp.getTime());
			document.add(Chunk.NEWLINE);
			document.add(tp.getOffice());
			document.add(tp.getTitle(title));
			document.add(Chunk.NEWLINE);


			String[] headers = new String[] { "Produkt", "Wartość początkowa", "Cena", "Kategoria", "Jednostka" };
			PdfPTable table = new PdfPTable(headers.length);
			table.setWidthPercentage(100);			
			PdfPCell[] cell = new PdfPCell[headers.length];

			for (int i = 0; i < headers.length; i++) {
				cell[i] = new PdfPCell(new Paragraph(headers[i], header_font));
				table.addCell(cell[i]);
			}

			for (Object[] obj : lisob) {
				for (int i = 0; i < obj.length; i++) {
					cell[i] = new PdfPCell(new Paragraph(obj[i].toString(), body_font));
					table.addCell(cell[i]);
				}
			}

			document.add(table);
			document.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		  catch (DocumentException e) {
			e.printStackTrace();
		}	
		return uri;
	}	
	
	public String pdfHistoriaFaktur(String uri, List<Object[]> lisob) {
		
		Double sumik = new Double(0);
		Double d = new Double(0);

		try {
			PdfWriter.getInstance(document, new FileOutputStream(uri));

			document.open();
			String title = "Historia Faktur";
			document.addTitle(title);
			ToolPDF tp = new ToolPDF();
			document.add(tp.getTime());
			document.add(Chunk.NEWLINE);
			document.add(tp.getOffice());
			document.add(tp.getTitle(title));
			document.add(Chunk.NEWLINE);

			String[] headers = new String[] { "Dostawca", "Nazwa faktury", "Data rejestracji faktury", "Cena" };
			PdfPTable table = new PdfPTable(headers.length);
			table.setWidthPercentage(100);
			PdfPCell[] cell = new PdfPCell[headers.length];

			for (int i = 0; i < headers.length; i++) {
				cell[i] = new PdfPCell(new Paragraph(headers[i], header_font));
				table.addCell(cell[i]);
			}

			for (Object[] obj : lisob) {
				for (int i = 0; i < obj.length; i++) {
					
					switch(i) {
					case 2:		cell[i] = new PdfPCell(new Paragraph(ConvertTime.prettyFullDate((Date) obj[i]))); break;
					case 3:		{		
									d = (Double) obj[i];
									cell[i] = new PdfPCell(new Paragraph(ConvertData.setPrecisionToTwo(d)));
									sumik=sumik+d;
									break;
								}
//					case 3:		cell[i] = new PdfPCell(new Paragraph(ConvertData.setPrecisionToTwo((Double) obj[i]))); break;
					default: 	cell[i] = new PdfPCell(new Paragraph(obj[i].toString(), body_font));
					}
					table.addCell(cell[i]);
					
				}
			}

			document.add(table);
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("Suma całkowita: " + ConvertData.setPrecisionToTwo(sumik), body_font));			
			document.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		  catch (DocumentException e) {
			e.printStackTrace();
		}	
		return uri;
	}

	public String pdfStanMagazyn(String uri, List<Object[]> lisob, String s) {
		
		Double sumik = new Double(0);
		Double d = new Double(0);
		try {
			PdfWriter.getInstance(document, new FileOutputStream(uri));

			document.open();
			String title = "Stan Magazynu na dzień " + s;
			document.addTitle(title);
			ToolPDF tp = new ToolPDF();
			document.add(tp.getTime());
			document.add(Chunk.NEWLINE);
			document.add(tp.getOffice());
			document.add(tp.getTitle(title));
			document.add(Chunk.NEWLINE);

			String[] headers = new String[] { "Nr produktu", "Produkt", "Faktura", "Dostawca", "Cena", "Ilość", "Wartość" };
			PdfPTable table = new PdfPTable(headers.length);
			table.setWidthPercentage(100);
			PdfPCell[] cell = new PdfPCell[headers.length];

			for (int i = 0; i < headers.length; i++) {
				cell[i] = new PdfPCell(new Paragraph(headers[i], header_font));
				table.addCell(cell[i]);
			}
			
			for (Object[] obj : lisob) {
				for (int i = 0; i < obj.length; i++) {
					switch(i) {
					case 6:		{
						d = (Double) obj[i];
						cell[i] = new PdfPCell(new Paragraph(ConvertData.setPrecisionToTwo(d)));
						sumik=sumik+d;
						break;
						}
					default: 	cell[i] = new PdfPCell(new Paragraph(obj[i].toString(), body_font));
					}
					table.addCell(cell[i]);
				}
			}

			document.add(table);
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("Suma całkowita: " + ConvertData.setPrecisionToTwo(sumik), body_font));
			document.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		  catch (DocumentException e) {
			e.printStackTrace();
		}	
		return uri;
	}
	

}
