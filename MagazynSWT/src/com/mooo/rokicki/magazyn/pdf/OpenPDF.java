package com.mooo.rokicki.magazyn.pdf;

import java.awt.Desktop;
import java.io.File;

public class OpenPDF {

	public void openUri(String s) {

		try {
			File pdfFile = new File(s);
			if (pdfFile.exists()) {

				if (Desktop.isDesktopSupported()) {
					Desktop.getDesktop().open(pdfFile);
				} else {
					System.out.println("Awt Desktop is not supported!");
				}

			} else {
				System.out.println("File is not exists!");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
}
