package com.mooo.rokicki.magazyn.pdf;

import java.io.FileOutputStream;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
public class WaterMark {
    public static void main(String[] args) {
        try {
            PdfReader Read_PDF_To_Watermark = new PdfReader("/tmp/StanMagazyn.pdf");
            int number_of_pages = Read_PDF_To_Watermark.getNumberOfPages();
            PdfStamper stamp = new PdfStamper(Read_PDF_To_Watermark, new FileOutputStream("/tmp/StanMark.pdf"));
            int i = 0;
            Image watermark_image = Image.getInstance("/tmp/box100wbr.png");
            watermark_image.setAbsolutePosition(50, 100);
            PdfContentByte add_watermark;            
            while (i < number_of_pages) {
              i++;
              add_watermark = stamp.getUnderContent(i);
              add_watermark.addImage(watermark_image);
            }
            stamp.close();
        }
        catch (Exception i1) {
            i1.printStackTrace();
        }
    }
}
