package com.mooo.rokicki.magazyn.pdf;

import java.io.IOException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;

public class FontPDF {
	
	protected BaseFont bf;
	protected Font header_font;
	protected Font body_font;
	
	public FontPDF() {

		try {
			bf = BaseFont.createFont("Helvetica", "ISO-8859-2", false);
			header_font = new Font(bf, 12, Font.BOLDITALIC);
			body_font = new Font(bf, 12, Font.NORMAL);
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	


	

	

}
