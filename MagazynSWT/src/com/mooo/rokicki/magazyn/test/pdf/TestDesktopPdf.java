package com.mooo.rokicki.magazyn.test.pdf;

import com.mooo.rokicki.magazyn.hql.SelectPersonel;
import com.mooo.rokicki.magazyn.pdf.OpenPDF;
import com.mooo.rokicki.magazyn.pdf.PersonelPDF;
import com.mooo.rokicki.magazyn.util.ConvertList;
import com.mooo.rokicki.magazyn.util.SystemVariable;

public class TestDesktopPdf {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PersonelPDF rpdf = new PersonelPDF();
		OpenPDF opdf = new OpenPDF();
		opdf.openUri(rpdf.pdfLista(SystemVariable.PATHPDF.getValue()+"ListaOsob_"+".pdf", ConvertList.listToArray(new SelectPersonel().listPersonel())));

	}

}
