package com.mooo.rokicki.magazyn.test.syntax;

import java.util.*;

public enum EnumSigletonArray {
	
	MAPA;
	
	private final List<String> lipa;
	
	EnumSigletonArray() {
		lipa=new ArrayList<String>();
	}
	
	public List<String> getSingiel() {
		return lipa;
	}


}