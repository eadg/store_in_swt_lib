package com.mooo.rokicki.magazyn.test.syntax;

import java.util.List;

public abstract class TestSigletonString {

	public static void main(String[] args) {
		
		UseSingletons us = new UseSingletons();
		us.useSigletonList();
		
		List<String> lista = EnumSigletonArray.MAPA.getSingiel();
		lista.add("aaa");
		
		List<String> lista2 = EnumSigletonArray.MAPA.getSingiel();
		lista2.add("nana");
		
		
		for(String s : lista2) {
			System.out.println(s);
		}
		
		
		StringBuilder sb = EnumSigletonString.SWING.getSingiel();
		sb.append("lala");
		us.useSigletonString();
		StringBuilder sb1 = EnumSigletonString.SWING.getSingiel();
		System.out.println(sb1);
		
		
//		System.out.println( EnumSigletonString.MAPA.getString());
//		UseSigletonS1 us = new UseSigletonS1();
//		System.out.println(us.useSigleton());
//		System.out.println( EnumSigletonString.MAPA.getString());
	}

}
