package com.mooo.rokicki.magazyn.test.syntax;

import java.util.List;


public class UseSingletons {
	
	public List<String> useSigletonList() {
		List<String> lista = EnumSigletonArray.MAPA.getSingiel();
		lista.add("ok");
		return lista; 
	}
	
	public StringBuilder useSigletonString() {
		StringBuilder s = EnumSigletonString.SWING.getSingiel();
		s.append("kuku");
		return s; 
	}
	

}
