package com.mooo.rokicki.magazyn.test.syntax;

public enum EnumSigletonString {
	
	SWING;
	
	private final StringBuilder pipa;
	
	EnumSigletonString() {
		pipa=new StringBuilder();
	}
	
	public StringBuilder getSingiel() {
		return pipa;
	}
}
