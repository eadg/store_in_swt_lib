package com.mooo.rokicki.magazyn.test.db;

import com.mooo.rokicki.magazyn.hql.SelectPersonel;
import com.mooo.rokicki.magazyn.util.ConvertList;

public class SelectPersonelTest {

	public static void main(String[] args) {
		
		String[] tabPersonel=ConvertList.listToArray(new SelectPersonel().listPersonel());
		for(String s : tabPersonel) {
			System.out.println(s);
		}
	}
}