package com.mooo.rokicki.magazyn.test.db;

import com.mooo.rokicki.magazyn.hql.InsertFaktura;

public class InsertFakturaTest {

	public static void main(String[] args) {
		InsertFaktura ik1 = new InsertFaktura();
		ik1.saveFaktura("SG34", "Blumar SA"); 
		ik1.saveFaktura("SDGF343", "Blumar SA");
		ik1.saveFaktura("SDFK45", "Blumar SA");
		ik1.saveFaktura("OITRE/45", "Blumar SA");
		ik1.saveFaktura("NMM/45", "Blumar A");
		ik1.saveFaktura("IJWRI/23", "Blumar A");
		ik1.saveFaktura("LKJELGFSD/23", "Blumar A");
		ik1.saveFaktura("JKLSDKF/123", "Blumar A");
		ik1.saveFaktura("WIER/56", "Blumar A");
	}

}
