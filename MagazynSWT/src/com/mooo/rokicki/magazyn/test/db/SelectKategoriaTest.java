package com.mooo.rokicki.magazyn.test.db;

import com.mooo.rokicki.magazyn.hql.SelectKategoria;
import com.mooo.rokicki.magazyn.util.ConvertList;

public class SelectKategoriaTest {

	public static void main(String[] args) {
		
		String[] tabKategoria=ConvertList.listToArray(new SelectKategoria().listStringKategoria());
		for (String s : tabKategoria) {
				System.out.println(s);
		}
	}
}
