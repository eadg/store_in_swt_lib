package com.mooo.rokicki.magazyn.test.db.magazyn;

import java.util.Date;
import java.util.List;

import com.mooo.rokicki.magazyn.util.ConvertTime;
import com.mooo.rokicki.magazyn.hql.StanMagazyn;

public class StanMagazynuTest4 {
	public static void main(String[] args) {
		
		Integer[] in = new Integer[]{2013, 7, 29}; 
		ConvertTime ct = new ConvertTime();
		Date dt = ct.arrayIntToDate(in);
		StanMagazyn sp = new StanMagazyn();
		List<Object[]> his = sp.selectProduktWithAmountToDay(dt);
		for(Object[] obj : his) {	
			System.out.println(obj[0] + "|" +obj[1] + "|" + obj[2] + "|" + obj[3] + "|" + obj[4] + "|" + obj[5]);
		}

	}
}