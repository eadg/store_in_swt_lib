package com.mooo.rokicki.magazyn.test.db;

import com.mooo.rokicki.magazyn.hql.SelectDostawca;
import com.mooo.rokicki.magazyn.util.ConvertList;

public class SelectDostawcaTest {

	public static void main(String[] args) {
		
		String[] tabDostawca=ConvertList.listToArray(new SelectDostawca().listDostawca());
		for(String s : tabDostawca) {
			System.out.println(s);
		}
	}
}