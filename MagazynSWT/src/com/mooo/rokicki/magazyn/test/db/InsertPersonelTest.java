package com.mooo.rokicki.magazyn.test.db;

import com.mooo.rokicki.magazyn.hql.InsertPersonel;

public class InsertPersonelTest {

	public static void main(String[] args) {
		InsertPersonel ip = new InsertPersonel();
		ip.savePersonel("kuba rozpruwacz"); 
		ip.savePersonel("kubuś puchatek");
		ip.savePersonel("Agnostyk Front");
		ip.savePersonel("Andrzej Łajdak");
		ip.savePersonel("Amorella Pick");
		ip.savePersonel("Iso Hunt");
		ip.savePersonel("Margerita Dontaczer");
		ip.savePersonel("Jaś Wędrowniczek");
	}
}
