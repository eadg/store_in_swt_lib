package com.mooo.rokicki.magazyn.test.db;

import java.util.Date;
import java.util.List;

import com.mooo.rokicki.magazyn.util.ConvertTime;
import com.mooo.rokicki.magazyn.hql.SelectFaktura;

public class SelectFakturaTest3 {
	public static void main(String[] args) {

		Integer[] beg = new Integer[] { 2013, 5, 26 };
		Integer[] fin = new Integer[] { 2013, 9, 26 };
		ConvertTime ct = new ConvertTime();
		Date db = ct.arrayIntToDate(beg);
		Date df = ct.arrayIntToDate(fin);
		SelectFaktura sf = new SelectFaktura();
		List<Object[]> lobj = sf.selectFakturaByDostawcaPeriod(db, df, "Blumar SA");
		for(Object[] obj : lobj) {
			System.out.print(obj[0] + " ");
			System.out.print(obj[1]+ " ");
			System.out.print(obj[2]+ " ");
			System.out.println(obj[3]);
		}

	}

}

