package com.mooo.rokicki.magazyn.test.db.produkt;
import java.util.Date;
import java.util.List;

import com.mooo.rokicki.magazyn.hql.SelectFaktura;
import com.mooo.rokicki.magazyn.util.ConvertTime;

public class TestSelectFakturaByDostawcaPeriod {

	public static void main(String[] args) {
		
		Integer[] start = new Integer[]{2013, 1, 29};
		Integer[] end = new Integer[]{2013, 11, 29};		
		ConvertTime ct = new ConvertTime();
		Date dstart = ct.arrayIntToDate(start);
		Date dend = ct.arrayIntToDate(end);
		SelectFaktura sf = new SelectFaktura();
		List<Object[]> lfak = sf.selectFakturaByDostawcaPeriod(dstart, dend, "Blumar A");
		for(Object[] aobj : lfak) {
			System.out.print(aobj[0]+ " ");
			System.out.print(aobj[1]+ " ");
			System.out.print(aobj[2]+ " ");
			System.out.println(aobj[3]);
		}
	}

}
