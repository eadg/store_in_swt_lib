package com.mooo.rokicki.magazyn.test.db;

import com.mooo.rokicki.magazyn.db.Faktura;
import com.mooo.rokicki.magazyn.hql.SelectFaktura;
import java.util.*;

public class SelectFakturaTest {

	public static void main(String[] args) {
		
		SelectFaktura sk = new SelectFaktura();
		List<Faktura> listFaktura= sk.listFakturaAlive();
		for (Faktura s : listFaktura) {
			System.out.println(s);
		}		
	}
}
