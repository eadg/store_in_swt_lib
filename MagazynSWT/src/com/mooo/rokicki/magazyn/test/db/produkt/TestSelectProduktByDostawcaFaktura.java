package com.mooo.rokicki.magazyn.test.db.produkt;

import java.util.List;
import com.mooo.rokicki.magazyn.hql.SelectProdukt;

public class TestSelectProduktByDostawcaFaktura {
	public static void main(String[] args) {
		
		/*		Wersja z listą List<Produkt> też w classie SelectProdukt
		 *
		 *
		 */
		SelectProdukt sp = new SelectProdukt();
		List<Object[]> lp = sp.selectProduktByDostawcaFaktura("Blumar SA<>SG34");
		System.out.println(lp.size());
		for(Object[] obj : lp) {	
			System.out.println(obj[0] + "|" +obj[1] + "|" + obj[2] + "|" + obj[3] + "|" + obj[4]);
		}
//		List<Produkt> lp = sp.listProduktByDostawcaFaktura("Blumar SA<>SG34");
//		System.out.println(lp.size());
//		for(Produkt p : lp) {
//			System.out.println(p.getName());
//		}
		
	}
}
