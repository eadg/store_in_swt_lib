package com.mooo.rokicki.magazyn.test.db.produkt;

import java.util.Date;
import java.util.List;

import com.mooo.rokicki.magazyn.util.ConvertTime;
import com.mooo.rokicki.magazyn.hql.SelectProdukt;

public class TestSelectProduktByPersonel {
	public static void main(String[] args) {
		
		Integer[] start = new Integer[]{2013, 8, 29};
		Integer[] end = new Integer[]{2013, 10, 29};		
		ConvertTime ct = new ConvertTime();
		Date dstart = ct.arrayIntToDate(start);
		Date dend = ct.arrayIntToDate(end);
		SelectProdukt sp = new SelectProdukt();
		List<Object[]> his = sp.selectProduktByPersonelPeriod(dstart, dend, "janek2");
		for(Object[] obj : his) {	
			System.out.println(obj[0] + "|" +obj[1] + "|" + obj[2] + "|" + obj[3] + "|" + obj[4] + "|" + obj[5]);
		}

	}
}