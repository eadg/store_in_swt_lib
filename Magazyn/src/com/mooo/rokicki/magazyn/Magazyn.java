package com.mooo.rokicki.magazyn;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;
import java.awt.BorderLayout;
import java.awt.Color;

public class Magazyn {

	private JFrame frmMagazyn;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Magazyn window = new Magazyn();
					window.frmMagazyn.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Magazyn() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMagazyn = new JFrame();
		frmMagazyn.setTitle("Magazyn");
		frmMagazyn.setBounds(100, 100, 450, 300);
		frmMagazyn.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		LoginDialogController ldc = new LoginDialogController(frmMagazyn);
		ldc.show();
		frmMagazyn.setTitle("Witaj "+ldc.login+" w magazynie");
		
		JMenuBar menuBar = new JMenuBar();
		frmMagazyn.setJMenuBar(menuBar);
		
		JMenu mnAkcja = new JMenu("Akcja");
		menuBar.add(mnAkcja);
		
		JMenuItem mntmBaza = new JMenuItem("Baza");
		mnAkcja.add(mntmBaza);
		mntmBaza.addActionListener(al);
		
	}
	
	  private ActionListener al = new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	SelectTest st = new SelectTest();
		    	String[] strtab = st.select_produkty();
		        for (String s : strtab) {
		            System.out.println(s);
		            //txtrTextArea.append(s+"\n");
		        }		    	
		    }
		  };	

}
